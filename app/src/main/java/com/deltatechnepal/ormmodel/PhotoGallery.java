package com.deltatechnepal.ormmodel;

import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeltaTech on 4/12/2018.
 */

@Table(name = "PhotoGallery")
public class PhotoGallery extends Model {
    @Column(name = "photo_id")      public String photo_id;
    @Column(name = "restaurant_id") public String restaurant_id;
    @Column(name = "title")          public String title;
    @Column(name = "image")         public String image;
    @Column(name = "status")       public String status;
    @Column(name = "isMenu")      public String isMenu;
    @Column(name = "isMustTry")  public String isMustTry;
    @Column(name = "isHighLight")  public String isHighLight;
    @Column(name = "created_at")         public String created_at;

    //Mandatory no arg constructor
    public PhotoGallery(){
        super();
    }

    //retrieve single item using id
    public static PhotoGallery getItem(String id) {
        PhotoGallery  result = new Select().from(PhotoGallery.class).where("photo_id = ?", id).executeSingle();
        return result;
    }

    public static List<PhotoGallery> getMenu(String restaurant_id) {
        List<PhotoGallery> result = new Select().from(PhotoGallery.class).where("restaurant_id = ? AND isMenu = 1",restaurant_id).orderBy("created_at DESC").execute();
        return result;
    }
    public static List<PhotoGallery> getMustTry(String restaurant_id) {
        List<PhotoGallery> result = new Select().from(PhotoGallery.class).where("restaurant_id = ? AND isMustTry = 1",restaurant_id).orderBy("created_at DESC").execute();
        return result;
    }
    public static List<PhotoGallery> getHighLight(String restaurant_id) {
        List<PhotoGallery> result = new Select().from(PhotoGallery.class).where("restaurant_id = ? AND isHighLight = 1",restaurant_id).orderBy("created_at DESC").execute();
        return result;
    }
}
