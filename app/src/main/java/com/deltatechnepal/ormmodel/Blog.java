package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Blog")
public class Blog extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Blog";

    @Column(name = "blog_id")      public String blog_id;
    @Column(name = "title")          public String title;
    @Column(name = "slug")          public String slug;
    @Column(name = "summary")         public String summary;
    @Column(name = "details")       public String details;
    @Column(name = "author")      public String author;
    @Column(name = "publish_date")  public String publish_date;
    @Column(name = "image")         public String image;
    @Column(name = "status")          public String status;
    @Column(name = "readingtime")         public String readingtime;
    @Column(name = "tags")       public String tags;
    @Column(name = "created_at")       public String created_at;
    @Column(name = "viewed")        public String viewed;
    @Column(name = "creator_role")    public String creator_role;
    @Column(name = "author_name")    public String author_name;
    @Column(name = "total_like")    public String total_like;
    @Column(name = "comments")    public String comments;


    //Mandatory no arg constructor
    public Blog(){
        super();
    }

    //retrieve single item using id
    public static Blog getItem(String id) {
        Blog  result = new Select().from(Blog.class).where("blog_id = ?", id).executeSingle();
        return result;
    }
    public static List<Blog> getMatchingBlog(String id) {
        List<Blog>  result = new Select().from(Blog.class).where("slug = ?",id).execute();
        return result;
    }

    //retrieve all items
    public static List<Blog> getSponsoredBlog(int limit ) {
        List<Blog> result = new Select().from(Blog.class).orderBy("name DESC").limit(limit).execute();
        return result;
    }

    //get resto with limit or without limit
    public static List<Blog> getAllBlog(int limit) {
        List<Blog> result = new Select().from(Blog.class).orderBy("viewed DESC").limit(limit).execute();
        return result;
    }

    //get resto with user_id
    public static List<Blog> getAllBlogByUserID(String  userID) {
        List<Blog> result = new Select().from(Blog.class).where("author = ?",userID).orderBy("created_at DESC").execute();
        return result;
    }
}