package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Foodfact")
public class Foodfact extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Foodfact";

    @Column(name = "ff_id")      public String ff_id;
    @Column(name = "title")      public String title;
    @Column(name = "content")      public String content;


    //Mandatory no arg constructor
    public Foodfact(){
        super();
    }

    public static Foodfact getFoodfact() {
        Foodfact  result = new Select().from(Foodfact.class).where("ff_id =1").executeSingle();
        return result;
    }




    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}