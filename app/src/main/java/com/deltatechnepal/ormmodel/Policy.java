package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Policy")
public class Policy extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Policy";

    @Column(name = "policy_id")      public String policy_id;
    @Column(name = "content")      public String content;


    //Mandatory no arg constructor
    public Policy(){
        super();
    }

    public static Policy getPolicy() {
        Policy result = new Select().from(Policy.class).where("policy_id =1").executeSingle();
        return result;
    }

    public static String getContent() {
        Policy result = new Select().from(Policy.class).where("policy_id =1").executeSingle();
        String content = (result != null)?result.content:"";
        return content;
    }
}