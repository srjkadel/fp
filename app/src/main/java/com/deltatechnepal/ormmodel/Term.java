package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Term")
public class Term extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Policy";

    @Column(name = "term_id")      public String term_id;
    @Column(name = "content")      public String content;


    //Mandatory no arg constructor
    public Term(){
        super();
    }

    public static Term getTerm() {
        Term result = new Select().from(Term.class).where("term_id =1").executeSingle();
        return result;
    }

    public static String getContent() {
        Term result = new Select().from(Term.class).where("term_id =1").executeSingle();
        String content = (result != null)?result.content:"";
        return content;
    }
}