package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Banner")
public class Banner extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Banner";

    @Column(name = "banner_id")      public String banner_id;
    @Column(name = "title")      public String title;
    @Column(name = "image")      public String image;
    @Column(name = "banner_order")      public String banner_order;
    @Column(name = "status")      public String status;
    @Column(name = "caption")      public String caption;
    @Column(name = "link")      public String link;


    //Mandatory no arg constructor
    public Banner(){
        super();
    }

    //retrieve single item using id
    public static Banner getItem(String id) {
        Banner  result = new Select().from(Banner.class).where("banner_id = ?", id).executeSingle();
        return result;
    }

    //retrieve all items
    public static List<Banner> getActiveBanner(int limit ) {
        List<Banner> result = new Select().from(Banner.class).where("status=1").orderBy("banner_order DESC").limit(limit).execute();
        return result;
    }

    //get resto with limit or without limit
    public static List<Banner> getAllBanner(int limit) {
        List<Banner> result = new Select().from(Banner.class).orderBy("viewed DESC").limit(limit).execute();
        return result;
    }
}