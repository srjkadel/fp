package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

@Table(name = "RestoCat")
public class RestoCat extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "RestoCat";

    @Column(name = "cat_id")      public String cat_id;
    @Column(name = "name")      public String name;
    @Column(name = "summary")          public String summary;
    @Column(name = "status")          public String status;
    @Column(name = "created_at")         public String created_at;

    //Mandatory no arg constructor
    public RestoCat(){
        super();
    }

    public RestoCat(JsonObject jsonObject){
        this.cat_id     = MFunction.getStringVal(jsonObject, "id");
        this.name       = MFunction.getStringVal(jsonObject, "name");
        this.summary    = MFunction.getStringVal(jsonObject, "summary");
        this.status     = MFunction.getStringVal(jsonObject, "status");
        this.created_at = MFunction.getStringVal(jsonObject,"created_at");
    }

    //retrieve single item using id
    public static RestoCat getItem(String id) {
        RestoCat  result = new Select().from(RestoCat.class).where("cat_id = ?", id).executeSingle();
        return result;
    }

    //retrieve all active items
    public static List<RestoCat> getAllActiveRestoCat(int limit ) {
        List<RestoCat> result = new Select().from(RestoCat.class).where("status = 1").orderBy("name DESC").limit(limit).execute();
        return result;
    }

    //for retrive filter resturant

//    public static List<RestoCat> getFilterRestocat(int limit){
//        List <RestoCat> restoCats = new Select().
//    }

    //get resto with limit
    public static List<RestoCat> getAllRestoCat(int limit) {
        List<RestoCat> result = new Select().from(RestoCat.class).orderBy("name DESC").limit(limit).execute();
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    public static void saveAllCat(JsonArray array){
        if (array != null) {

            new Delete().from(RestoCat.class).execute();

            for (JsonElement temp:array
            ) {
                JsonObject tempObj = temp.getAsJsonObject();
                RestoCat tempResto = new RestoCat(tempObj);
                tempResto.save();

            }
        }

    }

}