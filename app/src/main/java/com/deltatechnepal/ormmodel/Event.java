package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.example.easywaylocation.EasyWayLocation;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.easywaylocation.EasyWayLocation.calculateDistance;

@Table(name = "Event")
public class Event extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Event";

    @Column (name = "event_id" )        public String event_id;
    @Column (name = "name")      public String name;
    @Column (name = "slug")      public String slug;
    @Column (name = "image")     public String image;
    @Column (name = "start_date" )    public String start_date;
    @Column (name = "end_date")   public String end_date;
    @Column (name = "location")   public String location;
    @Column (name = "country")   public String country;
    @Column (name = "state")   public String state;
    @Column (name = "city")   public String city;
    @Column (name =  "description" ) public String description;
    @Column (name =  "activities")  public String activities;
    @Column (name =  "contact_no")  public String contact_no;
    @Column (name =  "ticket_info" )  public String ticket_info;
    @Column (name =  "time" )  public String time;
    @Column (name =  "category")  public String category;
    @Column (name =  "status" ) public String status;
    @Column (name =  "IsSponsered")  public String IsSponsered;
    @Column (name =  "website")  public String website;
    @Column (name =  "lati")  public String lati;
    @Column (name =  "longi")  public String longi;
    @Column (name =  "formatted_address")  public String formatted_address;
    @Column (name =  "created_at")  public String created_at;
    @Column (name =  "category_name")  public String category_name;

    //Mandatory no arg constructor
    public Event(){
        super();
    }

    public Event(JsonObject tempobj){

        this.event_id = MFunction.getStringVal(tempobj,"event_id");
        this.name = MFunction.getStringVal(tempobj,"name");
        this.slug= MFunction.getArrayVal(tempobj,"slug");
        this.image =MFunction.getStringVal(tempobj,"image");
        this.start_date = MFunction.getStringVal(tempobj,"start_date");
        this.end_date = MFunction.getStringVal(tempobj,"end_date");
        this.location= MFunction.getStringVal(tempobj,"location");
        this.country = MFunction.getStringVal(tempobj,"country");
        this.state = MFunction.getStringVal(tempobj,"state");
        this.city = MFunction.getStringVal(tempobj,"city");
        this.description = MFunction.getStringVal(tempobj,"description");
        this.activities = MFunction.getStringVal(tempobj,"activities");
        this.contact_no = MFunction.getStringVal(tempobj,"contact_no");
        this.ticket_info= MFunction.getStringVal(tempobj,"ticket_info");
        this.time = MFunction.getStringVal(tempobj,"time");
        this.category = MFunction.getStringVal(tempobj,"category");
        this.status = MFunction.getStringVal(tempobj,"status");
        this.IsSponsered = MFunction.getStringVal(tempobj,"IsSponsored");
        this.website= MFunction.getStringVal(tempobj,"website");
        this.lati = MFunction.getStringVal(tempobj,"lati");
        this.longi = MFunction.getStringVal(tempobj,"longi");
        this.formatted_address = MFunction.getStringVal(tempobj,"formatted_address");
        this.created_at = MFunction.getStringVal(tempobj,"created_at");
        this.category_name = MFunction.getStringVal(tempobj,"category_name");

    }

    public static JsonObject getJsonObjectFromEvent(Event event){
        JsonObject temobj = new JsonObject();
        temobj.addProperty("event_id",event.event_id);
        temobj.addProperty("name",event.name);
        temobj.addProperty("slug",event.slug);
        temobj.addProperty("image",event.image);
        temobj.addProperty("start_date",event.start_date);
        temobj.addProperty("end_date",event.end_date);
        temobj.addProperty("location",event.location);
        temobj.addProperty("country",event.country);
        temobj.addProperty("state", event.state);
        temobj.addProperty("city",event.city);
        temobj.addProperty("description",event.description);
        temobj.addProperty("activities",event.activities);
        temobj.addProperty("contact_no",event.contact_no);
        temobj.addProperty("ticket_info",event.ticket_info);
        temobj.addProperty("time",event.time);
        temobj.addProperty("category",event.category);
        temobj.addProperty("status",event.status);
        temobj.addProperty("IsSponsored",event.IsSponsered);
        temobj.addProperty("website",event.website);
        temobj.addProperty("lati",event.lati);
        temobj.addProperty("longi",event.longi);
        temobj.addProperty("formatted_address",event.formatted_address);
        temobj.addProperty("created_at",event.created_at);
        temobj.addProperty("category_name",event.category_name);



        return temobj;



    }


    //retrieve all items
    public static List<Event> getSponsoredEvent(int limit ) {
        List<Event> result = new Select().from(Event.class).orderBy("name DESC").limit(limit).execute();
        return result;
    }

    //get resto with limit or without limit
    public static List<Event> getAllEvent(int limit) {
        List<Event> result = new Select().from(Event.class).orderBy("name DESC").limit(limit).execute();
        return result;
    }

    //retrieve single item using id
    public static Event getItem(String id) {
        Event  result = new Select().from(Event.class).where("event_id = ?", id).executeSingle();
        return result;
    }

    public static List<Event> getNearbyEvent(Double lati,Double longi){
        List<Event> result = new ArrayList<Event>();
        List<Event> tempList = new ArrayList<>();
        if(lati == null || longi == null) return result;
        tempList = new Select().from(Event.class).execute();
        for (Event ev:tempList
             ) {
            Double distanceFromMe = EasyWayLocation.calculateDistance(lati,longi,Double.parseDouble(ev.lati),Double.parseDouble(ev.longi));
            Log.i("DistanceFromMe Event",""+distanceFromMe);
            if(distanceFromMe < MConstant.NEARBY_DISTANCE_FILTER_LIMIT) {
                result.add(ev);
            }
        }
        return result;
    }

    public static void saveAll(JsonArray array){
        if (array != null) {

            new Delete().from(Event.class).execute();

            for (JsonElement temp:array
            ) {
                JsonObject tempObj = temp.getAsJsonObject();
                Event tempResto = new Event(tempObj);
                tempResto.save();

            }
        }

    }

}