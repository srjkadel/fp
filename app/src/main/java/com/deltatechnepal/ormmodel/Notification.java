package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Notification")
public class Notification extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Notification";

    @Column(name = "unique_id")      public String unique_id;
    @Column(name = "geofence_id")      public String geofence_id;
    @Column(name = "geofence_transition_type")      public String geofence_transition_type;
    @Column(name = "notification_id") public String notification_id;
    @Column(name = "title")      public String title;
    @Column(name = "description")      public String description;
    @Column(name = "unix_timestamp")   public String unix_timestamp ;


    //Mandatory no arg constructor
    public Notification(){
        super();
    }

    //retrieve single item using id
    public static Notification getItem(String id) {
        Notification result = new Select().from(Notification.class).where("notification_id = ?", id).executeSingle();
        return result;
    }

    //retrieve single item using id for transation_type
    public static Notification checkTransation(String id, int geofence_transition_type) {
        Notification result = new Select().from(Notification.class).where("geofence_id = ? AND geofence_transition_type=?",id,geofence_transition_type).orderBy("unix_timestamp DESC").executeSingle();

        //Notification result = new Select().from(Notification.class).where("notification_id = ?", id).and("geofence_transition_type =GEOFENCE_TRANSITION_ENTER ", geofence_transition_type).executeSingle();
        return result;
    }



    //retrive last row id
//    public static Long lastrow(){
//        Long i = new Select().from(Notification.class).orderBy("id Desc").executeSingle().getId();
//        return i;
//    }


    public static Notification getItemByUniqueID(String uniqueID) {
        Notification result = new Select().from(Notification.class).where("unique_id = ?", uniqueID).executeSingle();
        return result;
    }

    //retrieve all items
    public static List<Notification> getActiveBanner(int limit ) {
        List<Notification> result = new Select().from(Notification.class).where("status=1").orderBy("banner_order DESC").limit(limit).execute();
        return result;
    }

    //get notification with limit or without limit
    public static List<Notification> getAllNotification(int limit) {
        List<Notification> result = new Select().from(Notification.class).limit(limit).execute();
        return result;
    }



    //Delete trying



}