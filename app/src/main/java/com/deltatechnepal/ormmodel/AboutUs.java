package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

@Table(name = "AboutUs")
public class AboutUs extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "AboutUs";

    @Column(name = "about_id")      public String about_id;
    @Column(name = "content")      public String content;


    //Mandatory no arg constructor
    public AboutUs(){
        super();
    }

    public static AboutUs getAbout() {
        AboutUs  result = new Select().from(AboutUs.class).where("about_id =1").executeSingle();
        return result;
    }

    public static String getContent() {
        AboutUs about = new Select().from(AboutUs.class).where("about_id =1").executeSingle();
        String content = (about != null)?about.content:"";
        return content;
    }

}