package com.deltatechnepal.ormmodel;

/**
 * Created by DeltaTech on 1/29/2018.
 */


import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.example.easywaylocation.EasyWayLocation;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.util.Calendar.MONTH;

@Table(name = "Resto")
public class Resto extends Model {

    // Declare table name as public
    public static final String TABLE_NAME = "Resto";

    @Column(name = "resto_id")      public String resto_id;
    @Column(name = "name")          public String name;
    @Column(name = "slug")          public String slug;
    @Column(name = "email")         public String email;
    @Column(name = "website")       public String website;
    @Column(name = "category")      public String category;
    @Column(name = "banner_image")  public String banner_image;
    @Column(name = "image")         public String image;
    @Column(name = "city")          public String city;
    @Column(name = "state")         public String state;
    @Column(name = "country")       public String country;
    @Column(name = "zipcode")       public String zipcode;
    @Column(name = "mobile")        public String mobile;
    @Column(name = "created_by")    public String created_by;
    @Column(name =  "payment")      public String payment;
    @Column(name =  "average_cost") public String average_cost;
    @Column(name =  "phone")        public String phone;
    @Column(name =  "timing")       public String timing;
    @Column(name =  "seating")      public String seating;
    @Column(name =  "tags")         public String tags;
    @Column(name =  "owner")        public String owner;
    @Column(name =  "status")       public String status;
    @Column(name =  "about")        public String about;
    @Column(name =  "IsSponsored")  public String IsSponsored;
    @Column(name =  "facebookUrl")  public String facebookUrl;
    @Column(name =  "instagramUrl") public String instagramUrl;
    @Column(name =  "lati")         public String lati;
    @Column(name =  "longi")        public String longi;
    @Column(name =  "amenities")        public String amenities;
    @Column(name =  "cuisines")        public String cuisines;
    @Column(name =  "additional_info")        public String additional_info;
    @Column(name =  "photogallery")        public String photogallery;
    @Column(name =  "reviews")        public String reviews;
    @Column(name =  "formatted_address")        public String formatted_address;
    @Column(name =  "recomended")   public String recomended;
    @Column(name =  "addedByUser")  public String addedByUser;
    @Column(name =  "r_rating")     public String r_rating;
    @Column(name =  "created_at")   public String created_at;
    @Column(name =  "updated_at")   public String updated_at;
    @Column(name =  "country_sortname")     public String country_sortname;
    @Column(name =  "country_name")         public String country_name;
    @Column(name = "country_phone_code")    public String country_phone_code;
    @Column(name = "state_name")            public String state_name;
    @Column(name = "cat_name")              public String cat_name;
    @Column(name = "cat_summary")           public String cat_summary;
    @Column(name ="total_recommendation")           public String total_recommendation;
    @Column(name ="average_rating")           public String average_rating;

    //Mandatory no arg constructor
    public Resto(){
        super();
    }

    public Resto(JsonObject tempObj){

        this.resto_id = MFunction.getStringVal(tempObj,"id");
        this.name = MFunction.getStringVal(tempObj,"name");
        this.total_recommendation = MFunction.getStringVal(tempObj,"total_recommendation");
        this.reviews =MFunction.getStringVal(tempObj,"reviews");
        this.r_rating = MFunction.getStringVal(tempObj,"r_rating");
        this.recomended = MFunction.getStringVal(tempObj,"recomended");
        this.photogallery = MFunction.getStringVal(tempObj,"photogallery");
        this.formatted_address = MFunction.getStringVal(tempObj,"formatted_address");
        this.lati = MFunction.getStringVal(tempObj,"lat");
        this.longi = MFunction.getStringVal(tempObj,"log");
        this.instagramUrl = MFunction.getStringVal(tempObj,"instagramUrl");
        this.facebookUrl = MFunction.getStringVal(tempObj,"facebookUrl");
        this.cuisines = MFunction.getStringVal(tempObj,"cuisines");
        this.created_at = MFunction.getStringVal(tempObj,"created_at");
        this.country_sortname = MFunction.getStringVal(tempObj,"country_sortname");
        this.country_phone_code = MFunction.getStringVal(tempObj,"country_phone_code");
        this.country_name = MFunction.getStringVal(tempObj,"country_name");
        this.cat_name =MFunction.getStringVal(tempObj,"cat_name");
        this.average_rating =MFunction.getArrayVal(tempObj,"average_rating");
        this.amenities = MFunction.getStringVal(tempObj,"amenities");
        this.additional_info =MFunction.getStringVal(tempObj,"additional_info");
        this.about =MFunction.getStringVal(tempObj,"about");
        this.owner =MFunction.getStringVal(tempObj,"owner");
        this.tags = MFunction.getStringVal(tempObj,"tags");
        this.seating =MFunction.getStringVal(tempObj,"seating");
        this.timing = MFunction.getStringVal(tempObj,"timing");
        this.phone = MFunction.getStringVal(tempObj,"phone");
        this.average_cost = MFunction.getStringVal(tempObj,"average_cost");
        this.payment = MFunction.getStringVal(tempObj,"payment");
        this.created_by = MFunction.getStringVal(tempObj,"created_by");
        this.mobile = MFunction.getStringVal(tempObj,"mobile");
        this.zipcode = MFunction.getStringVal(tempObj,"zipcode");
        this.website = MFunction.getStringVal(tempObj,"website");
        this.email =MFunction.getStringVal(tempObj,"email");
        this.slug = MFunction.getStringVal(tempObj,"slug");
        this.name = MFunction.getStringVal(tempObj,"name");
        this.city = MFunction.getStringVal(tempObj,"city");
        this.image = MFunction.getStringVal(tempObj,"image");
        this.category = MFunction.getStringVal(tempObj,"category");
        this.state = MFunction.getStringVal(tempObj,"state");
        this.banner_image = MFunction.getStringVal(tempObj,"banner_image");
        this.IsSponsored = MFunction.getStringVal(tempObj,"IsSponsored");


    }

    public static JsonObject getJsonObjectFromResto(Resto resto){

        JsonObject tempObj = new JsonObject();
        tempObj.addProperty("id",resto.resto_id);
        tempObj.addProperty("name",resto.name);
        tempObj.addProperty("total_recommendation",resto.total_recommendation);
        tempObj.addProperty("reviews",resto.reviews);
        tempObj.addProperty("r_rating",resto.r_rating);
        tempObj.addProperty("recomended",resto.recomended);
        tempObj.addProperty("photogallery",resto.photogallery);
        tempObj.addProperty("formatted_address",resto.formatted_address);
        tempObj.addProperty("lat",resto.lati);
        tempObj.addProperty("log",resto.longi);
        tempObj.addProperty("instagramUrl",resto.instagramUrl);
        tempObj.addProperty("facebookUrl",resto.facebookUrl);
        tempObj.addProperty("cuisines",resto.cuisines);
        tempObj.addProperty("created_at",resto.created_at);
        tempObj.addProperty("country_name",resto.country_name);
        tempObj.addProperty("country_sortname",resto.country_sortname);
        tempObj.addProperty("country_phone_code",resto.country_phone_code);
        tempObj.addProperty("cat_name",resto.cat_name);
        tempObj.addProperty("average_rating",resto.average_rating);
        tempObj.addProperty("amenities",resto.amenities);
        tempObj.addProperty("additional_info",resto.additional_info);
        tempObj.addProperty("about",resto.about);
        tempObj.addProperty("owner",resto.owner);
        tempObj.addProperty("tags",resto.tags);
        tempObj.addProperty("seating",resto.seating);
        tempObj.addProperty("timing",resto.timing);
        tempObj.addProperty("phone",resto.phone);
        tempObj.addProperty("average_cost",resto.average_cost);
        tempObj.addProperty("payment",resto.payment);
        tempObj.addProperty("created_by",resto.created_by);
        tempObj.addProperty("mobile",resto.mobile);
        tempObj.addProperty("zipcode",resto.zipcode);
        tempObj.addProperty("website",resto.website);
        tempObj.addProperty("email",resto.email);
        tempObj.addProperty("slug",resto.slug);
        tempObj.addProperty("name",resto.name);
        tempObj.addProperty("city",resto.city);
        tempObj.addProperty("image",resto.image);
        tempObj.addProperty("category",resto.category);
        tempObj.addProperty("state",resto.state);
        tempObj.addProperty("banner_image",resto.banner_image);
        tempObj.addProperty("IsSponsored",resto.IsSponsored);



        return tempObj;

    }

    //retrieve single item using id
    public static Resto getItem(String id) {
        Resto  result = new Select().from(Resto.class).where("resto_id = ?", id).executeSingle();
        return result;
    }

    //retrieve all items
    public static List<Resto> getSponsoredResto(int limit ) {
        List<Resto> result = new ArrayList<Resto>();
        if(limit >0) {
            result = new Select().from(Resto.class).where("IsSponsored = 1").orderBy("name DESC").limit(limit).execute();
        } else {
            result = new Select().from(Resto.class).where("IsSponsored = 1").orderBy("name DESC").execute();
        }
        return result;
    }

    public static List<Resto> getMostLovedResto(int limit ) {
        List<Resto> result = new ArrayList<Resto>();
        if(limit >0) {

            result = new Select().from(Resto.class).where("r_rating !=0.00").orderBy("name DESC").limit(limit).execute();
        } else {
            result = new Select().from(Resto.class).where("r_rating !=0.00").orderBy("name DESC").execute();
        }
        return result;
    }

    public static List<Resto> getNewlyAddedResto(int limit ) {
        List<Resto> result = new ArrayList<>();
        List<Resto> tempList;

        tempList = new Select().from(Resto.class).execute();
        // long currentTimeStamp = System.currentTimeMillis();

        Calendar calendarFilterPoint = Calendar.getInstance();
        calendarFilterPoint.add(MONTH,-1);
        long myPoint = calendarFilterPoint.getTimeInMillis();

        for (Resto res:tempList) {
            long createdAtUnix = MFunction.getUnixTimestamp(res.created_at);

            if(createdAtUnix>myPoint){
                //add to result
                result.add(res);

            }
        }
        return result;
    }




    //get resto with limit or without limit
    public static List<Resto> getAllResto(int limit) {
        List<Resto> result = new Select().from(Resto.class).orderBy("name DESC").limit(limit).execute();
        return result;
    }

    //get resto with user_id
    public static List<Resto> getAllRestoByUserID(String  userID) {
        List<Resto> result = new Select().from(Resto.class).where("created_by = ? AND addedByUser=1",userID).orderBy("created_at DESC").execute();
        return result;
    }

    public static List<Resto> getNearbyResto(Double lati,Double longi){
        List<Resto> result = new ArrayList<Resto>();
        List<Resto> tempList = new ArrayList<>();
        if(lati == null || longi == null) return result;
        tempList = new Select().from(Resto.class).execute();
        for (Resto ev:tempList) {
            Double distanceFromMe = EasyWayLocation.calculateDistance(lati,longi,Double.parseDouble(ev.lati),Double.parseDouble(ev.longi));
            //Log.i("start lati","_"+lati);
            //Log.i("start longi","_"+longi);
            //Log.i("end lati",ev.lati);
            //Log.i("start longi",ev.longi);
            //Log.i("DistanceFromMe Resto",""+distanceFromMe);
            if(distanceFromMe < MConstant.NEARBY_DISTANCE_FILTER_LIMIT) {
                result.add(ev);
            }
        }
        return result;
    }

    public static List<Resto> getNearbyResto(Double lati,Double longi,Long radius){
        List<Resto> result = new ArrayList<Resto>();
        List<Resto> tempList = new ArrayList<>();
        if(lati == null || longi == null) return result;
        tempList = new Select().from(Resto.class).execute();
        if(tempList.isEmpty()) {
            return result;
        }
        for (Resto ev:tempList) {

            if(!TextUtils.isEmpty(ev.lati) && !TextUtils.isEmpty(ev.longi) ){

                Double distanceFromMe = EasyWayLocation.calculateDistance(lati,longi,Double.parseDouble(ev.lati),Double.parseDouble(ev.longi));
                //Log.i("start lati","_"+lati);
                //Log.i("start longi","_"+longi);
                //Log.i("end lati",ev.lati);
                //Log.i("start longi",ev.longi);
                //Log.i("DistanceFromMe Resto",""+distanceFromMe);
                if(distanceFromMe < Double.valueOf(radius)) {
                    result.add(ev);
                }
                else
                {

                }

            }


        }



        return  result;
        //return result.subList(0,4);
    }





    public static void saveAll(JsonArray array){
        if (array!=null) {

            new Delete().from(Resto.class).execute();

            for (JsonElement temp:array
            ) {
                JsonObject tempObj = temp.getAsJsonObject();
                Resto tempResto = new Resto(tempObj);
                tempResto.save();

            }
        }
    }
}