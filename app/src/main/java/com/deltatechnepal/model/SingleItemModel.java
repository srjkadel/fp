package com.deltatechnepal.model;

import com.deltatechnepal.ormmodel.Resto;

public class SingleItemModel {
    private String resto_id,name,banner_image,description;
    public SingleItemModel() {
    }
    public SingleItemModel(Resto item) {
        this.resto_id = item.resto_id;
        this.name = item.name;
        this.banner_image = item.banner_image;
    }
    public String getRestoID() {
        return resto_id;
    }
    public void setRestoID(String resto_id) {
        this.resto_id = resto_id;
    }

    public String getBannerImage() {return banner_image;}
    public void setBannerImage(String url) {
        this.banner_image = banner_image;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
