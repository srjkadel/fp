package com.deltatechnepal.model;

/**
 * Created by DeltaTech on 4/22/2018.
 */

public class BlogModel {
    private String blog_id,title,slug,summary,details,author,publish_date,image,status,readingtime,tags,created_at,viewed,creator_role;

    public BlogModel() {
    }


    public BlogModel(String blog_id, String title, String slug, String summary, String details, String author, String publish_date, String image, String status, String readingtime, String tags, String created_at, String viewed, String creator_role) {
        this.blog_id = blog_id;
        this.title = title;
        this.slug = slug;
        this.summary = summary;
        this.details = details;
        this.author = author;
        this.publish_date = publish_date;
        this.image = image;
        this.status = status;
        this.readingtime = readingtime;
        this.tags = tags;
        this.created_at = created_at;
        this.viewed = viewed;
        this.creator_role = creator_role;
    }


    public String getBlogID() {
        return blog_id;
    }

    public void setBlogID(String blog_id) {
        this.blog_id = blog_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDetail() {
        return details;
    }

    public void setDetail(String details) {
        this.details = details;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReadingTime() {
        return readingtime;
    }

    public void setReadingTime(String readingtime) {
        this.readingtime = readingtime;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getViewed() {
        return viewed;
    }

    public void setViewed(String viewed) {
        this.viewed = viewed;
    }

    public String getCreatorRole() {
        return creator_role;
    }

    public void setCreatorRole(String creator_role) {
        this.creator_role = creator_role;
    }
}
