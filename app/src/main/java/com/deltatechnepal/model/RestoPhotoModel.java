package com.deltatechnepal.model;

/**
 * Created by DeltaTech on 4/12/2018.
 */

public class RestoPhotoModel {
    private String photo_id,restaurant_id,title,image,status,isMenu,isMustTry,created_at;

    public RestoPhotoModel() {
    }

    public RestoPhotoModel(String photo_id, String restaurant_id, String title, String image, String status, String isMenu, String isMustTry, String created_at) {
        this.photo_id = photo_id;
        this.restaurant_id = restaurant_id;
        this.title = title;
        this.image = image;
        this.status = status;
        this.isMenu = isMenu;
        this.isMustTry = isMustTry;
        this.created_at = created_at;
    }

    public String getPhotoID() {
        return photo_id;
    }

    public void setPhotoID(String photo_id) {
        this.photo_id = photo_id;
    }

    public String getRestaurantID() {
        return restaurant_id;
    }

    public void setRestaurantID(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(String isMenu) {
        this.isMenu = isMenu;
    }

    public String getIsMustTry() {
        return isMustTry;
    }

    public void setIsMustTry(String isMustTry) {
        this.isMustTry = isMustTry;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }
}
