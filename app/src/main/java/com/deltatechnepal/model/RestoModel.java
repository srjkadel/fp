package com.deltatechnepal.model;

/**
 * Created by DeltaTech on 1/29/2018.
 */

public class RestoModel {
    public String resto_id;
    public String name;
    public String slug;
    public String email;
    public String website;
    public String category;
    public String banner_image;
    public String image;
    public String city;
    public String state;
    public String country;
    public String zipcode;
    public String mobile;
    public String created_by;
    public String payment;
    public String average_cost;
    public String phone;
    public String timing;
    public String seating;
    public String tags;
    public String owner;
    public String status;
    public String about;
    public String IsSponsored;
    public String facebookUrl;
    public String instagramUrl;
    public String lati;
    public String longi;
    public String amenities;
    public String formatted_address;
    public String recomended;
    public String addedByUser;
    public String r_rating;
    public String created_at;
    public String updated_at;
    public String country_sortname;
    public String country_name;
    public String country_phone_code;
    public String state_name;
    public String cat_name;
    public String cat_summary;
    public String cuisines;
    public String additional_info;
    public String reviews;
    public String photogallery;
    public RestoModel() {
    }
}