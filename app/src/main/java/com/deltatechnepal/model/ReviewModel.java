package com.deltatechnepal.model;

/**
 * Created by DeltaTech on 5/4/2018.
 */

public class ReviewModel {
    private String  review_id,user_id,restaurant_id,comment,image,review_date,food,service,ambience,cleanliness,status,created_at,rating;

    public ReviewModel() {

    }

    public String getReviewID() {
        return review_id;
    }

    public void setReviewID(String review_id) {
        this.review_id = review_id;
    }

    public String getUserID() {
        return user_id;
    }

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getRestaurantID() {
        return restaurant_id;
    }

    public void setRestaurantID(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReviewDate() {
        return review_date;
    }

    public void setReviewDate(String review_date) {
        this.review_date = review_date;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAmbience() {
        return ambience;
    }

    public void setAmbience(String ambience) {
        this.ambience = ambience;
    }

    public String getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(String cleanliness) {
        this.cleanliness = cleanliness;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}