package com.deltatechnepal.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.example.easywaylocation.EasyWayLocation;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static java.lang.Math.abs;

/**
 * Created by DeltaTech on 1/18/2018.
 */

public class MFunction {
    //private   static ResponseListener mResponseListener;
    private static final String TAG = MFunction.class.getSimpleName();

    public static String  getFormattedDate(String tempDate, Context context){
        String mDate = tempDate;
        SimpleDateFormat fromDFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat toDFormat=new SimpleDateFormat("d MMM , h:mm a");
        String finalDate= null;
        int fromYear=0;
        int fromMonth=0;
        int fromDay=0;
        long dbSeconds =0;
        double showHours = 0;
        double showMinutes =0;
        double showSeconds =0;
        double mRemainder =0;
        try {
            Date date = fromDFormat.parse(mDate);
                /* for getting yyyy,mm,dd*/
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            fromYear = calendar.get(Calendar.YEAR);
            fromMonth = calendar.get(Calendar.MONTH) + 1;
            fromDay = calendar.get(Calendar.DAY_OF_MONTH);
            dbSeconds = date.getTime()/1000;
            finalDate=   toDFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        long currentSeconds = new Date().getTime()/1000;
        long diffSeconds = currentSeconds - dbSeconds;

        if (diffSeconds < (long)86400 && diffSeconds>= 0) {
            showHours = (double)diffSeconds/(double) 3600;
            int intHrPart = (int) showHours;
            showMinutes = (showHours - (double)intHrPart) * (double)60;
            int intMPart = (int) showMinutes;
            showSeconds = (showMinutes - (double) intMPart) * (double)60;
            int intSPart = (int) showSeconds;
            String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
            String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
            String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
            finalDate   = hr + m + s + "ago";
        } else {
            diffSeconds = abs(diffSeconds);
            if(diffSeconds>86400){
                finalDate = finalDate;
            } else {
                showHours = (double)diffSeconds/(double) 3600;
                int intHrPart = (int) showHours;
                showMinutes = (showHours - (double)intHrPart) * (double)60;
                int intMPart = (int) showMinutes;
                showSeconds = (showMinutes - (double) intMPart) * (double)60;
                int intSPart = (int) showSeconds;
                String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
                String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
                String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
                finalDate   = hr + m + s + "left";
            }
        }
        return finalDate;
    }
    public static boolean isInternetAvailable(Context context){
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null) {
            Log.d(TAG,"No Network");
            return false;
        } else {
            if(info.isConnected()) {
                Log.d(TAG,"Connectivity Exists !");
                return true;
            } else if(info.isAvailable()){
                Log.d(TAG,"Network Connectivity is possible");
                return true;
            } else {
                return false;
            }

        }
    }



    public boolean saveFilterArray(boolean[] array, String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("filters", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(arrayName +"_size", array.length);
        for(int i=0;i<array.length;i++)
            editor.putBoolean(arrayName + "_" + i, array[i]);
        return editor.commit();
    }



    public boolean[] loadFilterArray(String arrayName, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("filters", 0);
        int size = prefs.getInt(arrayName + "_size", 0);
        boolean array[] = new boolean[size];
        for(int i=0;i<size;i++)
            array[i] = prefs.getBoolean(arrayName + "_" + i, false);
        return array;
    }



    public static void setFirstTimeUse(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("app_use", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("first_time", false);
        editor.apply();
    }

    public static boolean getFirstTimeUse(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("app_use", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("first_time", true);
    }






    public static boolean isValidUrl(CharSequence target) {

        return (!TextUtils.isEmpty(target) && Patterns.WEB_URL.matcher(target).matches());
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPhone(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.PHONE.matcher(target).matches());
    }

    public static JSONObject jsonStrToObj(String jsonString){
        if(jsonString != null){
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static String getFormatedString(JSONObject jsonObj, String key, String currentLangID,int trimLimit){
        String mString=null;
        String finalString =null;
        if (jsonObj != null) try {
            mString = jsonObj.getString(currentLangID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        else mString = null;
        finalString = mString !=null? Html.fromHtml(mString).toString():mString;
        if(finalString ==null) return finalString;
        if (finalString.length()>trimLimit){
            return TextUtils.substring(finalString,0,trimLimit).concat("...");
        } else {
            return finalString;
        }
    }

    public static String getFormatedString(JSONObject jsonObj, String key, String currentLangID){
        String mString=null;
        String finalString =null;
        if (jsonObj != null) try {
            mString = jsonObj.getString(currentLangID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        else mString = null;
        finalString = mString !=null?Html.fromHtml(mString).toString():mString;
        return finalString;
    }

    /*Get preference values*/
    public static String getMyPrefVal(String key, Context context){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        return preferences.getString(key);
    }

    public static String getMyPrefVal(String key, Context context,String defaultValue){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        String value = preferences.getString(key);
        return TextUtils.isEmpty(value)?defaultValue:value;
    }

    /*Set preference values*/
    public static void  putMyPrefVal(String key, String val, Context context){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        preferences.put(key,val);
    }

    /*Clear All preference values*/
    public static void  clearMyPrefVal(Context context){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        preferences.clear();
    }

    /*Remove preference value*/
    public static void  clearMyPrefVal(Context context,String[] tempArray){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        if(tempArray.length>0) {
            for (String key:tempArray) {
                preferences.removeValue(key);
            }
        }
    }
    /*Date Related Helper Functions*/

    public static String getCurrentDateTime(){
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateTime = sdf.format(dt);
        return currentDateTime;
    }

    public static String getCurrentDate(){
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String currentDateTime = sdf.format(dt);
        return currentDateTime;
    }

    public static Date getStrToDate(String strDate,String format) throws ParseException {
        return new SimpleDateFormat(format).parse(strDate);
    }

    public static String getUID(){
        return UUID.randomUUID().toString();
    }



    public static long getUnixTimestamp(String datetime){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }



    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static double roundDown5(double d) {
        return (long) (d * 1e5) / 1e5;
    }

    public static String removeLast(String s, int n) {
        if (null != s && !s.isEmpty()) {
            s = s.substring(0, s.length()-n);
        }
        return s;
    }

    public static String getStatusString(String status) {
        String statusString = "";
        switch (status){
            case "0":
                statusString = "Inactive";
                break;
            case "1":
                statusString = "Active";
                break;

            case "2":
                statusString = "Pending";
                break;

        }
        return statusString;
    }

    public static void savePreferenceData(Map staffData, Context context){
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        Iterator<Map.Entry<String, String>> it = staffData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            preferences.put(pair.getKey(),pair.getValue());
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getFormattedProgressText(int progress){
        String progressText = "";
        switch(progress){
            case 1:{
                progressText = "Below Average";
                break;
            }
            case 2:{
                progressText = "Average";
                break;
            }
            case 3:{
                progressText = "Good";
                break;
            }
            case 4:{
                progressText = "Very Good";
                break;
            }
            case 5:{
                progressText = "Excellent";
                break;
            }
        }
        return progressText;
    }

    public static Double getDistance(Context mContext,String lati1,String longi1){
        String lati2 = MFunction.getMyPrefVal("lati",mContext);
        String longi2 = MFunction.getMyPrefVal("longi",mContext);
        if(TextUtils.isEmpty(lati1)|| TextUtils.isEmpty(longi1)|| TextUtils.isEmpty(lati2)|| TextUtils.isEmpty(longi2))return Double.valueOf(0);
        Double distance = EasyWayLocation.calculateDistance(Double.parseDouble(lati1),Double.parseDouble(longi1),Double.parseDouble(lati2),Double.parseDouble(longi2));
        Double inKM = distance/1000;
        return Math.floor(inKM * 100) / 100;
    }

    public static Double getDistance(Context mContext,Double lat1,Double long1,Double lat2,Double long2){
        if(lat1 == null || long1==null || lat2 == null || long2==null)return Double.valueOf(0);
        String defaultlat2 = MFunction.getMyPrefVal("lati",mContext);
        String defaultlong2 = MFunction.getMyPrefVal("longi",mContext);
        if(lat2 == 0.0){
            lat2 = Double.parseDouble(defaultlat2);
        }

        if(long2 == 0.0){
            long2 = Double.parseDouble(defaultlong2);
        }
        Double distance = EasyWayLocation.calculateDistance(lat1,long1,lat2,long2);
        Double inKM = distance/1000;
        return Math.floor(inKM * 100) / 100;
    }

    public static void bookMark(String id,String type,Context mContext){
        if(TextUtils.isEmpty(id))return;
        final Context tempContext = mContext;
        String url = MConstant.API_END+"/addBookmark";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("id",id);
        params.put("user_id",MFunction.getMyPrefVal("user_id",tempContext));
        params.put("type","2");
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                String msg = response.getString("message");
                                Toast.makeText(tempContext, msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }





    /**************/

    public static String getStringVal(JsonObject obj , String member){

        if(!obj.has(member)) return "";
        JsonElement elem = obj.get(member);
        return elem.isJsonNull()?"":elem.getAsString();
    }

    public static boolean getBooleanVal(JsonObject obj , String member){
        if(!obj.has(member)) return false;
        JsonElement elem = obj.get(member);
        return elem.isJsonNull()?false:elem.getAsBoolean();
    }

    public static String getArrayVal(JsonObject obj , String member){
        if(!obj.has(member)) return "";
        JsonElement elem = obj.get(member);
        return elem.isJsonNull()?"":elem.toString();
    }

    public static String getDeviceInfo() {

        String manufacturer = Build.MANUFACTURER;

        String model = Build.MODEL + " " + android.os.Build.BRAND + " ("
                + android.os.Build.VERSION.RELEASE + ")"
                + " API-" + android.os.Build.VERSION.SDK_INT;

        if (model.startsWith(manufacturer)) {
            return model.toUpperCase();
        } else {
            return manufacturer.toUpperCase() + " " + model;
        }

    }

}
