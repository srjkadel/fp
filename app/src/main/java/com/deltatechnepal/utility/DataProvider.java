package com.deltatechnepal.utility;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.foodpal.LoginActivity;
import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.model.BlogModel;
import com.deltatechnepal.ormmodel.Banner;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.ormmodel.Foodfact;
import com.deltatechnepal.ormmodel.PhotoGallery;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.ormmodel.RestoCat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DeltaTech on 4/9/2018.
 */

public class DataProvider {
    public static void getAllResto(Context context) {
        final Context mContext = context;
        String url = MConstant.API_END+"/getRestaurants";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("limit","100");
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("mResponse",response.toString());
                        try {
                            JSONArray restoArray = response.getJSONArray("data");
                            ActiveAndroid.beginTransaction();
                             for(int i = 0;i<restoArray.length();i++) {
                                 JSONObject obj = restoArray.getJSONObject(i);
                                 Resto resto = Resto.getItem(obj.getString("id"));
                                 boolean doUpdate = false;
                                 if(resto != null){
                                     doUpdate = true;
                                 } else {
                                     resto = new Resto();
                                 }
                                 resto.resto_id = obj.getString("id");
                                 resto.name = obj.getString("name");
                                 resto.slug = obj.getString("slug");
                                 resto.email = obj.getString("email");
                                 resto.website = obj.getString("website");
                                 resto.category = obj.getString("category");
                                 resto.banner_image = obj.getString("banner_image");
                                 resto.image = obj.getString("image");
                                 resto.city = obj.getString("city");
                                 resto.state = obj.getString("state");
                                 resto.country = obj.getString("country");
                                 resto.zipcode = obj.getString("zipcode");
                                 resto.mobile = obj.getString("mobile");
                                 resto.created_by = obj.getString("created_by");
                                 resto.payment = obj.getString("payment");
                                 resto.average_cost = obj.getString("average_cost");
                                 resto.phone = obj.getString("phone");
                                 resto.timing = obj.getString("timing");;
                                 resto.seating = obj.getString("seating");
                                 resto.tags = obj.getString("tags");
                                 resto.owner = obj.getString("owner");
                                 resto.status = obj.getString("status");
                                 resto.about = obj.getString("about");
                                 resto.IsSponsored = obj.getString("IsSponsored");
                                 resto.facebookUrl = obj.getString("facebookUrl");
                                 resto.instagramUrl = obj.getString("instagramUrl");
                                 resto.lati = obj.getString("lat");
                                 resto.longi = obj.getString("log");
                                 resto.formatted_address = obj.getString("formatted_address");
                                 resto.recomended = obj.getString("recomended");
                                 resto.addedByUser = obj.getString("addedByUser");
                                 resto.r_rating = obj.getString("r_rating");
                                 resto.created_at = obj.getString("created_at");
                                 //resto.updated_at = obj.getString("");
                                 resto.country_sortname = obj.getString("country_sortname");
                                 resto.country_name = obj.getString("country_name");
                                 resto.country_phone_code = obj.getString("country_phone_code");
                                 resto.state_name = obj.getString("state_name");
                                 resto.cat_name = obj.getString("cat_name");
                                 resto.cat_summary = obj.getString("cat_summary");
                                 resto.cuisines = obj.getString("cuisines");
                                 resto.additional_info = obj.getString("additional_info");
                                 resto.photogallery = obj.getString("photogallery");
                                 resto.reviews = obj.getString("reviews");
                                 resto.total_recommendation = obj.getString("total_recommendation");
                                 resto.average_rating = obj.getString("average_rating");
                                 if(doUpdate) {
                                     //do nothing currently
                                     resto.save();

                                 } else {
                                     resto.save();
                                 }
                             }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void getAllRestoCat(Context context) {
        final Context mContext = context;
        String url = MConstant.API_END+"/getAllRestoCat";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objParamData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objParamData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray catArray = response.getJSONArray("data");
                            ActiveAndroid.beginTransaction();
                            for(int i = 0;i<catArray.length();i++) {
                                JSONObject obj = catArray.getJSONObject(i);
                                RestoCat restoCat = RestoCat.getItem(obj.getString("id"));
                                if(restoCat == null) { restoCat = new RestoCat();}

                                restoCat.cat_id = obj.getString("id");
                                restoCat.name = obj.getString("name");
                                restoCat.summary = obj.optString ("summary");
                                restoCat.status = obj.getString("status");
                                restoCat.created_at = obj.getString("created_at");
                                restoCat.save();
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void getAllRestoPhoto(Context context) {
        final Context mContext = context;
        String url = MConstant.API_END+"/getAllRestoPhoto";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray restoArray = response.getJSONArray("data");
                            ActiveAndroid.beginTransaction();
                            for(int i = 0;i<restoArray.length();i++) {
                                JSONObject obj = restoArray.getJSONObject(i);
                                PhotoGallery photoGallery = PhotoGallery.getItem(obj.getString("id"));
                                boolean doUpdate = false;
                                if(photoGallery != null){
                                    doUpdate = true;
                                } else {
                                    photoGallery = new PhotoGallery();
                                }
                                photoGallery.photo_id = obj.getString("id");
                                photoGallery.restaurant_id = obj.getString("restaurant_id");
                                photoGallery.title = obj.getString("title");
                                photoGallery.image = obj.getString("image");
                                photoGallery.status = obj.getString("status");
                                photoGallery.isMenu = obj.getString("isMenu");
                                photoGallery.isHighLight = obj.getString("isHighlight");
                                photoGallery.isMustTry = obj.getString("isMustTry");
                                photoGallery.created_at = obj.getString("created_at");
                                long saved = photoGallery.save();
                                Log.i("Photo gallery Saved", ""+saved);
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void getBannerAndFoodFact(Context context) {
        final Context mContext = context;
        String url = MConstant.API_END+"/getBannerAndFoodFact";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objParamData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objParamData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            Log.i("response", response.toString());
                            if(status) {
                                JSONObject data = response.getJSONObject("data");
                                JSONObject foodFact = data.getJSONObject("foodfact");
                                JSONArray bannerArray = data.getJSONArray("banners");
                                ActiveAndroid.beginTransaction();
                                Foodfact ff = Foodfact.getFoodfact();
                                if(ff == null) {
                                    ff = new Foodfact();
                                }
                                ff.ff_id = "1";
                                ff.title = foodFact.getString("title");
                                ff.content = foodFact.getString("fact");
                                Long saved = ff.save();
                                Log.i("FoodFact","Saved");

                                for(int i = 0;i<bannerArray.length();i++) {
                                    JSONObject obj = bannerArray.getJSONObject(i);
                                    Banner banner = Banner.getItem(obj.getString("id"));
                                    if(banner == null) { banner = new Banner();}
                                    banner.banner_id = obj.getString("id");
                                    banner.title = obj.getString("title");
                                    banner.image = obj.getString("image");
                                    banner.banner_order = obj.getString("banner_order");
                                    banner.status = obj.getString("status");
                                    banner.caption = obj.getString("caption");
                                    banner.link = obj.getString("link");
                                    Long bsaved = banner.save();
                                    Log.i("BannerSaved", ""+bsaved);
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Banner Fetch",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void getAllEvent(Context context) {
        final Context mContext = context;
        String url = MConstant.API_END+"/getAllEvent";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objParamData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objParamData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray eventArray = response.getJSONArray("data");
                            ActiveAndroid.beginTransaction();
                            for(int i = 0;i<eventArray.length();i++) {
                                JSONObject obj = eventArray.getJSONObject(i);
                                Event event = Event.getItem(obj.getString("id"));
                                boolean doUpdate = false;
                                if(event == null) { event = new Event();}
                                event.event_id = obj.getString("id");
                                event.name = obj.getString("name");
                                event.slug = obj.getString("slug");
                                event.image = obj.getString("image");
                                event.start_date = obj.getString("start_date");
                                event.end_date = obj.getString("end_date");
                                event.location = obj.getString("location");
                                event.country = obj.getString("country");
                                event.state = obj.getString("state");
                                event.city = obj.getString("city");
                                event.description = obj.getString("description");
                                event.activities = obj.getString("activities");
                                event.contact_no = obj.getString("contact_no");
                                event.ticket_info = obj.getString("ticket_info");
                                event.time = obj.getString("time");
                                event.category = obj.getString("category");
                                event.status = obj.getString("status");
                                event.IsSponsered = obj.getString("IsSponsered");
                                event.website = obj.getString("website");
                                event.lati = obj.getString("lat");
                                event.longi = obj.getString("log");
                                event.formatted_address = obj.getString("formatted_address");
                                event.created_at = obj.getString("created_at");
                                event.category_name = obj.getString("category_name");
                                event.save();
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static List<BlogModel> getAllBlog(Context context,String author) {
        final List<BlogModel> blogList = new ArrayList<BlogModel>();
        final Context mContext = context;
        String url = MConstant.API_END+"/getAllBlog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("author",author);
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray eventArray = response.getJSONArray("data");
                            for(int i = 0;i<eventArray.length();i++) {
                                JSONObject obj = eventArray.getJSONObject(i);
                                BlogModel blog = new BlogModel();
                                blog.setBlogID(obj.getString("id"));
                                blog.setAuthor(obj.getString("author"));
                                blog.setTitle(obj.getString("title"));
                                blog.setSlug(obj.getString("slug"));
                                blog.setSummary(obj.getString("summary"));
                                blog.setDetail(obj.getString("details"));
                                blog.setImage(obj.getString("image"));
                                blog.setCreatedAt(obj.getString("created_at"));
                                //blog.setPublish_date(obj.getString("published_at"));
                                blog.setReadingTime(obj.getString("readingtime"));
                                blog.setStatus(obj.getString("status"));
                                blog.setTags(obj.getString("tags"));
                                blog.setCreatorRole(obj.getString("creator_role"));
                                blogList.add(blog);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
        return blogList;
    }
}
