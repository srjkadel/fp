package com.deltatechnepal.utility;

/**
 * Created by DeltaTech on 1/18/2018.
 */


public class MConstant {
    public static final String APP_NAME = "FoodPal";
    public static final String MY_APP_ID = "com.deltatechnepal.foodpal";
    public static final String DEVELOPER_URL = "http://www.deltatechnepal.com";
    public static final String HELP_LINE_NUM = "";

    //Server
    //public static final String SERVER = "https://192.168.254.12/foodpal";
   // public static final String SERVER = "https://208.91.198.26";
    //public static final String SERVER = "https://staging2.deltatech.com.np/foodpal";

   // public static final String SERVER = "https://foodpal.com.np";
    //public static final String SERVER = "http://10.0.2.2/php-foodpal";//local server
   public static final String SERVER = "http://192.168.1.88/php-foodpal";//local server

    public static final String API_ID_ACCESS_TOKEN_STRING = "qwertyuiopasdfghjklzxcv";
    public static final String API_END = SERVER + "/api";

    /*For Encryption*/
    public static final String PREFERENCE_NAME = "foodpal-preference";
    public static final String PREFERENCE_ENCRYPT_KEY = "foodpal";

    /*Home Page(Nearby Activity)*/
    public static final int BANNER_TRANSITION_DURATION = 8000;
    public static final float NEARBY_DISTANCE_FILTER_LIMIT = 5000; //in meter

    /*Geofencing Related*/
    public static final String GEOFENCE_ID = "MY TESTING RESTAURANT";
    public static final double GEOFENCE_LAT = 26.466311;
    public static final double GEOFENCE_LONG = 87.2835156;
    public static final int GEOFENCE_RADIUS = 5000;


    //Geofencing extra testing

    public static final String GEOFENCE_ID2 = "Bargachi RESTAURANT";
    public static final double GEOFENCE_LAT2 = 26.472950;
    public static final double GEOFENCE_LONG2 = 87.276419;
    public static final int GEOFENCE_RADIUS2 = 500;


    //Notification Type

    public static final String NOTIFICATION_TYPE_RESTAURANT = "restaurant";
    public static final String NOTIFICATION_TYPE_EVENT = "event";
    public static final String NOTIFICATION_TYPE_BLOG = "blog";
    public static final String NOTIFICATION_TYPE_REMINDER = "reminder";

}













