package com.deltatechnepal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static com.deltatechnepal.foodpal.MainActivity.TAG;
import static com.deltatechnepal.foodpal.R.layout.recycler_item_profile_bookmark;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class ProfileBookmarkAdapter extends RecyclerView.Adapter<ProfileBookmarkAdapter.ViewHolder>{
    private JsonArray jsonArray;
    private Context mContext;

    public ProfileBookmarkAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView ivBookmark;
        public TextView tvTitle,tvType,tvDate;
        public Button btnRemove;
        public ViewHolder(View v) {
            super(v);
            ivBookmark =(ImageView) v.findViewById(R.id.ivBookmark);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvType = (TextView) v.findViewById(R.id.tvType);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            btnRemove = (Button) v.findViewById(R.id.btnRemove);
        }
    }

    @Override
    public ProfileBookmarkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(recycler_item_profile_bookmark, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileBookmarkAdapter.ViewHolder holder, int position) {
        final int tempPosition = position;
        final ViewHolder tempHolder = holder;
        JsonElement jsonElement = jsonArray.get(position);
        JsonObject tempBookmark;
        JsonObject obj = jsonElement.getAsJsonObject();
        try {
             tempBookmark = obj.getAsJsonObject("bookmark");
        }catch (Error error){
            tempBookmark = new JsonObject();
            Log.d(TAG, "onBindViewHolder: "+error.toString());
        }
        String type = obj.get("type").getAsString();
        /*setting recycler item according to type of bookmark , bookmark:0,restaurant:1,event:2*/
        switch(type){
            case "0":{
                String imgUrl = MConstant.SERVER+"/uploads/blogs/262x183"+tempBookmark.get("image").getAsString();
                Picasso.with(mContext)
                        .load(imgUrl)
                        .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                        .error(mContext.getResources().getDrawable(R.drawable.default_image))
                        .fit()
                        .into(holder.ivBookmark);
                holder.tvTitle.setText(tempBookmark.get("title").getAsString());
                holder.tvType.setText("Blog");

                /*Image Click*/
                /*holder.ivBookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext,BlogDetailActivity.class);
                        intent.putExtra("blog_id",tempBookmark.get("blog_id").getAsString());
                        mContext.startActivity(intent);
                        int position = getAdapterPosition();
                        Log.i("adapterPosition",""+position);
                        JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                        JsonObject tempBookmark = jsonElement.getAsJsonObject();
                        Intent intent = new Intent(mContext,BlogDetailActivity.class);
                        String blog = tempBookmark.toString();
                        intent.putExtra("blog",blog);
                        mContext.startActivity(intent);
                    }
                });*/
                break;
            }
            case "1":{
                String imgUrl = MConstant.SERVER+"/uploads/restaurantimage/148x111"+tempBookmark.get("image").getAsString();
                Picasso.with(mContext)
                        .load(imgUrl)
                        .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                        .error(mContext.getResources().getDrawable(R.drawable.default_image))
                        .fit()
                        .into(holder.ivBookmark);
                holder.tvTitle.setText(tempBookmark.get("name").getAsString());
                holder.tvType.setText("Restaurant");
                break;
            }
            case "2":{
                String imgUrl = MConstant.SERVER+"/uploads/events/150x150"+tempBookmark.get("image").getAsString();
                Picasso.with(mContext)
                        .load(imgUrl)
                        .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                        .error(mContext.getResources().getDrawable(R.drawable.default_image))
                        .fit()
                        .into(holder.ivBookmark);
                holder.tvTitle.setText(tempBookmark.get("name").getAsString());
                holder.tvType.setText("Event");
                break;
            }
        }

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeBookmark(tempPosition);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != jsonArray ? jsonArray.size() : 0);
    }

    /*private void removeBookmark(int tempPosition){
        JsonElement jsonElement = jsonArray.get(tempPosition);
        JsonObject tempBookmark = jsonElement.getAsJsonObject();
        Log.i("mTempBookmark", tempBookmark.toString());
        if(true) {
            jsonArray.remove(tempPosition);
            notifyItemRemoved(tempPosition);
            notifyItemRangeChanged(tempPosition,jsonArray.size());
        }
    }*/

    private void removeBookmark(int position){
        final int tempPosition = position;
        JsonElement jsonElement = jsonArray.get(position);
        JsonObject tempBookmark = jsonElement.getAsJsonObject();
        String type = tempBookmark.get("type").getAsString();
        String tempID = tempBookmark.get("rest_blog_id").getAsString();

        String url = MConstant.API_END+"/removeBookmark";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("id",tempID);
        params.put("user_id", MFunction.getMyPrefVal("user_id",mContext));
        params.put("type",type);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                jsonArray.remove(tempPosition);
                                notifyItemRemoved(tempPosition);
                                notifyItemRangeChanged(tempPosition,jsonArray.size());
                                String msg = response.getString("message");
                                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}








































