package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.foodpal.BlogDetailActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CommonPopularBlogAdapter extends RecyclerView.Adapter<CommonPopularBlogAdapter.ViewHolder> {

    private JsonArray jsonArray;
    private Context mContext;

    public CommonPopularBlogAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_nearby_blog, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        JsonElement jsonElement = jsonArray.get(i);
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        String imgUrl = MConstant.SERVER+"/uploads/blogs/150x150"+jsonObj.get("image").getAsString();
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivBlog);
        holder.tvName.setText(jsonObj.get("title").getAsString());
        holder.tvBlogger.setText(jsonObj.get("first_name").getAsString()+" "+jsonObj.get("last_name").getAsString());
        holder.tvDate.setText(MFunction.getFormattedDate(jsonObj.get("publish_date").getAsString(),mContext));
    }

    @Override
    public int getItemCount() {
        return (null != jsonArray ? jsonArray.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivBlog;
        protected TextView tvName,tvBlogger,tvDate;

        public ViewHolder(View view) {
            super(view);

            this.ivBlog = (ImageView) view.findViewById(R.id.ivBlog);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvBlogger = (TextView) view.findViewById(R.id.tvBlogger);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Log.i("adapterPosition",""+position);
                    JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                    JsonObject jsonObj = jsonElement.getAsJsonObject();
                    Intent intent = new Intent(mContext,BlogDetailActivity.class);
                    String blog = jsonObj.toString();
                    intent.putExtra("blog",blog);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
