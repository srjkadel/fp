package com.deltatechnepal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.foodpal.R;

public class GridviewAdapter extends BaseAdapter {

    private Context mContext;
    private final String[] gridViewString;
    private final int[] gridViewImageId;
    boolean containsText;

    public GridviewAdapter(Context context,boolean containsText, String[] gridViewString, int[] gridViewImageId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gridViewString = gridViewString;
        this.containsText=containsText;
    }

    @Override
    public int getCount() {
        return gridViewImageId.length;

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridViewAndroid = new View(mContext);
            if(containsText) {
                gridViewAndroid = inflater.inflate(R.layout.gridview_layout, null);
                TextView textViewAndroid =  gridViewAndroid.findViewById(R.id.android_gridview_text);
                textViewAndroid.setText(gridViewString[i]);

            }
            else{
                gridViewAndroid = inflater.inflate(R.layout.cuisines_grid_layout, null);
            }


            ImageView imageViewAndroid =  gridViewAndroid.findViewById(R.id.android_gridview_image);
            imageViewAndroid.setImageResource(gridViewImageId[i]);





        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }
}