package com.deltatechnepal.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by DeltaTech on 4/11/2018.
 */

public class HomeGridAdapter extends BaseAdapter{
    private Context mContext;
    private List<HashMap> qsList;
    private boolean containsText;

    public HomeGridAdapter(Context context, boolean containsText, List<HashMap> qsList) {
        this.mContext = context;
        this.qsList = qsList;
        this.containsText=containsText;
    }

    @Override
    public int getCount() {
        return qsList.size();
    }

    @Override
    public Object getItem(int i) {
        return qsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        HashMap map = qsList.get(i);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            if(containsText)
            view  = inflater.inflate(R.layout.gridview_layout, null);
            else
                view  = inflater.inflate(R.layout.cuisines_home_grid_layout, null);

        }
        ImageView ivQsCategory = (ImageView) view.findViewById(R.id.android_gridview_image);

        int drawableImage = mContext.getResources().getIdentifier(map.get("image").toString(),"mipmap", mContext.getPackageName());
        Picasso.with(mContext)
                .load(drawableImage)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(ivQsCategory);

        if(containsText) {
            final TextView tvCategory = (TextView) view.findViewById(R.id.android_gridview_text);
            tvCategory.setText(map.get("cat_name").toString());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent cIntent = new Intent(mContext, MainActivity.class);
                    cIntent.putExtra("search_category", true);

                    cIntent.putExtra("category", tvCategory.getText().toString());

                    mContext.startActivity(cIntent);

                }
            });
        }
        return view;
    }
}
