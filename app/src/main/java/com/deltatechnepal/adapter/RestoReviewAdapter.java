package com.deltatechnepal.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.circularindicator.CircularIndicator;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class RestoReviewAdapter extends RecyclerView.Adapter<RestoReviewAdapter.ViewHolder> {
    private JsonArray jsonArray;
    private Context mContext;

    public RestoReviewAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ViewGroup vg = viewGroup;
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row_resto_review,viewGroup,false);
        ViewHolder mh = new ViewHolder(v);
        return mh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        JsonElement jsonElement = jsonArray.get(i);
        Log.i("mElement","mElement");
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        /*String imgUrl = MConstant.SERVER+"/uploads/avatars/members/150x150"+jsonObj.get("avatar");
        Picasso.with(mContext)
                .load(imgUrl)
                //.placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                //.error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivReview);*/
        holder.tvReviewer.setText(jsonObj.get("first_name").getAsString()+" "+jsonObj.get("last_name").getAsString());
        String reviewedDate = MFunction.getFormattedDate(jsonObj.get("review_date").getAsString(),mContext);

        holder.tvReviewTime.setText(reviewedDate);
        String userType = (TextUtils.equals(jsonObj.get("blogger").toString(),"1"))?"Blogger":"";
        holder.tvUserType.setText(userType);
        /*Setting Rating Bar Value*/
        int food = jsonObj.get("food").getAsInt();
        int service = jsonObj.get("service").getAsInt();
        int ambience = jsonObj.get("ambience").getAsInt();
        int cleanliness = jsonObj.get("cleanliness").getAsInt();
        int overall = (food+service+ambience+cleanliness)/4;
        holder.rbFood.setProgress(food,5);
        holder.rbFood.setProgressText(MFunction.getFormattedProgressText(food));

        holder.rbService.setProgress(service,5);
        holder.rbService.setProgressText(MFunction.getFormattedProgressText(service));

        holder.rbAmbience.setProgress(ambience,5);
        holder.rbAmbience.setProgressText(MFunction.getFormattedProgressText(ambience));


        holder.rbCleanliness.setProgress(cleanliness,5);
        holder.rbCleanliness.setProgressText(MFunction.getFormattedProgressText(cleanliness));

        holder.rbOverall.setProgress(overall,5);
        holder.rbOverall.setProgressText(MFunction.getFormattedProgressText(overall));

        holder.tvContent.setText(jsonObj.get("comment").getAsString());
    }

    @Override
    public int getItemCount() {

        int count =  (null != jsonArray ? jsonArray.size() : 0);
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivReview;
        protected TextView tvReviewer,tvReviewTime,tvUserType,tvNumberOfReview,tvContent;
        protected CircularIndicator rbFood, rbService,rbAmbience,rbCleanliness,rbOverall;

        public ViewHolder(View view) {
            super(view);
            this.ivReview       = (ImageView) view.findViewById(R.id.ivReview);
            this.tvReviewer     = (TextView) view.findViewById(R.id.tvReviewer);
            this.tvReviewTime   = (TextView) view.findViewById(R.id.tvReviewTime);
            this.tvUserType     = (TextView) view.findViewById(R.id.tvUserType);
            this.tvContent     = (TextView) view.findViewById(R.id.tvContent);
            this.tvNumberOfReview = (TextView) view.findViewById(R.id.tvNumberOfReview);
            this.rbFood         = (CircularIndicator) view.findViewById(R.id.rbFood);
            this.rbService      = (CircularIndicator) view.findViewById(R.id.rbService);
            this.rbAmbience     = (CircularIndicator) view.findViewById(R.id.rbAmbience);
            this.rbCleanliness  = (CircularIndicator) view.findViewById(R.id.rbCleanliness);
            this.rbOverall  = (CircularIndicator) view.findViewById(R.id.rbOverall);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*PhotoGallery photoGallery = reviewList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    intent.putExtra("photo_id",photoGallery.photo_id);
                    mContext.startActivity(intent);*/
                    return;
                }
            });
        }
    }
}