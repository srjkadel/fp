package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.foodpal.BlogDetailActivity;
import com.deltatechnepal.foodpal.EventDetailActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NearbyBlogAdapter extends RecyclerView.Adapter<NearbyBlogAdapter.ViewHolder> {

    private List<Blog> blogList;
    private Context mContext;

    public NearbyBlogAdapter(Context context, List<Blog> blogList) {
        this.blogList = blogList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_nearby_blog, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Blog blog = blogList.get(i);

        String imgUrl = MConstant.SERVER+"/uploads/event/1350x500"+blog.image;
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivBlog);
        holder.tvName.setText(blog.title);
        holder.tvBlogger.setText(blog.blog_id);
        holder.tvDate.setText(blog.publish_date);
    }

    @Override
    public int getItemCount() {
        return (null != blogList ? blogList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivBlog;
        protected TextView tvName,tvBlogger,tvDate;

        public ViewHolder(View view) {
            super(view);

            this.ivBlog = (ImageView) view.findViewById(R.id.ivBlog);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvBlogger = (TextView) view.findViewById(R.id.tvBlogger);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Blog blog = blogList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,BlogDetailActivity.class);
                    intent.putExtra("blog_id",blog.blog_id);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
