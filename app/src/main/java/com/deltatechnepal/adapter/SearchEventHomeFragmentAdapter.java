package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.EventDetailActivity;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static java.lang.Math.abs;

public class SearchEventHomeFragmentAdapter extends RecyclerView.Adapter<SearchEventHomeFragmentAdapter.ViewHolder> {

    private JsonArray jsonArray;
    private Context mContext;
    private boolean online;
    private  boolean isSearch;
    List<Event> eventList;

    public SearchEventHomeFragmentAdapter(Context context, JsonArray jsonArray,boolean isSearch) {
        this.jsonArray = jsonArray;
        this.mContext = context;
        this.online = true;
        this.isSearch = isSearch;
    }
    public SearchEventHomeFragmentAdapter(Context context, List eventList,boolean isSearch) {
        this.jsonArray = jsonArray;
        this.mContext = context;
        this.online = false;
        this.isSearch= isSearch;
        this.eventList=eventList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycleritem_event_homefragment_search_row, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        if (online) {

            JsonElement jsonElement = jsonArray.get(i);
            JsonObject event = jsonElement.getAsJsonObject();

            String imgUrl = MConstant.SERVER + "/uploads/events/150x150" + event.get("image").getAsString();
            Picasso.with(mContext)
                    .load(imgUrl)
                    .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                    .error(mContext.getResources().getDrawable(R.drawable.default_image))
                    .fit()
                    .into(holder.ivEvent);
            holder.tvName.setText(event.get("name").getAsString());
            holder.tvDate.setText(MFunction.getFormattedDate(event.get("start_date").getAsString(), mContext));
            holder.tvAddress.setText(event.get("formatted_address").getAsString());
            Double distance = MFunction.getDistance(mContext, event.get("lat").getAsString(), event.get("log").getAsString());
            holder.tvDistance.setText(distance.toString() + "KM Around");
        }

        else
        {
            Event tempResto = eventList.get(i);
            String imgUrl = MConstant.SERVER + "/uploads/events/" + tempResto.image;
            Picasso.with(mContext)
                    .load(imgUrl)
                    //.placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                    //.error(mContext.getResources().getDrawable(R.drawable.default_image))
                    //.fit()
                    .into(holder.ivEvent);
            holder.tvName.setText(tempResto.name);
            holder.tvDate.setText(MFunction.getFormattedDate(tempResto.start_date, mContext));
            holder.tvAddress.setText(tempResto.formatted_address);
            Double distance = MFunction.getDistance(mContext, tempResto.lati, tempResto.longi);
            holder.tvDistance.setText(distance.toString() + "KM Around");



        }

    }

    @Override
    public int getItemCount() {
        if(online){

            return (null != jsonArray ? jsonArray.size() : 0);
        } else {
            return (null != eventList ? eventList.size() : 0);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivEvent;
        protected TextView tvName,tvDate,tvAddress,tvDistance;

        public ViewHolder(View view) {
            super(view);

            this.ivEvent = (ImageView) view.findViewById(R.id.ivEvent);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            this.tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            this.tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    String event = "";

                    if(online){
                        JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                        JsonObject jsonObj = jsonElement.getAsJsonObject();
                        event = jsonObj.toString();

                    } else {
                        return;
                    }
                    Intent intent = new Intent(mContext,EventDetailActivity.class);
                    intent.putExtra("event",event);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    private String getFormattedDate(String tempDate){
        String mDate = tempDate;
        SimpleDateFormat fromDFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat toDFormat=new SimpleDateFormat("d MMM , h:mm a");
        String finalDate= null;
        int fromYear=0;
        int fromMonth=0;
        int fromDay=0;
        long dbSeconds =0;
        double showHours = 0;
        double showMinutes =0;
        double showSeconds =0;
        double mRemainder =0;
        try {
            Date date = fromDFormat.parse(mDate);
            /* for getting yyyy,mm,dd*/
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            fromYear = calendar.get(Calendar.YEAR);
            fromMonth = calendar.get(Calendar.MONTH) + 1;
            fromDay = calendar.get(Calendar.DAY_OF_MONTH);
            dbSeconds = date.getTime()/1000;
            finalDate=   toDFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        long currentSeconds = new Date().getTime()/1000;
        long diffSeconds = currentSeconds - dbSeconds;

        if (diffSeconds < (long)86400 && diffSeconds>= 0) {
            showHours = (double)diffSeconds/(double) 3600;
            int intHrPart = (int) showHours;
            showMinutes = (showHours - (double)intHrPart) * (double)60;
            int intMPart = (int) showMinutes;
            showSeconds = (showMinutes - (double) intMPart) * (double)60;
            int intSPart = (int) showSeconds;
            String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
            String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
            String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
            finalDate   = hr + m + s + "ago";
        } else {
            diffSeconds = abs(diffSeconds);
            if(diffSeconds>86400){
                finalDate = finalDate;
            } else {
                showHours = (double)diffSeconds/(double) 3600;
                int intHrPart = (int) showHours;
                showMinutes = (showHours - (double)intHrPart) * (double)60;
                int intMPart = (int) showMinutes;
                showSeconds = (showMinutes - (double) intMPart) * (double)60;
                int intSPart = (int) showSeconds;
                String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
                String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
                String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
                finalDate   = hr + m + s + "left";
            }
        }
        return finalDate;
    }
}
