package com.deltatechnepal.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.foodpal.BlogDetailActivity;
import com.deltatechnepal.foodpal.EventDetailActivity;
import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.RestoDetailActivity;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by DeltaTech on 4/11/2018.
 */

public class ExplorerAdapter extends BaseAdapter{

    String TAG = "ExplorerAdapter";
    private Context mContext;
    private List<HashMap> explorerList;

    public ExplorerAdapter(Context context, List<HashMap> explorerList) {
        this.mContext = context;
        this.explorerList = explorerList;
    }

    @Override
    public int getCount() {
        return explorerList.size();
    }

    @Override
    public Object getItem(int i) {
        return explorerList.get(i);
        //return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
        //return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        HashMap map = explorerList.get(i);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view  = inflater.inflate(R.layout.grid_item_explorer, null);
        }
        Log.i(TAG, "getView: ");
        ImageView ivCategory = (ImageView) view.findViewById(R.id.ivCategory);
        final TextView tvCategory = (TextView) view.findViewById(R.id.tvCategory);
        tvCategory.setTypeface(null, Typeface.BOLD);
        final String catID = map.get("cat_id").toString();
        int drawableImage = mContext.getResources().getIdentifier(map.get("image").toString(),"mipmap", mContext.getPackageName());
        Picasso.with(mContext)
                .load(drawableImage)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(ivCategory);
        tvCategory.setText(map.get("cat_name").toString());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cIntent = new Intent(mContext, MainActivity.class);
                cIntent.putExtra("search_category", true);
                cIntent.putExtra("category", tvCategory.getText().toString());
                mContext.startActivity(cIntent);

            }
        });
        return view;
    }
}
