package com.deltatechnepal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.utility.MConstant;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import static com.deltatechnepal.foodpal.R.layout.recycler_item_profile_blog;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class ProfileBlogAdapter extends RecyclerView.Adapter<ProfileBlogAdapter.ViewHolder>{
    private JsonArray jsonArray;
    private Context mContext;

    public ProfileBlogAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView ivBlog;
        public TextView tvTitle,tvTimeBlogger,tvReading;
        public ViewHolder(View v) {
            super(v);
            ivBlog =(ImageView) v.findViewById(R.id.ivBlog);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            /*tvTimeBlogger = (TextView) v.findViewById(R.id.tvTimeBlogger);
            tvReading = (TextView) v.findViewById(R.id.tvReading);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                    JsonObject jsonObj = jsonElement.getAsJsonObject();
                    Intent intent = new Intent(mContext,BlogDetailActivity.class);
                    String blog = jsonObj.toString();
                    intent.putExtra("blog",blog);
                    mContext.startActivity(intent);
                }
            });*/
        }
    }

    @Override
    public ProfileBlogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(recycler_item_profile_blog, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileBlogAdapter.ViewHolder holder, int position) {
        JsonElement jsonElement = jsonArray.get(position);
        final JsonObject jsonObj = jsonElement.getAsJsonObject();
        String imgUrl = MConstant.SERVER+"/uploads/blogs/262x183"+jsonObj.get("image").getAsString();

        /*Setting Image*/
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivBlog);

        /*Setting Title*/
        holder.tvTitle.setText(jsonObj.get("title").getAsString());

        //String publishedDate = getFormattedDate(jsonObj.get("publish_date").getAsString());
        //String bloggerName = jsonObj.get("first_name").getAsString()+" "+jsonObj.get("last_name").getAsString();
        /*Setting publish date and author name*/
        //holder.tvTimeBlogger.setText(publishedDate +"  by "+bloggerName);

        /*Setting Reading Time*/
        //holder.tvReading.setText(jsonObj.get("readingtime").getAsString()+" min read");

        /*Image Click*/
        /*holder.ivBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,BlogDetailActivity.class);
                intent.putExtra("blog_id",jsonObj.get("blog_id").getAsString());
                mContext.startActivity(intent);

                int position = getAdapterPosition();
                Log.i("adapterPosition",""+position);
                JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                JsonObject jsonObj = jsonElement.getAsJsonObject();
                Intent intent = new Intent(mContext,BlogDetailActivity.class);
                String blog = jsonObj.toString();
                intent.putExtra("blog",blog);
                mContext.startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return (null != jsonArray ? jsonArray.size() : 0);
    }

    public String getFormattedDate(String tempDate){
        String mDate = tempDate;
        SimpleDateFormat fromDFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat toDFormat=new SimpleDateFormat("d MMM , h:mm a");
        String finalDate= null;
        int fromYear=0;
        int fromMonth=0;
        int fromDay=0;
        long dbSeconds =0;
        double showHours = 0;
        double showMinutes =0;
        double showSeconds =0;
        double mRemainder =0;
        try {
            Date date = fromDFormat.parse(mDate);
            /* for getting yyyy,mm,dd*/
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            fromYear = calendar.get(Calendar.YEAR);
            fromMonth = calendar.get(Calendar.MONTH) + 1;
            fromDay = calendar.get(Calendar.DAY_OF_MONTH);
            dbSeconds = date.getTime()/1000;
            finalDate=   toDFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        long currentSeconds = new Date().getTime()/1000;
        long diffSeconds = currentSeconds - dbSeconds;

        if (diffSeconds < (long)86400) {
            showHours = (double)diffSeconds/(double) 3600;
            int intHrPart = (int) showHours;
            showMinutes = (showHours - (double)intHrPart) * (double)60;
            int intMPart = (int) showMinutes;
            showSeconds = (showMinutes - (double) intMPart) * (double)60;
            int intSPart = (int) showSeconds;

            String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
            String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
            String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
            finalDate   = hr + m + s + "ago";
        }
        return finalDate;
    }
}








































