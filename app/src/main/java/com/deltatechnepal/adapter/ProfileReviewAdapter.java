package com.deltatechnepal.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.circularindicator.CircularIndicator;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

public class ProfileReviewAdapter extends RecyclerView.Adapter<ProfileReviewAdapter.ViewHolder> {
    private JsonArray jsonArray;
    private Context mContext;

    public ProfileReviewAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ViewGroup vg = viewGroup;
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_profile_review,viewGroup,false);
        ViewHolder mh = new ViewHolder(v);
        return mh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        JsonElement jsonElement = jsonArray.get(i);
        Log.i("mElement","mElement");
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        String imgUrl = MConstant.SERVER+"/uploads/restaurantimage/148x111"+jsonObj.get("resto_image").getAsString();
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivResto);

        holder.tvRestaurantName.setText(jsonObj.get("resto_name").getAsString());

        String reviewedDate = MFunction.getFormattedDate(jsonObj.get("review_date").getAsString(),mContext);
        holder.tvTime.setText(reviewedDate);

        String status = (TextUtils.equals(jsonObj.get("status").getAsString(),"1"))?"Active":"Pending";
        holder.tvStatus.setText(status);

        /*Setting Rating Bar Value*/
        int food = jsonObj.get("food").getAsInt();
        int service = jsonObj.get("service").getAsInt();
        int ambience = jsonObj.get("ambience").getAsInt();
        int cleanliness = jsonObj.get("cleanliness").getAsInt();
        int overall = (food+service+ambience+cleanliness)/4;
        holder.rbFood.setProgress(food,5);
        holder.rbFood.setProgressText(MFunction.getFormattedProgressText(food));

        holder.rbService.setProgress(service,5);
        holder.rbService.setProgressText(MFunction.getFormattedProgressText(service));

        holder.rbAmbience.setProgress(ambience,5);
        holder.rbAmbience.setProgressText(MFunction.getFormattedProgressText(ambience));


        holder.rbCleanliness.setProgress(cleanliness,5);
        holder.rbCleanliness.setProgressText(MFunction.getFormattedProgressText(cleanliness));

        holder.rbOverall.setProgress(overall,5);
        holder.rbOverall.setProgressText(MFunction.getFormattedProgressText(overall));

        holder.tvReviewDescription.setText(jsonObj.get("comment").getAsString());

       /* holder.ivAttachedImage1.setImageBitmap();
        holder.ivAttachedImage2.setImageBitmap();
        holder.ivAttachedImage3.setImageBitmap();
        holder.ivAttachedImage4.setImageBitmap();*/

    }

    @Override
    public int getItemCount() {

        int count =  (null != jsonArray ? jsonArray.size() : 0);
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivResto;
        protected TextView tvRestaurantName,tvTime,tvStatus,tvReviewDescription;
        protected CircularIndicator rbFood, rbService,rbAmbience,rbCleanliness,rbOverall;
        protected ImageView ivAttachedImage1, ivAttachedImage2,ivAttachedImage3,ivAttachedImage4;

        public ViewHolder(View view) {
            super(view);
            this.ivResto       = (ImageView) view.findViewById(R.id.ivResto);
            this.tvRestaurantName     = (TextView) view.findViewById(R.id.tvRestaurantName);
            this.tvTime   = (TextView) view.findViewById(R.id.tvTime);
            this.tvStatus   = (TextView) view.findViewById(R.id.tvStatus);

            this.rbFood         = (CircularIndicator) view.findViewById(R.id.rbFood);
            this.rbService      = (CircularIndicator) view.findViewById(R.id.rbService);
            this.rbAmbience     = (CircularIndicator) view.findViewById(R.id.rbAmbience);
            this.rbCleanliness  = (CircularIndicator) view.findViewById(R.id.rbCleanliness);
            this.rbOverall  = (CircularIndicator) view.findViewById(R.id.rbOverall);

            this.ivAttachedImage1      = view.findViewById(R.id.ivAttachedImage1);
            this.ivAttachedImage2      = view.findViewById(R.id.ivAttachedImage2);
            this.ivAttachedImage3      = view.findViewById(R.id.ivAttachedImage3);
            this.ivAttachedImage4      = view.findViewById(R.id.ivAttachedImage4);


            this.tvReviewDescription   = (TextView) view.findViewById(R.id.tvReviewDescription);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*PhotoGallery photoGallery = reviewList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    intent.putExtra("photo_id",photoGallery.photo_id);
                    mContext.startActivity(intent);*/
                    return;
                }
            });
        }
    }
}