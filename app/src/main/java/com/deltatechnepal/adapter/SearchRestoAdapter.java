package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.circularindicator.CircularIndicator;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.RestoDetailActivity;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.ormmodel.RestoCat;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchRestoAdapter extends RecyclerView.Adapter<SearchRestoAdapter.ViewHolder> {

    private JsonArray jsonArray;
    private List<Resto> restoList;
    private Context mContext;
    boolean online;

    public SearchRestoAdapter(Context context, JsonArray jsonArray) {
        online = true;
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    public SearchRestoAdapter() {
    }

    public SearchRestoAdapter(Context context, List<Resto> tempList) {
        online = false;
        this.restoList = tempList;
        this.mContext = context;
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_search_resto_view, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        if(online){
            JsonElement jsonElement = jsonArray.get(i);
            JsonObject jsonObj = jsonElement.getAsJsonObject();
            String imgUrl = MConstant.SERVER+"/uploads/restaurantbanner/1350x500"+jsonObj.get("banner_image").getAsString();
            Picasso.with(mContext)
                    .load(imgUrl)
                    .fit()
                    .centerCrop()
                    //.placeholder(mContext.getResources().getDrawable(R.drawable.foodpal_wide))
                    //.error(mContext.getResources().getDrawable(R.drawable.foodpal_wide))
                    .into(holder.ivResto);
            String averageRating = jsonObj.get("r_rating").getAsString();
            holder.rbResto.setProgress(avarageRatingPercentage(averageRating),100);
            holder.rbResto.setProgressText(averageRating);
            holder.tvTitle.setText(jsonObj.get("name").getAsString());
            holder.tvTitle.setSelected(true);
            holder.tvCuisin.setText(jsonObj.get("cuisines").getAsString());
            holder.tvCuisin.setSelected(true);
            holder.tvAddress.setText(jsonObj.get("formatted_address").getAsString());
            holder.tvAddress.setSelected(true);
            Double distance = MFunction.getDistance(mContext,jsonObj.get("lat").getAsString(),jsonObj.get("log").getAsString());
            holder.tvDistance.setText(distance.toString()+"KM Around");

        } else{
            Resto tempResto = restoList.get(i);
            String imgUrl = MConstant.SERVER+"/uploads/restaurantbanner/1350x500"+tempResto.banner_image;
            Picasso.with(mContext)
                    .load(imgUrl)
                    //.fit()
                    //.placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                    //.error(mContext.getResources().getDrawable(R.drawable.default_image))
                    .into(holder.ivResto);
            String averageRating = tempResto.r_rating;
            holder.rbResto.setProgress(avarageRatingPercentage(averageRating),100);
            holder.rbResto.setProgressText(averageRating);
            holder.tvTitle.setText(tempResto.name);
            holder.tvTitle.setSelected(true);
            holder.tvCuisin.setText(tempResto.cuisines);
            holder.tvCuisin.setSelected(true);
            holder.tvAddress.setText(tempResto.formatted_address);
            holder.tvAddress.setSelected(true);
            Double distance = MFunction.getDistance(mContext,tempResto.lati,tempResto.longi);
            holder.tvDistance.setText(distance.toString()+"KM Around");

        }

    }

    @Override
    public int getItemCount() {
        if(online){
            return (null != jsonArray ? jsonArray.size() : 0);
        } else {
            return (null != restoList ? restoList.size() : 0);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivResto;
        protected TextView tvTitle,tvCuisin,tvAddress,tvDistance;
        protected CircularIndicator rbResto;

        public ViewHolder(View view) {
            super(view);
            this.ivResto = (ImageView) view.findViewById(R.id.ivResto);
            this.rbResto = (CircularIndicator) view.findViewById(R.id.rbResto);
            this.tvTitle = (TextView) view.findViewById(R.id.tvName);
            this.tvCuisin = (TextView) view.findViewById(R.id.tvCuisin);
            this.tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            this.tvDistance = (TextView) view.findViewById(R.id.tvDistance);

            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    JsonElement jsonElement = jsonArray.get(getAdapterPosition());
                    JsonObject jsonObj = jsonElement.getAsJsonObject();
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    String resto = jsonObj.toString();
                    intent.putExtra("resto",resto);
                    mContext.startActivity(intent);
                }
            });*/

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    JsonObject jsonObj = null;

                    if(online){
                        JsonElement jsonElement = jsonArray.get(position);
                        jsonObj = jsonElement.getAsJsonObject();

                    } else {
                        Resto tempResto = restoList.get(position);
                        jsonObj = Resto.getJsonObjectFromResto(tempResto);
                    }
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    String resto = jsonObj.toString();
                    intent.putExtra("resto",resto);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    private int avarageRatingPercentage(String averageRating){
        float rating = Float.parseFloat(averageRating);
        float percentage = (rating/5)*100;
        return Math.round(percentage);

    }



    public void updateList(JsonArray tempArray){
        jsonArray = tempArray;
        notifyDataSetChanged();
    }
}