package com.deltatechnepal.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.foodpal.R;

public class PaneTwoAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;
    private final int[] images;
    boolean[] states;
    private boolean changeColor,hasImage;


    public PaneTwoAdapter(Context context, String[] values,int[] images,boolean changeColor,boolean hasImage,boolean[] states) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.images=images;
        this.changeColor=changeColor;
        this.hasImage=hasImage;
        this.states=states;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.pane_two_layout, parent, false);
        CheckBox checkBox =  rowView.findViewById(R.id.cbList);

        if(hasImage)
        {
            Drawable d = context.getResources().getDrawable(images[position]);
            ImageView ivRating=rowView.findViewById(R.id.ivRating);
            ivRating.setVisibility(View.VISIBLE);
            ivRating.setImageDrawable(d);
            TextView textView = rowView.findViewById(R.id.text2);
            textView.setVisibility(View.GONE);
        }
        else{

            TextView textView = rowView.findViewById(R.id.text2);
            textView.setText(values[position]);
            if(changeColor)
                textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        }
        if(states!=null && (states[position]))
            checkBox.setChecked(true);




        return rowView;
    }
}