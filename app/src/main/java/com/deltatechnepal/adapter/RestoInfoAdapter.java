package com.deltatechnepal.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.model.RestoTimingModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ayar.oktay.advancedtextview.TypeFace;

/**
 * Created by DeltaTech on 4/11/2018.
 */

public class RestoInfoAdapter extends BaseAdapter{
    private Context context;
    private final JsonArray infoArray;


    public RestoInfoAdapter(Context context, JsonArray infoArray) {
        this.context = context;
        this.infoArray = infoArray;
    }

    @Override
    public int getCount() {
        return infoArray.size();
    }

    @Override
    public Object getItem(int i) {
        return infoArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        JsonElement element = infoArray.get(i);
        JsonObject info = element.getAsJsonObject();
        String infoText = info.get("info").getAsString();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view  = inflater.inflate(R.layout.grid_item_info, null);
        }
        Drawable icon = context.getResources(). getDrawable( R.drawable.info_tick);
        //show icon to the left of text
        TextView textView = (TextView) view.findViewById(R.id.tvInfo);
        textView.setCompoundDrawablesWithIntrinsicBounds( icon, null, null, null );
        textView.setText(infoText);
        Typeface cabin = Typeface.createFromAsset(context.getAssets(),  "fonts/cabin_regular.ttf");
        textView.setTypeface(cabin);
        return view;
    }
}
