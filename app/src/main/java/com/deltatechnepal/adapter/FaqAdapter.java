package com.deltatechnepal.adapter;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.deltatechnepal.foodpal.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import com.transitionseverywhere.extra.Scale;


/**
 * Created by Ravi on 13/05/15.
 */
public class FaqAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private JsonArray jsonArray;
    private TextView temp=null;

    public FaqAdapter(Activity activity, JsonArray jsonArray) {
        this.activity = activity;
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        return jsonArray.size();
    }

    @Override
    public Object getItem(int location) {
        return jsonArray.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        JsonElement jsonElement = jsonArray.get(position);
        final JsonObject jsonObj = jsonElement.getAsJsonObject();
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.faq_list_row, null);
        final ViewGroup transitionsContainer = (ViewGroup) convertView.findViewById(R.id.container);

        Button btnTitle =  convertView.findViewById(R.id.btnTitle);
        final TextView tvContent =  convertView.findViewById(R.id.tvContent);

        btnTitle.setText(jsonObj.get("title").getAsString());
        tvContent.setText(jsonObj.get("content").getAsString());


        btnTitle.setOnClickListener(new View.OnClickListener() {

            boolean visible;

            @Override
            public void onClick(View v) {
                TransitionSet set = new TransitionSet()
                        .addTransition(new Scale(0.7f))
                        .addTransition(new Fade())
                        .setInterpolator(visible ? new LinearOutSlowInInterpolator() :
                                new FastOutLinearInInterpolator());

                TransitionManager.beginDelayedTransition(transitionsContainer,set);
                visible = !visible;
                if(temp!=null)
                    temp.setVisibility(View.GONE);
                temp=tvContent;

                tvContent.setVisibility(visible ? View.VISIBLE : View.GONE);


            }


        });

        return convertView;
    }

}