package com.deltatechnepal.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.model.RestoTimingModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by DeltaTech on 4/11/2018.
 */

public class RestoTimingAdapter  extends RecyclerView.Adapter<RestoTimingAdapter.MyViewHolder>{
    private List<RestoTimingModel> restoTimingList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDay, tvFrom, tvTo;

        public MyViewHolder(View view) {
            super(view);
            tvDay = (TextView) view.findViewById(R.id.tvDay);
            tvFrom = (TextView) view.findViewById(R.id.tvFrom);
            tvTo = (TextView) view.findViewById(R.id.tvTo);
        }
    }
    public RestoTimingAdapter(List<RestoTimingModel> restoTimingList) {
        this.restoTimingList = restoTimingList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_resto_timing, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RestoTimingModel restoTiming = restoTimingList.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date date = new Date();
        String today = sdf.format(date).toLowerCase();
        if(TextUtils.equals(restoTiming.getDay(),today)) {
            holder.tvDay.setTextColor(Color.GREEN);
            holder.tvFrom.setTextColor(Color.GREEN);
            holder.tvTo.setTextColor(Color.GREEN);
        }
        holder.tvDay.setText(restoTiming.getDay());
        holder.tvFrom.setText(restoTiming.getFrom());
        holder.tvTo.setText(restoTiming.getTo());
    }

    @Override
    public int getItemCount() {
        return restoTimingList.size();
    }
}
