package com.deltatechnepal.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.PhotoGallery;
import com.deltatechnepal.utility.MConstant;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

public class RestoPhotoGalleryAdapter extends RecyclerView.Adapter<RestoPhotoGalleryAdapter.ViewHolder> {
    private List<JsonObject> photoList;
    private Context mContext;

    public RestoPhotoGalleryAdapter(Context context, List<JsonObject> photoList) {
        this.photoList = photoList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_card_restodetail_photogal, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        JsonObject photo = photoList.get(i);
        String imgUrl = MConstant.SERVER+"/uploads/photogallery/150x150"+photo.get("image");
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivPhotoGallery);
        holder.tvTitle.setText(photo.get("title").toString());
    }

    @Override
    public int getItemCount() {
        return (null != photoList ? photoList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivPhotoGallery;
        protected TextView tvTitle,tvCuisin,tvAddress;

        public ViewHolder(View view) {
            super(view);
            this.ivPhotoGallery = (ImageView) view.findViewById(R.id.ivPhotoGallery);
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*PhotoGallery photoGallery = photoList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    intent.putExtra("photo_id",photoGallery.photo_id);
                    mContext.startActivity(intent);*/
                    return;
                }
            });
        }
    }
}