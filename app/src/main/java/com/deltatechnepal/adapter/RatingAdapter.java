package com.deltatechnepal.adapter;



import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.deltatechnepal.foodpal.R;


import java.util.List;



public class RatingAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Boolean> ratingBar;


    public RatingAdapter(Activity activity,List<Boolean> ratingBar) {
        this.activity = activity;
        this.ratingBar=ratingBar;

    }

    @Override
    public int getCount() {
        return ratingBar.size();
    }

    @Override
    public Object getItem(int location) {
        return ratingBar.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.checkbox_rating_layout, null);



        return convertView;
    }





}