package com.deltatechnepal.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.foodpal.BlogDetailActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.deltatechnepal.foodpal.R.layout.list_row_popular_blog;

/**
 * Created by DeltaTech on 4/15/2018.
 */

public class BlogPopularAdapter extends RecyclerView.Adapter<BlogPopularAdapter.ViewHolder>{
    private List<Blog> blogList;
    private Context mContext;

    public BlogPopularAdapter(Context context,List<Blog> blogList) {
        this.blogList = blogList;
        this.mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView ivBlog;
        public TextView tvBlogTitle;
        public Button btnEdit;
        public ViewHolder(View v) {
            super(v);
            ivBlog =(ImageView) v.findViewById(R.id.ivBlog);
            tvBlogTitle = (TextView) v.findViewById(R.id.tvBlogTitle);
        }
    }

    @Override
    public BlogPopularAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(list_row_popular_blog, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BlogPopularAdapter.ViewHolder holder, int position) {
        final Blog blog = blogList.get(position);
        String imgUrl = MConstant.SERVER+"/uploads/blogs/150x150"+blog.image;
        holder.tvBlogTitle.setText(blog.title);
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivBlog);
        holder.ivBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,BlogDetailActivity.class);
                intent.putExtra("blog_id",blog.blog_id);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != blogList ? blogList.size() : 0);
    }
}







































