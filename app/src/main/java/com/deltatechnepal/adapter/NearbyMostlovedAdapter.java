package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.RestoDetailActivity;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import java.util.List;

public class NearbyMostlovedAdapter extends RecyclerView.Adapter<NearbyMostlovedAdapter.ViewHolder> {

    private JsonArray jsonArray;
    private Context mContext;

    public NearbyMostlovedAdapter(Context context, JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_nearby_mostloved, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        JsonElement jsonElement = jsonArray.get(i);
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        String imgUrl = MConstant.SERVER+"/uploads/restaurantimage/"+jsonObj.get("image").getAsString();
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return (null != jsonArray ? jsonArray.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Resto resto = restoList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    intent.putExtra("resto_id",resto.resto_id);
                    mContext.startActivity(intent);*/
                }
            });
        }
    }
}