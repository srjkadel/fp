package com.deltatechnepal.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.deltatechnepal.foodpal.Notifis;
import com.activeandroid.query.Delete;
import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.NotificationList;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Notification;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import static com.activeandroid.Cache.getContext;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>{
    private List<Notification>notifisList;
    private Context mContext;


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_total_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Notification notifis = notifisList.get(position);
        holder.restname.setText(notifis.title);
        holder.desc.setText(notifis.description);
        String unixtime = notifis.unix_timestamp;
        long changedint = Long.valueOf(unixtime);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String actualdate = dateFormat.format(changedint);
        holder.showtime.setText(actualdate);

    }
    public NotificationAdapter(Context tempContext,List<Notification> moviesList) {
        this.notifisList = moviesList;
        mContext = tempContext;
    }

    @Override
    public int getItemCount() {
        return notifisList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView restname, desc, showtime;

        public MyViewHolder(View itemView) {
            super(itemView);

            restname = itemView.findViewById(R.id.restroname);
            desc     = itemView.findViewById(R.id.description);
            showtime = itemView.findViewById(R.id.showtime);

            /*restname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performaction();
                }
            });

            desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    performaction();
                }
            });*/

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = getAdapterPosition();
                    performaction(index);
                    //Toast.makeText(mContext, "ok", Toast.LENGTH_SHORT).show();
                }
            });



        }

    }

    private void performaction(int index){

        /*//Toast.makeText(getContext(),"You clicked me",Toast.LENGTH_LONG).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you want to delete notification?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getContext(),"I am deleted",Toast.LENGTH_LONG).show();

            }
        });

        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getContext(),"I am safe ",Toast.LENGTH_LONG).show();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
*/
        final int tempIndex = index;

        new AlertDialog.Builder(mContext)
                .setTitle("Notification")
                .setMessage("Want To Delete this notification ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //finish();
                        //Toast.makeText(mContext, "I am deleted", Toast.LENGTH_SHORT).show();
                        deleteItem(tempIndex);
                    }
                })
                .setNegativeButton("No", null).show();

    }

    void deleteItem(int index) {
        Notification temp = notifisList.get(index);
        Toast.makeText(mContext,"I am deleted "+index+"",Toast.LENGTH_LONG).show();
        temp.delete();




        notifisList.remove(index);
        // to do delete frm db

        notifyItemRemoved(index);
    }





}




