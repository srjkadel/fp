package com.deltatechnepal.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.RestoDetailActivity;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.squareup.picasso.Picasso;
import java.util.List;

public class RestoFragFeaturedAdapter extends RecyclerView.Adapter<RestoFragFeaturedAdapter.ViewHolder> {
    private List<Resto> restoList;
    private Context mContext;

    public RestoFragFeaturedAdapter(Context context, List<Resto> restoList) {
        this.restoList = restoList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_card_restofrag_featured, null);
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_nearby_recommended_resto, null);
        ViewHolder mh = new ViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Resto resto = restoList.get(i);
        String imgUrl = MConstant.SERVER+"/uploads/restaurantbanner/1350x500"+resto.banner_image;

        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .fit()
                .into(holder.ivResto);
        holder.tvName.setText(resto.name);
        holder.tvCuisin.setText(resto.cuisines);
        holder.tvAddress.setText(resto.formatted_address);
        Double distance = MFunction.getDistance(mContext,resto.lati,resto.longi);
        holder.tvDistance.setText(distance.toString()+"KM Around");
    }

    @Override
    public int getItemCount() {
        return (null != restoList ? restoList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivResto;
        protected TextView tvName,tvCuisin,tvAddress,tvDistance;

        public ViewHolder(View view) {
            super(view);
            this.ivResto = (ImageView) view.findViewById(R.id.ivResto);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvCuisin = (TextView) view.findViewById(R.id.tvCuisin);
            this.tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            this.tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Resto resto = restoList.get(getAdapterPosition());
                    Intent intent = new Intent(mContext,RestoDetailActivity.class);
                    intent.putExtra("resto_id",resto.resto_id);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}