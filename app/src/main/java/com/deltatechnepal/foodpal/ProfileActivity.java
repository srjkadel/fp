package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.model.MySingleton;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity implements
        ProfileFragment.OnFragmentInteractionListener,
        ProfileReviewFragment.OnFragmentInteractionListener,
        ProfileBlogFragment.OnFragmentInteractionListener,
        ProfileBookmarkFragment.OnFragmentInteractionListener {

    String TAG = "ProfileActivity";
    Context mContext;
    ProgressDialog mProd;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    int PICK_PROFILE = 100;
    ImageView ivProfile;
    TextView tvFP, tvProfileName, tvProfileType;
    Button btnFollow;
    Boolean isLogined;
    String userID, fromNav;
    ImageButton btnfacebook, btninsta;
    JSONObject profileData;
    JSONArray reviewData, blogData, bookmarkData;


    private JsonObjectRequest jsonObjectRequest;
    private JSONObject joData = new JSONObject();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        fromNav = getIntent().getStringExtra("from_nav");
        btnfacebook = findViewById(R.id.btnFacebook);
        btninsta = findViewById(R.id.btnInsta);

        userID = MFunction.getMyPrefVal("user_id", mContext);
        String isLogin = MFunction.getMyPrefVal("is_login", mContext);
        isLogined = TextUtils.equals(isLogin, "true") ? true : false;
        setContentView(R.layout.activity_profile);

        // making notification bar transparent
        changeStatusBarColor();

        /*Initializing Progress Dialog*/
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Fetching Data...");

        /*Initialising variables*/
        ivProfile = (ImageView) findViewById(R.id.ivUserImage);
        tvFP = (TextView) findViewById(R.id.tvUserPoints);
        tvProfileName = (TextView) findViewById(R.id.tvUserName);
        btnFollow = (Button) findViewById(R.id.btnFollow);

        ImageButton ibHome = (ImageButton) findViewById(R.id.ibHome);
        ibHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*For Tab Layout*/
        viewPager = findViewById(R.id.viewpager);
        //setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        // fetchData();


        String url = MConstant.API_END + "/getProfilePageData";
        joData = new JSONObject();


        try {
            joData.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);

            if (isLogined && TextUtils.equals(fromNav, "true"))
                joData.put("user_id", MFunction.getMyPrefVal("user_id", mContext));
            else
                joData.put("user_id", getIntent().getStringExtra("author_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "onCreate: "+url);
        sendVolleyRequest(url);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PROFILE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            ivProfile = (ImageView) findViewById(R.id.ivUserImage);
            ivProfile.setImageURI(uri);
            /*UCrop.of(uri,uri)
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(400,400 )
                    .start(this);*/
        }

        /*if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            final Uri resultUri = UCrop.getOutput(data);
            ImageView ivProfile = (ImageView)findViewById(R.id.ivProfile);
            ivProfile.setImageURI(resultUri);
        } else if(resultCode == UCrop.RESULT_ERROR){
            Log.i(TAG, "onActivityResult:"+UCrop.getError(data).toString());
        }*/
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(ProfileFragment.newInstance(profileData.toString(), blogData.toString()), "Profile");
        adapter.addFragment(ProfileReviewFragment.newInstance(reviewData.toString(), ""), "Reviews");
        adapter.addFragment(ProfileBlogFragment.newInstance(blogData.toString(), ""), "Blogs");
        adapter.addFragment(ProfileBookmarkFragment.newInstance(bookmarkData.toString(), ""), "Bookmarks");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void settingHeaderData(JSONObject profile) {
        try {
            System.out.println("PROFILE " + profile);
            String profileID = profile.getString("id");
            String loginedID = MFunction.getMyPrefVal("user_id", mContext);
            String imagePath = (TextUtils.equals(profileID, loginedID)) ? MFunction.getMyPrefVal("phptoUrl", mContext) : MConstant.SERVER + "/uploads/avatars/members/" + profile.getString("avatar");
            String fullName = profile.getString("first_name") + " " + profile.getString("last_name");
            Picasso.with(mContext)
                    .load(imagePath)
                    .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                    .error(mContext.getResources().getDrawable(R.drawable.default_image))
                    .fit()
                    .into(ivProfile);
            tvFP.setText(MFunction.getMyPrefVal("point", mContext) + "FP");
            tvFP.setTextColor(Color.WHITE);
            tvProfileName.setText(fullName);

            if (isLogined && TextUtils.equals(fromNav, "true")) {
                btnFollow.setVisibility(View.GONE);
                //btnfacebook.setVisibility(View.GONE);
                //btninsta.setVisibility(View.GONE);
            }

            TextView tvBlogCount = (TextView) findViewById(R.id.tvBlogCount);
            tvBlogCount.setText(profile.getString("blogCount"));

            TextView tvReviewCount = (TextView) findViewById(R.id.tvReviewCount);
            tvReviewCount.setText(profile.getString("reviewCount"));

            TextView tvFollowerCount = (TextView) findViewById(R.id.tvFollowerCount);
            tvFollowerCount.setText(profile.getString("followerCount"));
        } catch (JSONException e) {

        }
    }

    /*private void fetchData() {
        mProd.show();
       // http://10.0.2.2/php-foodpal/api/getProfilePageData

        String url = MConstant.API_END+"/getProfilePageData";
        final StringRequest req = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        mProd.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(!jsonObject.getBoolean("status"))return;
                            profileData = new JSONObject(jsonObject.getString("profile"));
                            reviewData=new JSONArray(jsonObject.getString("reviews"));
                            blogData =new JSONArray(jsonObject.getString("blogs"));
                            bookmarkData = new JSONArray(jsonObject.getString("bookmarks"));
                            settingHeaderData(profileData);
                            setupViewPager(viewPager);

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
     Log.e("mahenError",error.toString());

                        error.printStackTrace();
                        System.out.println("PROFILE response 2 "+error.getMessage());

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);

                //COMMENTED BY Pramod
*//*                if (!isLogined && !TextUtils.equals(fromNav, "true"))
                    params.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
                else
               params.put("user_id",getIntent().getStringExtra("author_id"));*//*



                //CHANGED BY Pramod

                *//*if (isLogined && TextUtils.equals(fromNav, "true"))
                    params.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
                else
                    params.put("user_id",getIntent().getStringExtra("author_id"));*//*
                    //params.put("user_id",getIntent().getStringExtra("3"));

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }*/


    private void sendVolleyRequest(String url) {

        System.out.println(" joData:> " + joData);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, joData, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        profileData = new JSONObject(response.getString("profile"));
                        reviewData = new JSONArray(response.getString("reviews"));
                        blogData = new JSONArray(response.getString("blogs"));
                        bookmarkData = new JSONArray(response.getString("bookmarks"));
                        settingHeaderData(profileData);
                        setupViewPager(viewPager);

                    } else {
                        Log.i("test", "test");

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("error", e.toString());


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", error.toString());

            }
        }) {
            /**
             * Returns a list of extra HTTP headers to go along with this request. Can throw {@link
             * AuthFailureError} as authentication may be required to provide these values.
             *
             * @throws AuthFailureError In the event of auth failure
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;

            }
        };
        jsonObjectRequest.setTag("OBTAINMETHOD");

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


}
