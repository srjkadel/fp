package com.deltatechnepal.foodpal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import com.deltatechnepal.adapter.GridviewAdapter;

public class CuisinesActivity extends AppCompatActivity {
    GridView androidGridView;
    int[] gridViewImageId = {
            R.mipmap.asian, R.mipmap.bakery, R.mipmap.burmese, R.mipmap.chinese, R.mipmap.continental, R.mipmap.european,
            R.mipmap.indian,R.mipmap.italian, R.mipmap.japanese, R.mipmap.korean, R.mipmap.mediterranean,
            R.mipmap.middle_eastern, R.mipmap.nepali,R.mipmap.newari,R.mipmap.oriental,R.mipmap.sea_food,R.mipmap.south_indian,
            R.mipmap.thai,R.mipmap.vietnamese,

    };

    Context mContext;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisines);
        mContext=this;

        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView tvTitle =toolbar.findViewById(R.id.toolbar_title);
        tvTitle.setText("Cuisines");

        GridviewAdapter adapterViewAndroid = new GridviewAdapter(mContext,false,null, gridViewImageId);
        androidGridView=findViewById(R.id.grid_view_image_text);
        androidGridView.setAdapter(adapterViewAndroid);
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {


                Intent cIntent = new Intent(getApplicationContext(), MainActivity.class);
                cIntent.putExtra("search_cuisines", true);
                switch (i) {

                    case 0:
                        cIntent.putExtra("cuisine", "asian");
                        break;

                    case 1:
                        cIntent.putExtra("cuisine", "bakery");
                        break;
                    case 2:
                        cIntent.putExtra("cuisine", "burmese");
                        break;
                    case 3:
                        cIntent.putExtra("cuisine", "chinese");
                        break;
                    case 4:
                        cIntent.putExtra("cuisine", "continental");
                        break;
                    case 5:
                        cIntent.putExtra("cuisine", "european");
                        break;
                    case 6:
                        cIntent.putExtra("cuisine", "indian");
                        break;
                    case 7:
                        cIntent.putExtra("cuisine", "italian");
                        break;
                    case 8:
                        cIntent.putExtra("cuisine", "japanese");
                        break;
                    case 9:
                        cIntent.putExtra("cuisine", "korean");
                        break;
                    case 10:
                        cIntent.putExtra("cuisine", "mediterranean");
                        break;
                    case 11:
                        cIntent.putExtra("cuisine", "middle_eastern");
                        break;
                    case 12:
                        cIntent.putExtra("cuisine", "nepali");
                        break;
                    case 13:
                        cIntent.putExtra("cuisine", "newari");
                        break;
                    case 14:
                        cIntent.putExtra("cuisine", "oriental");
                        break;
                    case 15:
                        cIntent.putExtra("cuisine", "sea_food");
                        break;
                    case 16:
                        cIntent.putExtra("cuisine", "south_indian");
                        break;
                    case 17:
                        cIntent.putExtra("cuisine", "thai");
                        break;
                    case 18:
                        cIntent.putExtra("cuisine", "vietnamese");
                        break;
                }

              startActivity(cIntent);


            }
        });


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_option, menu);
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_notifications: {

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


}