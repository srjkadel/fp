package com.deltatechnepal.foodpal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aromajoin.actionsheet.ActionSheet;
import com.aromajoin.actionsheet.OnActionListener;
import com.deltatechnepal.adapter.ExplorerAdapter;
import com.deltatechnepal.adapter.HomeGridAdapter;
import com.deltatechnepal.adapter.NearbyRestoAdapter;
import com.deltatechnepal.adapter.SearchRestoAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.ormmodel.RestoCat;
import com.deltatechnepal.utility.BottomNavigationViewHelper;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestaurantFragment extends Fragment implements OnActionListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private Context mContext;
    private View mView;
    private GridView gvExplore;
    private PlacesAutocompleteTextView actvPlace;
    private ConstraintLayout cLayout;
    JsonArray all;
    List<Resto> allResto, searchedList;
    RecyclerView rvSearchList;
    EditText etSearchView;
    NearbyRestoAdapter searchRestoAdapter;
    boolean rupees[], cuisines[];
    boolean isCuisinesSearch = false, isCategorySearch = false, isApplyFilter = false;
    String cuisine, category;
    TextView tvSearchQuery, tvfeaturedsectionseemore, tvmostlovedsectionseemore, tvnewlyaddedsectionseemore;
    ImageButton btnFilter;
    JsonArray categories;
    ImageButton gototop, gototop2;
    ScrollView scrollView;
    JsonArray sponsored, mostRecommended, newlyAdded;


    public RestaurantFragment() {
    }

    public static RestaurantFragment newInstance(String param1, String param2) {
        RestaurantFragment fragment = new RestaurantFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        isCuisinesSearch = getArguments().getBoolean("cuisines_search", false);
        cuisine = getArguments().getString("cuisine");
        isCategorySearch = getArguments().getBoolean("category_search", false);
        category = getArguments().getString("category");
        isApplyFilter = getArguments().getBoolean("apply_filter", false);


        ((MainActivity) getActivity()).setActionBarTitle("Search Restaurants");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mContext = getActivity();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        mView = view;

        fetchData(view);
        actvPlace = view.findViewById(R.id.actvPlace);
        etSearchView = view.findViewById(R.id.etSearchText);
        cLayout = view.findViewById(R.id.cLayout);
        tvSearchQuery = view.findViewById(R.id.tvSearchResults);
        btnFilter = view.findViewById(R.id.search_filter);
        scrollView = (ScrollView) view.findViewById(R.id.fcontainer);
        tvfeaturedsectionseemore = view.findViewById(R.id.tvFeaturedSectionseemore);
        tvmostlovedsectionseemore = view.findViewById(R.id.tvMostLovedSectionseemore);
        tvnewlyaddedsectionseemore = view.findViewById(R.id.tvNewlyAddedSectionseemore);


        //   gototop=(ImageButton) view.findViewById(R.id.ontop);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if (i1 > 100) {
                    gototop.setVisibility(View.VISIBLE);
                } else {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        MainActivity mainActivity = (MainActivity) getActivity();
        gototop = mainActivity.findViewById(R.id.gotoTop);
        gototop.setVisibility(View.GONE);


        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0, 0);
            }
        });

        rvSearchList = (RecyclerView) view.findViewById(R.id.rvSearchResult);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvSearchList.setHasFixedSize(true);
        rvSearchList.setNestedScrollingEnabled(true);
        rvSearchList.setLayoutManager(popularLM);


        tvfeaturedsectionseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cLayout.setVisibility(View.GONE);
                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, sponsored);
                rvSearchList.setAdapter(searchRestoAdapter);


            }
        });

        tvnewlyaddedsectionseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cLayout.setVisibility(View.GONE);
                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, newlyAdded);
                rvSearchList.setAdapter(searchRestoAdapter);
            }
        });
        tvmostlovedsectionseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cLayout.setVisibility(View.GONE);
                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, mostRecommended);
                rvSearchList.setAdapter(searchRestoAdapter);
            }
        });


//        if (MFunction.isInternetAvailable(mContext)) {
//            fetchData(mView);
//
//        } else {
        allResto = Resto.getAllResto(500);
        setSponsored(mView);
        setMostRecommended(mView);
        setNewlyAdded(mView);
        if (isApplyFilter) {
            cLayout.setVisibility(View.GONE);
            mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);

            String categories = getArguments().getString("categories");
            String cuisines = getArguments().getString("cuisines");
            String amenities = getArguments().getString("amenities");
            String rupees = getArguments().getString("rupees");
            String rating = getArguments().getString("rating");
            String order = getArguments().getString("order");

            List<Resto> filteredResto = new ArrayList<>();


            for (Resto resto : allResto
            ) {
                boolean filterCategory = filterCategory(categories, resto);
                boolean filterCuisines = filterCuisines(cuisines, resto);
                boolean filterAmenities = filterAmenities(amenities, resto);
                boolean filterRupees = filterRupees(rupees, resto);
                boolean filterRating = filterRating(rating, resto);
                Log.i("test", "test");

                if (filterCategory && filterCuisines && filterAmenities && filterRupees && filterRating) {
                    filteredResto.add(resto);
                }
            }

            SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, filteredResto);
            rvSearchList.setAdapter(searchRestoAdapter);
        }


        btnFilter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                String check = String.valueOf(all);
                // Toast.makeText(mContext,check,Toast.LENGTH_LONG).show();

                if (isApplyFilter) {

                   // getActivity().finish();
                } else {


                    //if(all!=null) {
                    if (true) {
                        Intent intent = new Intent(mContext, FilterActivity.class);
                        //intent.putExtra("cat", categories.toString());
                        startActivity(intent);
                       // getActivity().finish();
                    }

                }


            }
        });


        GridView gvExplore = view.findViewById(R.id.gvExplore);
        setExplorer(mView);
        setCuisines(mView);
        gvExplore.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> obj = (HashMap<String, String>) parent.getItemAtPosition(position);
                String catID = obj.get("cat_id").toString();
                switch (catID) {
                    case "1": {
                        Toast.makeText(mContext, catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "2": {
                        Toast.makeText(mContext, catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "3": {
                        Toast.makeText(mContext, catID, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case "4": {
                        Toast.makeText(mContext, catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "5": {
                        Toast.makeText(mContext, catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "6": {
                        showActionSheet(view);
                        break;
                    }
                }
            }
        });

        TextView tvPlaceChange = view.findViewById(R.id.tvPlaceChange);
        tvPlaceChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actvPlace.setText(null);
            }
        });


        etSearchView.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                String query = s.toString();
                tvSearchQuery.setText("Search results for : " + query);
                if (s.toString().length() != 0) {
                    cLayout.setVisibility(View.GONE);
                    mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                    // gototop2.setVisibility(View.VISIBLE);
                    gototop.setVisibility(View.GONE);

                    search(query);
                } else {
                    mView.findViewById(R.id.groupSearchResult).setVisibility(View.GONE);
                    cLayout.setVisibility(View.VISIBLE);
                    //  gototop2.setVisibility(View.GONE);
                    gototop.setVisibility(View.VISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        /*SwipeRefreshLayout srlSecond = (SwipeRefreshLayout) view.findViewById(R.id.srlSecond);
        srlSecond.setOnRefreshListener(this);*/

        return view;
    }

    private void showActionSheet(View view) {
        ActionSheet actionSheet = new ActionSheet(getActivity());
        actionSheet.setTitle("Choose Any Category");
        actionSheet.setSourceView(view);
        actionSheet.addAction("Extra Category 6", ActionSheet.Style.DEFAULT, this);
        actionSheet.addAction("Extra Category 7", ActionSheet.Style.DEFAULT, this);
        actionSheet.addAction("Extra Category 8", ActionSheet.Style.DEFAULT, this);
        actionSheet.show();
    }

    @Override
    public void onSelected(ActionSheet actionSheet, String title) {
       /* Toast.makeText(getActivity(),""+title, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(mContext,SearchRestaurantActivity.class);
        intent.putExtra("resto_cat_id","100");
        mContext.startActivity(intent);*/
        actionSheet.dismiss();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        //refresh();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void fetchData(View view) {
        final View mView = view;
        String url;
        ((MainActivity) getActivity()).openDialog();
        if (isApplyFilter)
            url = MConstant.API_END + "/applyFilter";
        else
            url = MConstant.API_END + "/getRestaurantPageData";

        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        if (isApplyFilter) {
            String category = getArguments().getString("categories");
            String cuisines = getArguments().getString("cuisines");
            String amenities = getArguments().getString("amenities");
            String rupees = getArguments().getString("rupees");
            String rating = getArguments().getString("rating");
            String order = getArguments().getString("order");
            params.put("categories", category);
            params.put("cuisines", cuisines);
            params.put("amenities", amenities);
            params.put("rupees", rupees);
            params.put("rating", rating);
            params.put("order", order);
        }
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MainActivity activity = (MainActivity) getActivity();
                        if (activity != null) activity.closeDialog();
                        try {
                            if (!response.getBoolean("status")) return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            all = data.getAsJsonArray("all");
                            Resto.saveAll(all);
                            setSearchList(mView);


                            if (isCuisinesSearch) {
                                cLayout.setVisibility(View.GONE);
                                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                                search(cuisine);

                            } else if (isCategorySearch) {
                                cLayout.setVisibility(View.GONE);
                                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                                search(category);
                            } else if (isApplyFilter) {
                                cLayout.setVisibility(View.GONE);
                                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                                SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, all);
                                rvSearchList.setAdapter(searchRestoAdapter);
                            } else {
                                sponsored = data.getAsJsonArray("sponsored");
                                mostRecommended = data.getAsJsonArray("most_recommended");
                                newlyAdded = data.getAsJsonArray("newly_added");
                                categories = data.getAsJsonArray("categories");
                                RestoCat.saveAllCat(categories);
                                JsonArray cuisines = data.getAsJsonArray("cuisines");
                                setSponsored(sponsored, mView);
                                setMostRecommended(mostRecommended, mView);
                                setNewlyAdded(newlyAdded, mView);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainActivity activity = (MainActivity) getActivity();
                        if (activity != null) activity.closeDialog();
                        Log.e("VolleyError", error.toString());
                        Toast.makeText(mContext, "Serverside error", Toast.LENGTH_LONG).show();

                        setSponsored(mView);

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void setSponsored(JsonArray tempArray, View view) {
        RecyclerView rvFeatured = (RecyclerView) view.findViewById(R.id.rvFeatured);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvFeatured.setHasFixedSize(true);
        rvFeatured.setNestedScrollingEnabled(true);
        rvFeatured.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempArray, false);
        rvFeatured.swapAdapter(nearbyRestoAdapter, false);


        //for insert inside database
//        if(all !=null){
//            JSONObject jsonObject = new JSONObject();
//            JSONObject featuresponser = jsonObject.optJSONObject("sponsored");
//
//            Gson gson = new Gson();
//            Resto updateresto = gson.fromJson(featuresponser.toString(),Resto.class);
//            updateresto.save();
//
//        }


    }

    private void setSponsored(View view) {
        List<Resto> tempList = Resto.getSponsoredResto(10);
        //List<Resto> allList = Resto.getAllResto(1000);

        RecyclerView rvFeatured = (RecyclerView) view.findViewById(R.id.rvFeatured);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvFeatured.setHasFixedSize(true);
        rvFeatured.setNestedScrollingEnabled(true);
        rvFeatured.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempList, false);
        rvFeatured.swapAdapter(nearbyRestoAdapter, false);
    }

    private void setMostRecommended(JsonArray tempArray, View view) {
        RecyclerView rvMostLoved = (RecyclerView) view.findViewById(R.id.rvMostLoved);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvMostLoved.setHasFixedSize(true);
        rvMostLoved.setNestedScrollingEnabled(true);
        rvMostLoved.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempArray, false);
        rvMostLoved.swapAdapter(nearbyRestoAdapter, false);

    }

    private void setMostRecommended(View view) {
        List<Resto> tempList = Resto.getMostLovedResto(20);
        RecyclerView rvMostLoved = (RecyclerView) view.findViewById(R.id.rvMostLoved);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvMostLoved.setHasFixedSize(true);
        rvMostLoved.setNestedScrollingEnabled(true);
        rvMostLoved.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempList, false);
        rvMostLoved.swapAdapter(nearbyRestoAdapter, false);

    }

    private void setNewlyAdded(View view) {
        List<Resto> tempList = Resto.getNewlyAddedResto(20);
        RecyclerView rvMostLoved = (RecyclerView) view.findViewById(R.id.rvNewlyAdded);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvMostLoved.setHasFixedSize(true);
        rvMostLoved.setNestedScrollingEnabled(true);
        rvMostLoved.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempList, false);
        rvMostLoved.swapAdapter(nearbyRestoAdapter, false);

    }

    private void setNewlyAdded(JsonArray tempArray, View view) {
        RecyclerView rvNewlyAdded = (RecyclerView) view.findViewById(R.id.rvNewlyAdded);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvNewlyAdded.setHasFixedSize(true);
        rvNewlyAdded.setNestedScrollingEnabled(true);
        rvNewlyAdded.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempArray, false);
        rvNewlyAdded.swapAdapter(nearbyRestoAdapter, false);
    }

    private void setSearchList(View view) {
        rvSearchList = (RecyclerView) view.findViewById(R.id.rvSearchResult);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvSearchList.setHasFixedSize(true);
        rvSearchList.setNestedScrollingEnabled(true);
        rvSearchList.setLayoutManager(popularLM);

        searchRestoAdapter = new NearbyRestoAdapter(mContext, all, true);
        rvSearchList.setAdapter(searchRestoAdapter);


    }

    private void setCategories(JsonArray tempArray, View view) {
        return;

    }

    private void setCuisines(View view) {
        GridView gvCuisines = (GridView) view.findViewById(R.id.gvCuisines);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String, String> map1 = new HashMap<>();
        map1.put("cat_id", "1");
        map1.put("image", "chinese");
        map1.put("cat_name", "");

        HashMap<String, String> map2 = new HashMap<>();
        map2.put("cat_id", "2");
        map2.put("image", "indian");
        map2.put("cat_name", "");

        HashMap<String, String> map3 = new HashMap<>();
        map3.put("cat_id", "3");
        map3.put("image", "italian");
        map3.put("cat_name", "");

        HashMap<String, String> map4 = new HashMap<>();
        map4.put("cat_id", "4");
        map4.put("image", "middle_eastern");
        map4.put("cat_name", "");

        HashMap<String, String> map5 = new HashMap<>();
        map5.put("cat_id", "5");
        map5.put("image", "japanese");
        map5.put("cat_name", "");

        HashMap<String, String> map6 = new HashMap<>();
        map6.put("cat_id", "6");
        map6.put("image", "thai");
        map6.put("cat_name", "");

        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        tempList.add(map4);
        tempList.add(map5);
        tempList.add(map6);
        HomeGridAdapter nearbyQuickSearch = new HomeGridAdapter(mContext, false, tempList);
        gvCuisines.setAdapter(nearbyQuickSearch);

    }

    private void setExplorer(View view) {

        GridView gvExplore = (GridView) view.findViewById(R.id.gvExplore);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String, String> map1 = new HashMap<String, String>();
        map1.put("cat_id", "1");
        map1.put("image", "cafe");
        map1.put("cat_name", "Cafe");

        HashMap<String, String> map2 = new HashMap<String, String>();
        map2.put("cat_id", "2");
        map2.put("image", "dinner");
        map2.put("cat_name", "Dinner");

        HashMap<String, String> map3 = new HashMap<String, String>();
        map3.put("cat_id", "3");
        map3.put("image", "fine_dining");
        map3.put("cat_name", "Night Life");

        HashMap<String, String> map4 = new HashMap<String, String>();
        map4.put("cat_id", "4");
        map4.put("image", "delivery");
        map4.put("cat_name", "Delivery");

        HashMap<String, String> map5 = new HashMap<String, String>();
        map5.put("cat_id", "5");
        map5.put("image", "liquor");
        map5.put("cat_name", "Bar & Lounge");

        HashMap<String, String> map6 = new HashMap<String, String>();
        map6.put("cat_id", "6");
        map6.put("image", "sweets");
        map6.put("cat_name", "Sweets ");

        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        tempList.add(map4);
        tempList.add(map5);
        tempList.add(map6);

        ExplorerAdapter explorerAdapter = new ExplorerAdapter(mContext, tempList);
        gvExplore.setAdapter(explorerAdapter);
    }

    private void refresh() {
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    public void search(String searchtext) {
        List<Resto> searchedList = new ArrayList<>();
        String query = searchtext.toLowerCase();
        if (true) {
            for (Resto item : allResto) {
                String restoName = item.name.toLowerCase();
                String restoCuisines = item.cuisines.toLowerCase();
                String restoCategory = item.category;
                String address = item.formatted_address.toLowerCase();
                if ((restoName.contains(query) || restoCuisines.contains(query) || restoCategory.contains(query)) && addresscheck(address)) {
                    searchedList.add(item);
                }
            }
            SearchRestoAdapter searchRestoAdapter = new SearchRestoAdapter(mContext, searchedList);
            rvSearchList.setAdapter(searchRestoAdapter);
        }
    }

    private boolean addresscheck(String tempAddress) {
        boolean result = true;
        String activeplace = String.valueOf(actvPlace.getText()).toLowerCase();
        String[] arrayactiveplace = activeplace.split(",");


        if (TextUtils.isEmpty(activeplace)) return result;

        String[] formattedPlace = tempAddress.split(",");
        for (String tempAddressPiece : formattedPlace
        ) {
            if (activeplace.contains(tempAddressPiece.trim())) {
                result = true;
                break;
            } else {
                result = false;
            }
        }
        return result;
    }
    /*public void search(String searchText){
        JsonArray newList=new JsonArray();
        String query = searchText.toLowerCase();
        if(all==null) return;
        for (JsonElement item: all) {
            JsonObject jsonObj = item.getAsJsonObject();
            String restoName = jsonObj.get("name").getAsString().toLowerCase();
            String restoCuisines = jsonObj.get("cuisines").getAsString().toLowerCase();
            String restoCategory = jsonObj.get("category").getAsString();

            if(restoName.contains(query) || restoCuisines.contains(query) || restoCategory.contains(query))
            {
                newList.add(item);
            }
        }
        if(newList.size()==0) tvSearchQuery.setText("No Results Found for : "+searchText);

        searchRestoAdapter.updateList(newList);
    }





*/

    private boolean filterCategory(String csvCategories, Resto resto) {

        boolean result = true;
        if (TextUtils.isEmpty(csvCategories)) return result;
        String[] arrayCatFromFilter = csvCategories.split(",");
        String[] arrayCatFromCurrentResto = resto.category.split(",");
        boolean commonornot = Collections.disjoint(Arrays.asList(arrayCatFromFilter), Arrays.asList(arrayCatFromCurrentResto));

        if (commonornot == false) {
            result = true;
        } else {
            result = false;
        }

        return result;

    }

    private boolean filterCuisines(String csvCuisines, Resto resto) {
        boolean result = true;
        if (TextUtils.isEmpty(csvCuisines)) {
            return result;
        }
        String[] arraycuisnFromFilter = csvCuisines.split(",");
        String[] arraycuisnFromCurrentResto = resto.cuisines.split(",");
        boolean commonornot = Collections.disjoint(Arrays.asList(arraycuisnFromFilter), Arrays.asList(arraycuisnFromCurrentResto));


        if (commonornot == false) {
            result = true;
        } else {
            result = false;
        }

        return result;

    }

    private boolean filterAmenities(String csvAmenities, Resto resto) {
        boolean result = true;
        if (TextUtils.isEmpty(csvAmenities)) return result;

        String[] arrayameniFromFilter = csvAmenities.split(",");
        String[] arrayameniFromCurrentResto = resto.amenities.split(",");
        boolean commonornot = Collections.disjoint(Arrays.asList(arrayameniFromFilter), Arrays.asList(arrayameniFromCurrentResto));


        if (commonornot == false) {
            result = true;
        } else {
            result = false;
        }

        return result;

    }

    private boolean filterRupees(String csvRupees, Resto resto) {
        boolean result = false;
        if (TextUtils.isEmpty(csvRupees)) {
            result = true;
            return result;
        }
        String rupee = csvRupees;
        String databaserupee = resto.average_cost;
        switch (rupee) {
            case "1": {
                if (Integer.valueOf(resto.average_cost) <= 700) {
                    result = true;
                    break;
                }
            }
            case "2": {
                if (Integer.valueOf(resto.average_cost) > 700 || Integer.valueOf(resto.average_cost) < 2000) {
                    result = true;
                    break;
                }

            }
            case "3": {
                if (Integer.valueOf(resto.average_cost) >= 2000) {
                    result = true;
                    break;
                }

            }
            case "0": {
                result = true;
                break;

            }
        }
        return result;


    }

    public boolean filterRating(String tempRating, Resto resto) {
        boolean result = false;
        if (TextUtils.isEmpty(tempRating)) {
            result = true;
            return result;
        }
        Double rating = Double.valueOf(tempRating);//5
        Double dbRating = Double.valueOf(resto.r_rating); //3.5
        if (dbRating >= (rating - 1) && dbRating <= rating) {
            result = true;
        }

        /*switch (rating) {
            case "1": {
                if (Float.valueOf(databaserupee) <1) {
                    result = true;
                    break;
                }
            }
            case "2": {
                if (Float.valueOf(databaserupee) >=1 || Float.valueOf(databaserupee) <2) {
                    result = true;
                    break;
                }
            }
            case "3": {

                if (Float.valueOf(databaserupee) >=2 || Float.valueOf(databaserupee)< 3) {
                    result = true;
                    break;


                }

            }
            case "4": {
                if (Float.valueOf(databaserupee) >=3 || Float.valueOf(databaserupee)<4) {
                    result = true;
                    break;


                }
            }
            case "5": {
                if (Float.valueOf(databaserupee) >=4) {
                    result = true;
                    break;

                }


            }
*/
        return result;
    }


}





