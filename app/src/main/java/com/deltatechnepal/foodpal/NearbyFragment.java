package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.NearbyEventAdapter;
import com.deltatechnepal.adapter.NearbyRestoAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.example.easywaylocation.EasyWayLocation;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.AddressComponentType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NearbyFragment extends Fragment{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1,mParam2,recentAddress,name;
    private Context mContext;
    private ImageButton filter;
    private EditText etSearchText;
    private PlacesAutocompleteTextView actvPlace;
    private double activeLatitude,activeLongitude;
    ImageButton gototop;
    ScrollView scrollView;

    List<Resto> featuredRestoList;
    public NearbyFragment() {
    }

    public static NearbyFragment newInstance(String param1, String param2) {
        NearbyFragment fragment = new NearbyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setActionBarTitle("Nearby");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mContext = getActivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearby, container, false);
        final View mView = view;
        actvPlace = view.findViewById(R.id.actvPlace);
        actvPlace.setText(MFunction.getMyPrefVal("fullAddress",mContext));
        actvPlace.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                actvPlace.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
                        Log.d("test", "details address name " + details);
                        Log.i("latitude",""+details.geometry.location.lat);
                        Log.i("longitude",""+details.geometry.location.lng);
                        activeLatitude = details.geometry.location.lat;
                        activeLongitude = details.geometry.location.lng;


                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                    }
                });
            }
        });

        etSearchText = view.findViewById(R.id.etSearchText);
        etSearchText.setOnEditorActionListener(new android.widget.EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, android.view.KeyEvent event) {
                if (actionId == android.view.inputmethod.EditorInfo.IME_ACTION_DONE) {
                    fetchData(mView);
                    return true;
                }
                return false;
            }
        });
        fetchData(mView);
        filter=view.findViewById(R.id.search_filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                showFilterDialog(v);
            }
        });
        scrollView = (ScrollView) view.findViewById(R.id.fcontainer);
        scrollView = view.findViewById(R.id.fcontainer);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        MainActivity mainActivity =(MainActivity) getActivity();
        gototop = mainActivity.findViewById(R.id.gotoTop);
        gototop.setVisibility(View.GONE);



        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0,0);
            }
        });

        ImageButton search = (ImageButton)view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchData(mView);
            }
        });

        TextView tvPlaceChange = (TextView)view.findViewById(R.id.tvPlaceChange);
        tvPlaceChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actvPlace.setText(null);
            }
        });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showFilterDialog(View view) {

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // String array for alert dialog multi choice items
        String[] filters = new String[]{
                "Popular",
                "Recent"
//                "Location",
//                "Author",
//                "Liked"
        };

        // Boolean array for initial selected items
        final boolean[] checkedFields = new boolean[]{
                false, // Red
                true // Green
//                false, // Blue
//                true, // Purple
//                false // Olive

        };

        // Convert the color array to list
        final List<String> filterList = Arrays.asList(filters);
        builder.setMultiChoiceItems(filters, checkedFields, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedFields[which] = isChecked;

                // Get the current focused item
                String currentItem = filterList.get(which);

                // Notify the current action
                Toast.makeText(getActivity(),
                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Filter by :");

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i<checkedFields.length; i++){
                    boolean checked = checkedFields[i];
                }
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    private void setRestaurantSection(JsonArray tempArray, View view){
        Double radius = 4d;
        JsonArray filteredArray = new JsonArray();
        for (JsonElement temp: tempArray) {
            Log.i("string",temp.toString());
            Double latitude;
            try {
                latitude = temp.getAsJsonObject().get("lat").getAsDouble();
            } catch(Exception e){
                latitude = 0.0;
            }
            Double longitude;

            try {
                longitude = temp.getAsJsonObject().get("log").getAsDouble();
            } catch(Exception e){
                longitude = 0.0;
            }

            Double resultDistance = MFunction.getDistance(mContext,latitude,longitude,activeLatitude,activeLongitude);
            if(resultDistance <= radius && latitude >0 && latitude>0){
                filteredArray.add(temp);
            }
            Log.i("resultDistance",""+resultDistance);
        }

        RecyclerView  rvRestaurants = (RecyclerView) view.findViewById(R.id.rvRestaurants);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvRestaurants.setHasFixedSize(true);
        rvRestaurants.setNestedScrollingEnabled(true);
        rvRestaurants.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext,filteredArray,false);
        rvRestaurants.swapAdapter(nearbyRestoAdapter,false);
    }

    private void setEventSection(JsonArray tempArray, View view){
        Double radius = 4d;
        JsonArray filteredArray = new JsonArray();
        for (JsonElement temp: tempArray) {
            Log.i("string",temp.toString());
            Double latitude;
            try {
                latitude = temp.getAsJsonObject().get("lat").getAsDouble();
            } catch(Exception e){
                latitude = 0.0;
            }
            Double longitude;

            try {
                longitude = temp.getAsJsonObject().get("log").getAsDouble();
            } catch(Exception e){
                longitude = 0.0;
            }
            Double resultDistance = MFunction.getDistance(mContext,latitude,longitude,activeLatitude,activeLongitude);
            if(resultDistance <= radius && latitude >0 && longitude >0){
                filteredArray.add(temp);
            }
            Log.i("resultDistance",""+resultDistance);
        }
        RecyclerView  rvEvents = (RecyclerView) view.findViewById(R.id.rvEvents);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvEvents.setHasFixedSize(true);
        rvEvents.setNestedScrollingEnabled(true);
        rvEvents.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyEventAdapter = new NearbyEventAdapter(mContext,filteredArray,false);
        rvEvents.swapAdapter(nearbyEventAdapter,false);

    }

    private void fetchData(View view) {

        /*try {

            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

        } catch (Error er){
        }*/

        ((MainActivity) getActivity()).openDialog();
        final View  mView = view;
        String url = MConstant.API_END+"/getNearbyPageData";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("formatted_address",actvPlace.getText().toString());
        params.put("name",etSearchText.getText().toString());
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if((MainActivity) getActivity() != null)((MainActivity) getActivity()).closeDialog();
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            JsonArray restaurants = data.getAsJsonArray("restaurants");
                            JsonArray events = data.getAsJsonArray("events");
                            setRestaurantSection(restaurants,mView);
                            setEventSection(events,mView);
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((MainActivity) getActivity()).closeDialog();
                        Log.e("VolleyError",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}







