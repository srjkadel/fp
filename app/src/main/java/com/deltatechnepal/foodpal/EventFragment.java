package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SearchEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aromajoin.actionsheet.ActionSheet;
import com.aromajoin.actionsheet.OnActionListener;
import com.deltatechnepal.adapter.ExplorerAdapter;
import com.deltatechnepal.adapter.NearbyEventAdapter;
import com.deltatechnepal.adapter.NearbyRestoAdapter;
import com.deltatechnepal.adapter.SearchEventAdapter;
import com.deltatechnepal.adapter.SearchRestoAdapter;
import com.deltatechnepal.adapter.ViewPagerAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class EventFragment extends Fragment implements OnActionListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private RecyclerView rvSearchList, rvFeatured, rvUpcoming;
    List<Event> allevent;
    private View mView;
    JsonArray all;
    private ConstraintLayout cLayout;
    TextView tvView;
    TextView tvFeaturedSection, tvUpcomingSection, tvExplorer, tvfeaturedsectionseemore, tvupcomingsectionseemore;
    GridView gvExplorer;
    private PlacesAutocompleteTextView searchlocation;
    ImageButton gototop, gototop2;
    ScrollView scrollView;
    Spinner spinner;
    JsonArray sponsored, upcoming;


    EditText etSearchview;

    private OnFragmentInteractionListener mListener;

    public EventFragment() {
        // Required empty public constructor
    }

    public static EventFragment newInstance(String param1, String param2) {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setActionBarTitle("Search Events");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mContext = getActivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        etSearchview = (EditText) view.findViewById(R.id.etSearchText);
        cLayout = (ConstraintLayout) view.findViewById(R.id.cLayout);
        tvView = (TextView) view.findViewById(R.id.tvSearchResults);
        mView = view;
        rvSearchList = (RecyclerView) view.findViewById(R.id.rvSearchResult);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvSearchList.setHasFixedSize(true);
        rvSearchList.setNestedScrollingEnabled(true);
        rvSearchList.setLayoutManager(popularLM);
        rvFeatured = (RecyclerView) view.findViewById(R.id.rvFeatured);
        tvFeaturedSection =(TextView) view.findViewById(R.id.tvFeaturedSection);
        tvUpcomingSection=(TextView) view.findViewById(R.id.tvUpcomingSection);
        rvUpcoming =(RecyclerView) view.findViewById(R.id.rvUpcoming);
        tvExplorer =(TextView) view.findViewById(R.id.tvExplorer);
        gvExplorer =(GridView) view.findViewById(R.id.gvExplore);
        searchlocation = (PlacesAutocompleteTextView) view.findViewById(R.id.actvPlace);
        tvfeaturedsectionseemore = view.findViewById(R.id.tvFeaturedSectionseemore);
        tvupcomingsectionseemore = view.findViewById(R.id.tvUpcomingSectionseemore);
        final GridView gvExplore = (GridView) view.findViewById(R.id.gvExplore);




        tvfeaturedsectionseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                rvFeatured.setVisibility(View.GONE);
                tvFeaturedSection.setVisibility(View.GONE);
                tvfeaturedsectionseemore.setVisibility(View.GONE);
               tvupcomingsectionseemore.setVisibility(View.GONE);
                rvUpcoming.setVisibility(View.GONE);
                tvUpcomingSection.setVisibility(View.GONE);
                tvExplorer.setVisibility(View.GONE);
                gvExplore.setVisibility(View.GONE);

               SearchEventAdapter searchEventAdapter = new SearchEventAdapter(mContext,sponsored);
                rvFeatured.swapAdapter(searchEventAdapter,false);

            }
        });

        tvupcomingsectionseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                rvFeatured.setVisibility(View.GONE);
                tvFeaturedSection.setVisibility(View.GONE);
                tvfeaturedsectionseemore.setVisibility(View.GONE);
                tvupcomingsectionseemore.setVisibility(View.GONE);
                rvUpcoming.setVisibility(View.GONE);
                tvUpcomingSection.setVisibility(View.GONE);
                tvExplorer.setVisibility(View.GONE);
                gvExplore.setVisibility(View.GONE);

               SearchEventAdapter searchEventAdapter = new SearchEventAdapter(mContext,upcoming);
                rvFeatured.swapAdapter(searchEventAdapter,false);

            }
        });

        scrollView = view.findViewById(R.id.fcontainer);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        MainActivity mainActivity =(MainActivity) getActivity();
        gototop = mainActivity.findViewById(R.id.gotoTop);
        gototop.setVisibility(View.GONE);



        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0,0);
            }
        });

        //Spinner if client  wants to
//        spinner = (Spinner) view.findViewById(R.id.search_filter_spinner);
//        spinner.setOnItemSelectedListener(this);
//
//        List<String> myoptions =new ArrayList<String>();
//        myoptions.add("Popular");
//        myoptions.add("Upcoming");
//
//        ArrayAdapter<String> dataadapter = new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_item,myoptions);
//        dataadapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
//
//        spinner.setAdapter(dataadapter);
//        spinner.setSelected(false);
//


        if (MFunction.isInternetAvailable(mContext)){

            fetchData(mView);
        }
        else
        {
            allevent = Event.getAllEvent(10);
            String a = "123";
            String b = String.valueOf(a);
            Log.i(b,"NO internet");
            Toast.makeText(mContext, "No internet available "+b, Toast.LENGTH_SHORT).show();
            setSponsored(mView);


        }



        setExplorer(view);
        gvExplore.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String ,String> obj =(HashMap<String ,String>) parent.getItemAtPosition(position);
                String catID = obj.get("cat_id").toString();
                switch (catID) {
                    case "1":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "2":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "3":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case "4":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "5":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "6":{
                        showActionSheet(view);
                        break;
                    }
                }
            }
        });

        etSearchview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String query = s.toString();
                if(s.toString().length()!=0){
                    // cLayout.setVisibility(View.GONE);
                    mView.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
                    rvFeatured.setVisibility(View.GONE);
                    tvFeaturedSection.setVisibility(View.GONE);
                    tvfeaturedsectionseemore.setVisibility(View.GONE);
                    tvupcomingsectionseemore.setVisibility(View.GONE);
                    rvUpcoming.setVisibility(View.GONE);
                    tvUpcomingSection.setVisibility(View.GONE);
                    tvExplorer.setVisibility(View.GONE);
                    gvExplore.setVisibility(View.GONE);
                    //gototop2.setVisibility(View.VISIBLE);
                    gototop.setVisibility(View.GONE);

                    Search(query);
                }
                else {
                    mView.findViewById(R.id.groupSearchResult).setVisibility(View.GONE);
                    //    cLayout.setVisibility(View.VISIBLE);
                    rvFeatured.setVisibility(View.VISIBLE);
                    tvFeaturedSection.setVisibility(View.VISIBLE);
                    rvUpcoming.setVisibility(View.VISIBLE);
                    tvUpcomingSection.setVisibility(View.VISIBLE);
                    tvExplorer.setVisibility(View.VISIBLE);
                    tvfeaturedsectionseemore.setVisibility(View.VISIBLE);
                    tvupcomingsectionseemore.setVisibility(View.VISIBLE);
                    gvExplore.setVisibility(View.VISIBLE);
                    // gototop2.setVisibility(View.GONE);
                    gototop.setVisibility(View.VISIBLE);



                }

            }
        });


        return view;
    }

    public  void Search(String searchtext){
        List<Event> searchlist = new ArrayList<>();
        String query = searchtext.toLowerCase();
        allevent= Event.getAllEvent(100);
        if(true){
            for (Event item:allevent) {
                String name = item.name.toLowerCase();
                String address = item.formatted_address.toLowerCase();

                if(name.contains(query) && checkaddress(address)){
                    searchlist.add(item);
                }
            }
            SearchEventAdapter searchRestoAdapter = new SearchEventAdapter(mContext, searchlist);
            rvSearchList.setAdapter(searchRestoAdapter);

        }
    }
    private boolean checkaddress(String myaddress){
        boolean result = true;
        String address = String.valueOf(searchlocation.getText()).toLowerCase();
        String [] arrayactiveplace = address.split(",");

        if(TextUtils.isEmpty(address)) return result;
        String[] formattedplace = myaddress.split(",");
        for(String templocation: formattedplace){
            if(address.contains(templocation.trim())){
                result = true;
                break;
            }
            else {
                result = false;

            }
        }

        return result;


    }



    private void showActionSheet(View view) {
        ActionSheet actionSheet = new ActionSheet(getActivity());
        actionSheet.setTitle("Choose Any Category");
        actionSheet.setSourceView(view);
        actionSheet.addAction("Extra Category 6",ActionSheet.Style.DEFAULT,this);
        actionSheet.addAction("Extra Category 7",ActionSheet.Style.DEFAULT,this);
        actionSheet.addAction("Extra Category 8",ActionSheet.Style.DEFAULT,this);
        actionSheet.show();
    }

    private void setSponsored(View view) {
        List<Event> tempList = Event.getSponsoredEvent(10);
        //List<Resto> allList = Resto.getAllResto(1000);

        RecyclerView rvFeatured = (RecyclerView) view.findViewById(R.id.rvFeatured);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvFeatured.setHasFixedSize(true);
        rvFeatured.setNestedScrollingEnabled(true);
        rvFeatured.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyeventadapter = new NearbyEventAdapter(mContext, tempList,false);
        rvFeatured.swapAdapter(nearbyeventadapter, false);
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSelected(ActionSheet actionSheet, String title) {
        /*Toast.makeText(getActivity(),""+title, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(mContext,SearchRestaurantActivity.class);
        intent.putExtra("resto_cat_id","100");
        mContext.startActivity(intent);
        actionSheet.dismiss();*/
    }

//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        String item = parent.getItemAtPosition(position).toString();
//        Toast.makeText(parent.getContext(),"Selected item :"+item,Toast.LENGTH_SHORT).show();
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }



    private void fetchData(View view) {
        final View  mView = view;
        ((MainActivity) getActivity()).openDialog();
        String url = MConstant.API_END+"/getEventPageData";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MainActivity activity =(MainActivity) getActivity();
                        if(activity!=null) activity.closeDialog();
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            all = data.getAsJsonArray("sponsored");
                            Event.saveAll(all);
                            sponsored = data.getAsJsonArray("sponsored");
                            upcoming = data.getAsJsonArray("upcoming");
                            setSponsoredEvents(sponsored,mView);
                            setUpcomingEvents(upcoming,mView);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainActivity activity =(MainActivity) getActivity();
                        if(activity!=null) activity.closeDialog();
                        Log.e("VolleyError",error.toString());
                        setSponsored(mView);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void setSponsoredEvents(JsonArray tempArray, View view){
        RecyclerView rvFeatured = (RecyclerView) view.findViewById(R.id.rvFeatured);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvFeatured.setHasFixedSize(true);
        rvFeatured.setNestedScrollingEnabled(true);
        rvFeatured.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyEventAdapter = new NearbyEventAdapter(mContext,tempArray,false);
        rvFeatured.swapAdapter(nearbyEventAdapter,false);
    }

    private void setUpcomingEvents(JsonArray tempArray, View view){
        RecyclerView rvUpcoming = (RecyclerView) view.findViewById(R.id.rvUpcoming);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvUpcoming.setHasFixedSize(true);
        rvUpcoming.setNestedScrollingEnabled(true);
        rvUpcoming.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyEventAdapter = new NearbyEventAdapter(mContext,tempArray,false);
        rvUpcoming.swapAdapter(nearbyEventAdapter,false);
    }

    private void setExplorer(View view){
        GridView gvExplore = (GridView) view.findViewById(R.id.gvExplore);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String,String> map1 = new HashMap<String,String>();
        map1.put("cat_id","1");
        map1.put("image","conference");
        map1.put("cat_name","Conference");

        HashMap<String,String> map2 = new HashMap<String,String>();
        map2.put("cat_id","2");
        map2.put("image","exhibition");
        map2.put("cat_name","Exhibition");

        HashMap<String,String> map3 = new HashMap<String,String>();
        map3.put("cat_id","3");
        map3.put("image","festival");
        map3.put("cat_name","Festival");

        HashMap<String,String> map4 = new HashMap<String,String>();
        map4.put("cat_id","4");
        map4.put("image","food_drinks");
        map4.put("cat_name","Food_Drinks");

        HashMap<String,String> map5 = new HashMap<String,String>();
        map5.put("cat_id","5");
        map5.put("image","liveevents");
        map5.put("cat_name","Live events");

        HashMap<String,String> map6 = new HashMap<String,String>();
        map6.put("cat_id","6");
        map6.put("image","music");
        map6.put("cat_name","Music ");

        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        tempList.add(map4);
        tempList.add(map5);
        tempList.add(map6);

        ExplorerAdapter explorerAdapter = new ExplorerAdapter(mContext,tempList);
        gvExplore.setAdapter(explorerAdapter);

    }


}
