package com.deltatechnepal.foodpal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.deltatechnepal.adapter.RatingAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.model.MySingleton;
import com.deltatechnepal.model.VolleyMultipartRequest;
import com.deltatechnepal.utility.CustomScrollView;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostReviewActivity extends AppCompatActivity {
    int PHOTO_PICKER_KITKAT = 1, PHOTO_PICKER, position;
    private ProgressDialog pDialog;
    Context mContext;
    private ScrollView svPostReview;
    private EditText editMoreWords;
    private String restoID;
    ProgressDialog mProd;
    private ListView listView;
    private RatingAdapter adapter;
    View base, view1;
    CheckBox cb1, cb2, cb3, cb4, cb5;
    List<Boolean> ratings = new ArrayList<Boolean>();
    List<Integer> storedRatings = new ArrayList<Integer>();
    List<String> storedImages = new ArrayList<String>();
    String picturePath, image1;
    ImageView iv1, iv2, iv3;
    Uri selectedImageUri;
    Uri img1, img2, img3;
    private JsonObjectRequest jsonObjectRequest;
    private JSONObject joData = new JSONObject();
    private RequestQueue rQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_review);
        Toolbar tb = findViewById(R.id.toolbar);
        editMoreWords = findViewById(R.id.editMoreWords);

        setSupportActionBar(tb);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);

        final CustomScrollView svPostReview = findViewById(R.id.svPostReview);

        listView = findViewById(R.id.listView);
        List<Boolean> ratingBar = new ArrayList<Boolean>();


        //storedRatings for storing ratings value to be sent to serever and ratingBar just for adapter
        storedRatings.add(0);
        for (int i = 1; i < 5; i++) {
            ratingBar.add(true);
            storedRatings.add(i);
        }
        for (int i = 1; i <= 3; i++) {
            storedImages.add("ImagePath");
        }

        adapter = new RatingAdapter(PostReviewActivity.this, ratingBar);
        listView.setAdapter(adapter);

        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);


        // Logic for disabling main scroll while inside edittext to let it scroll
        final View activityRootView = findViewById(R.id.rootView);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 100) {
                    svPostReview.setEnableScrolling(false);
                } else {
                    //enter code for hid
                    svPostReview.setEnableScrolling(true);
                }
            }
        });

        restoID = getIntent().getStringExtra("resto_id");

        /*Initializing Progress Dialog*/
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Saving");

        Button btnRecommend = (Button) findViewById(R.id.btnRecommend);
        btnRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new android.support.v7.app.AlertDialog.Builder(mContext)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Are you sure to Recommend?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                recommend();
                            }
                        })
                        .setNegativeButton("No", null).show();
            }
        });


        Button btnPublish = (Button) findViewById(R.id.btnPublish);
        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get data from form
                System.out.println("RESPONSE RETROREVIEW");
                // postReview();
                String url = MConstant.API_END + "/postReview";

                try {
                    joData.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
                    joData.put("user_id", MFunction.getMyPrefVal("user_id", mContext));
                    joData.put("restaurant_id", restoID);
                    joData.put("food", storedRatings.get(0) + 1);
                    joData.put("service", storedRatings.get(1) + 1);
                    joData.put("ambience", storedRatings.get(2) + 1);
                    joData.put("cleanliness", storedRatings.get(3) + 1);
                    joData.put("review", editMoreWords.getText().toString());
                    joData.put("image1", image1);


                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("RESPONSE RETROREVIEW joError " + e.getMessage());
                }


                VolleyStandard(url);
            }
        });


        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(mContext, "Working",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void selectImage1(View v) {
        view1 = v;
        openGallery();
        storedImages.set(0, picturePath);
    }

    public void selectImage2(View v) {
        view1 = v;
        openGallery();
        storedImages.set(1, picturePath);
    }

    public void selectImage3(View v) {
        view1 = v;
        openGallery();
        storedImages.set(2, picturePath);
    }


    public void openGallery() {

        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PHOTO_PICKER_KITKAT);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/jpeg");
            startActivityForResult(intent, PHOTO_PICKER);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) return;
        if (null == data) return;
        Uri originalUri = null;
        if (requestCode == PHOTO_PICKER) {
            selectedImageUri = data.getData();
        } else if (requestCode == PHOTO_PICKER_KITKAT) {
            selectedImageUri = data.getData();
            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.
            if (Build.VERSION.SDK_INT >= 19)
                getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
        }


        Cursor returnCursor =
                getContentResolver().query(selectedImageUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         * move to the first row in the Cursor, get the data,
         * and display it.
         */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String imgFileName=returnCursor.getString(nameIndex);
        Bitmap imgBitmap=null;
        try {
            imgBitmap=getBitmapFromUri(selectedImageUri);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println();
        }
        String url = MConstant.API_END + "/postReview";
        //uploadPhoto(imgBitmap, url);



        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);

        System.out.println("PICTURE PATH: "+picturePath);

        switch (view1.getId()) {
            case R.id.iv1:
                iv1.setImageURI(selectedImageUri);

                    /*Bitmap myImg = BitmapFactory.decodeFile(picturePath);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    myImg.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byte_arr = stream.toByteArray();
                    image1 = Base64.encodeToString(byte_arr, 0);
                    */

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), selectedImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, outputStream);
                image1 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

                System.out.println("RESPONSE RETROREVIEW image1 "+image1);
                break;
            case R.id.iv2:
                iv2.setImageURI(selectedImageUri);
                img2 = selectedImageUri;
                break;
            case R.id.iv3:
                iv3.setImageURI(selectedImageUri);
                img3 = selectedImageUri;
                break;
        }

        cursor.close();
    }


    // Implementing Rating Logic
    public void Checked(View v) {
        ViewGroup containerParent = (ViewGroup) v.getParent();
        cb1 = containerParent.findViewById(R.id.cbLevel1);
        cb2 = containerParent.findViewById(R.id.cbLevel2);
        cb3 = containerParent.findViewById(R.id.cbLevel3);
        cb4 = containerParent.findViewById(R.id.cbLevel4);
        cb5 = containerParent.findViewById(R.id.cbLevel5);

        ratings.clear();
        for (int i = 0; i < 5; i++) {
            ratings.add(false);
        }
        RatingtLogic();

        position = listView.getPositionForView(v);

        switch (v.getId()) {
            case R.id.cbLevel1:

                Looper(0);
                RatingtLogic();
                break;
            case R.id.cbLevel2:

                Looper(1);
                RatingtLogic();
                break;
            case R.id.cbLevel3:
                Looper(2);
                RatingtLogic();
                break;
            case R.id.cbLevel4:
                Looper(3);
                RatingtLogic();
                break;
            case R.id.cbLevel5:
                Looper(4);
                RatingtLogic();
                break;
        }

        Toast.makeText(mContext, "Rated " + (storedRatings.get(position)),
                Toast.LENGTH_LONG).show();
    }

    public void Looper(int i) {
        for (int j = 0; j <= i; j++) {
            ratings.set(j, true);
        }
        storedRatings.set(position, i);

    }


    public void RatingtLogic() {
        cb1.setChecked(ratings.get(0));
        cb2.setChecked(ratings.get(1));
        cb3.setChecked(ratings.get(2));
        cb4.setChecked(ratings.get(3));
        cb5.setChecked(ratings.get(4));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    private void recommend() {
        String url = MConstant.API_END + "/recommendRestaurant";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("restaurant_id", restoID);
        params.put("unique_id", MFunction.getMyPrefVal("uniqueID", mContext));
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            int rCount = response.getInt("data");
                            if (status) {
                                String msg = response.getString("message");
                                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                                /*if(rCount ==1) {
                                    tvRecommendation.setText(""+totalRecommendation+1);
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

/*
    private void postReview() {
        mProd.show();
        Log.i("inside ", "postReview");
        List<Integer> ratings = storedRatings;
        int food = storedRatings.get(0) + 1;
        int service = storedRatings.get(1) + 1;
        int ambience = storedRatings.get(2) + 1;
        int cleanliness = storedRatings.get(3) + 1;
        EditText editMoreWords = (EditText) findViewById(R.id.editMoreWords);
        String url = MConstant.API_END + "/postReview";

        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("user_id", MFunction.getMyPrefVal("user_id", mContext));
        params.put("restaurant_id", restoID);
        params.put("food", "" + food);
        params.put("service", "" + service);
        params.put("ambience", "" + ambience);
        params.put("cleanliness", "" + cleanliness);
        params.put("review", editMoreWords.getText().toString());
        params.put("image1", image1);

        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();

                        System.out.println("RESPONSE RETROREVIEW " + response);

                        try {
                            boolean status = response.getBoolean("status");
                            if (status) {
                                JSONObject obj = response.getJSONObject("data");
                                String msg = response.getString("message");
                                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error", error.toString());
                        finish();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
*/


    private void VolleyStandard(String url) {

        System.out.println("RESPONSE RETROREVIEW inside volley jodata " + joData);

        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, joData, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    //TODO POSTREVIEW RESPONSE
                        /*{"status":true,"message":"You have successfully posted your review.",
                        "data":{"user_id":"60","restaurant_id":"7","food":1,"service":1,"ambience":1,"cleanliness":1,"comment":"fdsfsfdsafsa","rating":1,
                        "review_date":"2019-06-27 19:15:16","image":"4b2bde0785fb19ffd26196934c270aa8.png"}}*/

                        if(response.getBoolean("status"))
                        {
                            Toast.makeText(mContext,response.getString("message"),Toast.LENGTH_SHORT).show();
                        }



                } catch (Exception e) {
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            /**
             * Returns a list of extra HTTP headers to go along with this request. Can throw {@link
             * AuthFailureError} as authentication may be required to provide these values.
             *
             * @throws AuthFailureError In the event of auth failure
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;

            }
        };
        jsonObjectRequest.setTag("OBTAINMETHOD");

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


    private void uploadPhoto(final Bitmap bitmap, String urlUpoadImage) {
        System.out.println("RESPONSE:> INIT  in the upload function.");

        ;
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, urlUpoadImage,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        Log.d("ressssssoo", new String(response.data));
                        rQueue.getCache().clear();
                        System.out.println("RESPONSE:> INIT " + new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            System.out.println("RESPONSE:> " + jsonObject);

                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                            String fileName = jsonObject.getJSONObject("data").getJSONObject("user").getString("filename");
                            jsonObject.toString().replace("\\\\", "");

                            if (jsonObject.getString("status").equals("true")) {


                                String url = "http://cubetech.com.np/kyc/uploads/" + fileName;
//                                for (int i = 0; i < dataArray.length(); i++) {
//                                    JSONObject dataobj = dataArray.getJSONObject(i);
//                                    url = dataobj.optString("pathToFile");
//                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error == null || error.networkResponse == null) {
                            return;
                        }

                        String body;
                        //get status code here
                        final String statusCode = String.valueOf(error.networkResponse.statusCode);
                        //get response body and parse with appropriate encoding
                        try {
                            body = new String(error.networkResponse.data, "UTF-8");
                            System.out.println(" RESPONSE:> ERROR:>>> " + body + " status code " + statusCode);
                        } catch (UnsupportedEncodingException e) {
                            // exception
                        }

                        //do stuff with the body...
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "register_image"); // add string parameters
                params.put("path", "uploads"); // add string parameters
                return params;
            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("filename", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };


        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        rQueue = Volley.newRequestQueue(PostReviewActivity.this);
        rQueue.add(volleyMultipartRequest);


    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }



    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}