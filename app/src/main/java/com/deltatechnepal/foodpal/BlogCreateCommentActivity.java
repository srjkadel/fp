package com.deltatechnepal.foodpal;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class BlogCreateCommentActivity extends AppCompatActivity {

    private Button btnComment;
    private Button btnCancel;
    public static final String BLOG_COMMENT="blog_comment";
    private EditText etComment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_create_comment);

        setTitle("Comment");

        btnCancel =findViewById(R.id.btnCancel);
        btnComment =findViewById(R.id.btnComment);
        etComment=findViewById(R.id.etComment);


        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(TextUtils.isEmpty(etComment.getText())){
                   etComment.setError("Comment should not be blank.");
               }else {
                Intent i=new Intent();
                i.putExtra(BLOG_COMMENT,etComment.getText().toString());
                setResult(RESULT_OK,i);
                finish();//finises the auth activity only
               }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }



}
