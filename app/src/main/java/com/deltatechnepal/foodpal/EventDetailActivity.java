package com.deltatechnepal.foodpal;

import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.aromajoin.actionsheet.ActionSheet;
import com.aromajoin.actionsheet.OnActionListener;
import com.deltatechnepal.adapter.ExplorerAdapter;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static java.lang.Math.abs;

public class EventDetailActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,OnMapReadyCallback,OnActionListener {
    @BindView(R.id.ivBannerImage) ImageView ivBannerImage;
    @BindView(R.id.tvBoldDate) TextView tvBoldDate;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvDistance) TextView tvDistance;
    @BindView(R.id.tvDescriptionContent) ExpandableTextView tvDescriptionContent;

    @BindView(R.id.btnBook) Button btnBook;
    @BindView(R.id.btnAddToCalendar) Button btnAddToCalendar;

    @BindView(R.id.tvWebVal) TextView tvWebVal;
    @BindView(R.id.tvNumbers) TextView tvNumbers;
    @BindView(R.id.ibTwiter) ImageButton ibTwiter;
    @BindView(R.id.ibInsta) ImageButton ibInsta;
    @BindView(R.id.ibFB) ImageButton ibFB;
    @BindView(R.id.tvActivitiesContent) TextView tvActivitiesContent;
    @BindView(R.id.tvTicketInfoContent) TextView tvTicketInfoContent;

    TextView mOutputText;


    GoogleAccountCredential mCredential;
    private JsonObject mEvent;
    ProgressDialog mProd;

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final String BUTTON_TEXT = "Call Google Calendar API";
    private static final String PREF_ACCOUNT_NAME = "accountName";
    Context mContext;
    ScrollView scrollView;
    ImageButton gototop;
    TextView tvPastEvent, tvdescription, tvticket, tvactivities;
    ImageButton share,bookmark;


    Button overview,activities,ticketInfo,pastEvents;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);
        mContext = this;

        /*Initializing Progress Dialog*/
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Fetching Data...");

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        /*Transparent Action Bar*/
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getSupportActionBar().setElevation(0);

        /*Setting Title of Activity*/
        this.setTitle("");

        JsonParser jsonParser = new JsonParser();
        mEvent = (JsonObject) jsonParser.parse(getIntent().getStringExtra("event"));
        //Log.i("mEvent",mEvent.getAsString());

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Bookin Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        btnAddToCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addToCalender(view);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        tvWebVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,WebViewActivity.class);
                intent.putExtra("url",mEvent.get("website").getAsString());
                mContext.startActivity(intent);
            }
        });

        tvNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mEvent.get("contact_no").getAsString())));
            }
        });

        ibTwiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mContext,WebViewActivity.class);
                intent.putExtra("url",mEvent.get("twitterUrl").getAsString());
                mContext.startActivity(intent);*/
            }
        });

        ibFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mContext,WebViewActivity.class);
                intent.putExtra("url",mEvent.get("facebookUrl").getAsString());
                mContext.startActivity(intent);*/
            }
        });

        ibInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mContext,WebViewActivity.class);
                intent.putExtra("url",mEvent.get("instagramUrl").getAsString());
                mContext.startActivity(intent);*/
            }
        });

        setExplorer();
        setBasicData();


        scrollView = findViewById(R.id.svEventDetail);
        gototop = findViewById(R.id.ontop);
        overview = findViewById(R.id.btnOverview);
        activities = findViewById(R.id.btnActivities);
        ticketInfo = findViewById(R.id.btnTicketInfo);
        pastEvents = findViewById(R.id.btnPastEvent);


        tvPastEvent = findViewById(R.id.tvPastEvent);
        tvdescription = findViewById(R.id.tvDescripion);
        tvactivities = findViewById(R.id.tvActivities);
        tvticket = findViewById(R.id.tvTicketInfo);



        share = findViewById(R.id.action_share);
        bookmark = findViewById(R.id.action_bookmark);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,MConstant.SERVER+"/restaurant-details/"+mEvent.get("slug").getAsString());
                startActivity(Intent.createChooser(sharingIntent, "Share"));

            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(mEvent.get("id").getAsString(),"2",mContext);

            }
        });


        overview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0,tvdescription.getTop());
            }
        });

        activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0,tvactivities.getTop());
            }
        });
        ticketInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0,tvticket.getTop());
            }
        });
        pastEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0,tvPastEvent.getTop());
            }
        });

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        gototop.setVisibility(View.GONE);


        //goto top after click button
        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0,0);
            }
        });


    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_restodetail_option, menu);
//        return true;
//    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_share: {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,MConstant.SERVER+"/restaurant-details/"+mEvent.get("slug").getAsString());
                startActivity(Intent.createChooser(sharingIntent, "Share"));
                return true;
            }
            case R.id.action_bookmark: {
                if(!TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(mEvent.get("id").getAsString(),"2",mContext);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }  */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLang = new LatLng(Double.parseDouble(mEvent.get("lat").getAsString()), Double.parseDouble(mEvent.get("log").getAsString()));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

            }
        });
        googleMap.setMyLocationEnabled(false);
        //googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLang, 13));
        //googleMap.getUiSettings().setMapToolbarEnabled(true);//enable default buttons when marker is clicked
        //googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.addMarker(new MarkerOptions()
                .title(mEvent.get("name").getAsString())
                .snippet(mEvent.get("formatted_address").getAsString())
                .position(latLang));
    }


    private void getResultsFromApi() {
        if (! isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (! isDeviceOnline()) {
            mOutputText.setText("No network connection available.");
        }
        else {
            //new MakeRequestTask(mCredential).execute();
        }
    }


    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(this, android.Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS, android.Manifest.permission.GET_ACCOUNTS);
        }
    }


    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    mOutputText.setText(
                            "This app requires Google Play Services. Please install " +
                                    "Google Play Services on your device and relaunch this app.");
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }


    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }


    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }


    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                EventDetailActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }


    private com.google.api.services.calendar.Calendar mService = null;

    private void setBasicData(){
        String imageUrl = MConstant.SERVER+"/uploads/events/banner/1350x500"+MFunction.getStringVal(mEvent,"banner_image");
        Picasso.with(mContext)
                .load(imageUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.logo))
                .error(mContext.getResources().getDrawable(R.drawable.logo))
                .into(ivBannerImage);

        tvBoldDate.setText(getBoldDate(MFunction.getStringVal(mEvent,"start_date"),"MMM dd"));

        tvName.setText(mEvent.get("name").getAsString());

        tvAddress.setText(mEvent.get("formatted_address").getAsString());

        tvDate.setText(MFunction.getFormattedDate(mEvent.get("start_date").getAsString(),mContext));

        Double distance = MFunction.getDistance(mContext,mEvent.get("lat").getAsString(),mEvent.get("log").getAsString());
        tvDistance.setText(distance.toString()+"KM Around");

        String description = mEvent.get("description").getAsString();
        tvDescriptionContent.setText(Html.fromHtml(description));
        final Button buttonToggle = (Button) this.findViewById(R.id.btnToggleDescriptionContent);
        // set interpolators for both expanding and collapsing animations
        tvDescriptionContent.setInterpolator(new OvershootInterpolator());
        // or set them separately
        tvDescriptionContent.setExpandInterpolator(new OvershootInterpolator());
        tvDescriptionContent.setCollapseInterpolator(new OvershootInterpolator());
        // toggle the ExpandableTextView
        buttonToggle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                buttonToggle.setText(tvDescriptionContent.isExpanded() ? "Read More" : "Show Less");
                tvDescriptionContent.toggle();
            }
        });

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragmentEvent);
        mapFragment.getMapAsync(this);

        /*Disableing Parent Scrolling when map is scrolled using two fingers*/
        final ScrollView svEventDetail = (ScrollView) findViewById(R.id.svEventDetail);
        ImageView ivMapTransparent = (ImageView) findViewById(R.id.ivMapTransparent);
        ivMapTransparent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        svEventDetail.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        svEventDetail.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        svEventDetail.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        tvWebVal.setText(mEvent.get("website").getAsString());
        tvNumbers.setText(mEvent.get("contact_no").getAsString());

        String activities = mEvent.get("activities").getAsString();
        tvActivitiesContent.setText(Html.fromHtml(activities));

        String ticketInfo = mEvent.get("ticket_info").getAsString();
        tvTicketInfoContent.setText(Html.fromHtml(ticketInfo));
    }

    private void setExplorer(){
        GridView gvExplore = (GridView) findViewById(R.id.gvExplore);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String,String> map1 = new HashMap<String,String>();
        map1.put("cat_id","1");
        map1.put("image","conference");
        map1.put("cat_name","Conference");

        HashMap<String,String> map2 = new HashMap<String,String>();
        map2.put("cat_id","2");
        map2.put("image","exhibition");
        map2.put("cat_name","Exhibition");

        HashMap<String,String> map3 = new HashMap<String,String>();
        map3.put("cat_id","3");
        map3.put("image","festival");
        map3.put("cat_name","Festival");

        HashMap<String,String> map4 = new HashMap<String,String>();
        map4.put("cat_id","4");
        map4.put("image","food_drinks");
        map4.put("cat_name","Food_Drinks");

        HashMap<String,String> map5 = new HashMap<String,String>();
        map5.put("cat_id","5");
        map5.put("image","liveevents");
        map5.put("cat_name","Live events");

        HashMap<String,String> map6 = new HashMap<String,String>();
        map6.put("cat_id","6");
        map6.put("image","music");
        map6.put("cat_name","Music ");


        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        tempList.add(map4);
        tempList.add(map5);


        ExplorerAdapter explorerAdapter = new ExplorerAdapter(mContext,tempList);
        gvExplore.setAdapter(explorerAdapter);

        gvExplore.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String ,String> obj =(HashMap<String ,String>) parent.getItemAtPosition(position);
                String catID = obj.get("cat_id").toString();
                switch (catID) {
                    case "1":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "2":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "3":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case "4":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "5":{
                        Toast.makeText(mContext,catID, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case "6":{
                        showActionSheet(view);
                        break;
                    }
                }
            }
        });
    }

    private void addToCalender(View view) throws ParseException {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");

        Date startDate = MFunction.getStrToDate(mEvent.get("start_date").getAsString(),"yyyy-MM-dd HH:mm:ss");
        long startTime = startDate.getTime();

        Date endDate = MFunction.getStrToDate(mEvent.get("end_date").getAsString(),"yyyy-MM-dd HH:mm:ss");
        long endTime = endDate.getTime();

        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,endTime);
        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);

        intent.putExtra(CalendarContract.Events.TITLE, mEvent.get("name").getAsString());
        intent.putExtra(CalendarContract.Events.DESCRIPTION,  mEvent.get("description").getAsString());
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, mEvent.get("formatted_address").getAsString());
        intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
        startActivity(intent);
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private String getBoldDate(String tempDate,String toFormat){
        SimpleDateFormat fromDFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat toDFormat=new SimpleDateFormat(toFormat);
        String finalDate= null;
        try {
            Date date = fromDFormat.parse(tempDate);
            finalDate =   toDFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return finalDate;
    }

    private void showActionSheet(View view) {
        ActionSheet actionSheet = new ActionSheet(mContext);
        actionSheet.setTitle("Choose Any Category");
        actionSheet.setSourceView(view);
        actionSheet.addAction("Extra Category 6",ActionSheet.Style.DEFAULT,this);
        actionSheet.addAction("Extra Category 7",ActionSheet.Style.DEFAULT,this);
        actionSheet.addAction("Extra Category 8",ActionSheet.Style.DEFAULT,this);
        actionSheet.show();
    }

    @Override
    public void onSelected(ActionSheet actionSheet, String title) {

    }
}
