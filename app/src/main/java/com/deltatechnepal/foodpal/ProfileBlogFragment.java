package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.ProfileBlogAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProfileBlogFragment extends Fragment {
    private Context mContext;
    private String userID;
    private JsonArray blogData;

    private OnFragmentInteractionListener mListener;
    private RecyclerView rvBlog;
    private RecyclerView.LayoutManager mLayoutManager;
    public List<Blog> blogList = new ArrayList<Blog>();

    //public FloatingActionButton fabBlogCreate;
    public Button fabBlogCreate;

    public ProfileBlogFragment() {
        // Required empty public constructor
    }

    public static ProfileBlogFragment newInstance(String param1, String param2) {
        ProfileBlogFragment fragment = new ProfileBlogFragment();
        Bundle args = new Bundle();
        args.putString("blogData", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String temp = getArguments().getString("blogData");
            JsonParser jsonParser = new JsonParser();
            blogData = (JsonArray) jsonParser.parse(temp);
        }
        mContext = getActivity();
        userID = MFunction.getMyPrefVal("user_id",mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_blog, container, false);

        /*Setting Recycler View Blog*/
        setRvBlog(blogData, view);

        fabBlogCreate = (Button) view.findViewById(R.id.fabCreateBlog);
        if (fabBlogCreate != null) {
            fabBlogCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!MFunction.isInternetAvailable(mContext)) {
                        Snackbar.make(view, "Please Try when Internet Is Available", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        return;
                    }

                    Intent intent = new Intent(getActivity(), CreateBlogActivity.class);
                    intent.putExtra("test", "test");
                    startActivity(intent);
                }
            });
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setRvBlog(JsonArray tempArray, View view){
        rvBlog = (RecyclerView) view.findViewById(R.id.rvBlog);
        rvBlog.setHasFixedSize(true);
        rvBlog.setNestedScrollingEnabled(true);
        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        rvBlog.setLayoutManager(mLayoutManager);

        ProfileBlogAdapter profileBlogAdapter = new ProfileBlogAdapter(mContext,tempArray);
        rvBlog.swapAdapter(profileBlogAdapter,false);
    }

    /*public void fetchBlogs() {
        mProd.show();
        String url = MConstant.API_END+"/getAllBlog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("author",userID);
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        try {
                            JSONArray tempArray = response.getJSONArray("data");
                            for(int i = 0;i<tempArray.length();i++) {
                                JSONObject obj = tempArray.getJSONObject(i);
                                *//*BlogModel blog = new BlogModel();
                                blog.setBlogID(obj.getString("id"));
                                blog.setAuthor(obj.getString("author"));
                                blog.setTitle(obj.getString("title"));
                                blog.setSlug(obj.getString("slug"));
                                blog.setSummary(obj.getString("summary"));
                                blog.setDetail(obj.getString("details"));
                                blog.setImage(obj.getString("image"));
                                blog.setCreatedAt(obj.getString("created_at"));
                                blog.setReadingTime(obj.getString("readingtime"));
                                blog.setStatus(obj.getString("status"));
                                blog.setTags(obj.getString("tags"));
                                blog.setCreatorRole(obj.getString("creator_role"));
                                blogList.add(blog);*//*

                                Blog ormBlog = Blog.getItem(obj.getString("id"));
                                if(ormBlog == null) { ormBlog = new Blog();}
                                ormBlog.blog_id = obj.getString("id");
                                ormBlog.author = obj.getString("author");
                                ormBlog.title = obj.getString("title");
                                ormBlog.slug = obj.getString("slug");
                                ormBlog.summary = obj.getString("summary");
                                ormBlog.details = obj.getString("details");
                                ormBlog.image = obj.getString("image");
                                ormBlog.created_at = obj.getString("created_at");
                                ormBlog.readingtime = obj.getString("readingtime");
                                ormBlog.status = obj.getString("status");
                                ormBlog.tags = obj.getString("tags");
                                ormBlog.creator_role = obj.getString("creator_role");
                                ormBlog.save();
                                blogList.add(ormBlog);
                            }
                            mAdapter = new ProfileBlogAdapter(mContext,blogList);
                            rvBlog.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        blogList = Blog.getAllBlogByUserID(userID);
                        mAdapter = new ProfileBlogAdapter(mContext,blogList);
                        rvBlog.setAdapter(mAdapter);
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }*/
}
