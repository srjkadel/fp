package com.deltatechnepal.foodpal;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.ormmodel.RestoCat;
import com.deltatechnepal.utility.DataProvider;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.StringUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateRestoActivity extends AppCompatActivity implements  RangeTimePickerDialog.ISelectedTime{
    int PLACE_PICKER_REQUEST = 1;
    int PICK_LOGO = 200;
    int PICK_BANNER_IMAGE = 300;

    @BindView(R.id.svCreateResto) ScrollView svCreateResto;
    @BindView(R.id.etName) EditText etName;
    @BindView(R.id.etRestoCat) Button etRestoCat;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etPhone) EditText etPhone;
    @BindView(R.id.etWeb) EditText etWeb;
    @BindView(R.id.etFBUrl) EditText etFBUrl;
    @BindView(R.id.etInstaUrl) EditText etInstaUrl;
    @BindView(R.id.etAverageCost) EditText etAverageCost;

    @BindView(R.id.tvChooseLocation) TextView tvChooseLocation;

    @BindView(R.id.rbYes) RadioButton rbYes;
    @BindView(R.id.rbNo) RadioButton rbNo;

    @BindView(R.id.cbIndoor) CheckBox cbIndoor;
    @BindView(R.id.cbOutdoor) CheckBox cbOutdoor;

    @BindView(R.id.rbCardAndCash) RadioButton rbCardAndCash;
    @BindView(R.id.rbCash) RadioButton rbCash;


    @BindView(R.id.btnAddRestoLogo) Button btnAddRestoLogo;
    @BindView(R.id.btnAddRestobanner) Button btnAddRestobanner;
    @BindView(R.id.btnAddResto) Button btnAddResto;

    @BindView(R.id.etAmen1) EditText etAmen1;
    @BindView(R.id.etAmen2) EditText etAmen2;
    @BindView(R.id.etAmen3) EditText etAmen3;
    @BindView(R.id.etAmen4) EditText etAmen4;
    @BindView(R.id.etAmen5) EditText etAmen5;
    @BindView(R.id.btnAddAmen1) FloatingActionButton btnAddAmen1;
    @BindView(R.id.btnAddAmen2) FloatingActionButton btnAddAmen2;
    @BindView(R.id.btnAddAmen3) FloatingActionButton btnAddAmen3;
    @BindView(R.id.btnAddAmen4) FloatingActionButton btnAddAmen4;
    @BindView(R.id.btnAddAmen5) FloatingActionButton btnAddAmen5;
    @BindView(R.id.gAmen2) Group gAmen2;
    @BindView(R.id.gAmen3) Group gAmen3;
    @BindView(R.id.gAmen4) Group gAmen4;
    @BindView(R.id.gAmen5) Group gAmen5;



    @BindView(R.id.etCuisin1) EditText etCuisin1;
    @BindView(R.id.etCuisin2) EditText etCuisin2;
    @BindView(R.id.etCuisin3) EditText etCuisin3;
    @BindView(R.id.etCuisin4) EditText etCuisin4;
    @BindView(R.id.etCuisin5) EditText etCuisin5;
    @BindView(R.id.btnAddCuisin1) FloatingActionButton btnAddCuisin1;
    @BindView(R.id.btnAddCuisin2) FloatingActionButton btnAddCuisin2;
    @BindView(R.id.btnAddCuisin3) FloatingActionButton btnAddCuisin3;
    @BindView(R.id.btnAddCuisin4) FloatingActionButton btnAddCuisin4;
    @BindView(R.id.btnAddCuisin5) FloatingActionButton btnAddCuisin5;
    @BindView(R.id.gCuisin2) Group gCuisin2;
    @BindView(R.id.gCuisin3) Group gCuisin3;
    @BindView(R.id.gCuisin4) Group gCuisin4;
    @BindView(R.id.gCuisin5) Group gCuisin5;



    @BindView(R.id.etInfo1) EditText etInfo1;
    @BindView(R.id.etInfo2) EditText etInfo2;
    @BindView(R.id.etInfo3) EditText etInfo3;
    @BindView(R.id.etInfo4) EditText etInfo4;
    @BindView(R.id.etInfo5) EditText etInfo5;
    @BindView(R.id.btnAddInfo1) FloatingActionButton btnAddInfo1;
    @BindView(R.id.btnAddInfo2) FloatingActionButton btnAddInfo2;
    @BindView(R.id.btnAddInfo3) FloatingActionButton btnAddInfo3;
    @BindView(R.id.btnAddInfo4) FloatingActionButton btnAddInfo4;
    @BindView(R.id.btnAddInfo5) FloatingActionButton btnAddInfo5;
    @BindView(R.id.gInfo2) Group gInfo2;
    @BindView(R.id.gInfo3) Group gInfo3;
    @BindView(R.id.gInfo4) Group gInfo4;
    @BindView(R.id.gInfo5) Group gInfo5;



    @BindView(R.id.tvSundayChoose) TextView tvSundayChoose;
    @BindView(R.id.tvMondayChoose) TextView tvMondayChoose;
    @BindView(R.id.tvTuesdayChoose) TextView tvTuesdayChoose;
    @BindView(R.id.tvWedChoose) TextView tvWedChoose;
    @BindView(R.id.tvThusChoose) TextView tvThusChoose;
    @BindView(R.id.tvFriChoose) TextView tvFriChoose;
    @BindView(R.id.tvSatChoose) TextView tvSatChoose;

    @BindView(R.id.spinHearAboutUs) Spinner spinHearAboutUs;
    @BindView(R.id.etDescription) EditText etDescription;



    ProgressDialog pDialog;
    Context mContext;
    int clickedTimeChoose;
    List<JSONObject> catList = new ArrayList<JSONObject>();
    Boolean[] checkedFields;
    String[] category;
    /*For Values*/
    private String restoName,categoryIDs,email,phone,webaddress,fbLink,instaLink,averageCost,latitude,longitude,formattedAddress,owner,seating,payment,timingInfo,restoLogo,restoBanner,hearAboutUs,description;
    private String[] prevIDs;
    private Spinner catSpin;
    private RadioGroup rgYesNo,rgCardCash;
    private String indoor,outdoor;
    private String restaurant_timing_from_sun,restaurant_timing_to_sun,restaurant_timing_from_mon,restaurant_timing_to_mon,restaurant_timing_from_tue,restaurant_timing_to_tue,restaurant_timing_from_wed,restaurant_timing_to_wed,restaurant_timing_from_thu,restaurant_timing_to_thu,restaurant_timing_from_fri,restaurant_timing_to_fri,restaurant_timing_from_sat,restaurant_timing_to_sat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_resto);
        ButterKnife.bind(this);
        this.setTitle("Add Your Restaurant");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*Initializing Progress Dialog*/
        pDialog = new ProgressDialog(mContext);
        pDialog.setTitle(getString(R.string.app_name));
        pDialog.setMessage("Saving");

        loadSpinnerData();
        rgYesNo = (RadioGroup) findViewById(R.id.rgYesNo);
        rgCardCash = (RadioGroup) findViewById(R.id.rgCardCash);
        cbIndoor = (CheckBox) findViewById(R.id.cbIndoor);

        getAllRestoCat();

        btnAddRestoLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_LOGO);
            }
        });

        btnAddRestobanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_BANNER_IMAGE);
            }
        });



        /*Click Events*/
        btnAddResto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!MFunction.isInternetAvailable(mContext)){
                    Toast.makeText(mContext, "No Internet, Please Try Later", Toast.LENGTH_SHORT).show();
                    return;
                }
                //TODO need to manage properly validation part
                if(TextUtils.isEmpty(etName.getText())){
                    etName.setError("This is required Field");
                    etName.requestFocus();
                    return;
                } else if(!MFunction.isValidEmail(etEmail.getText())){
                    etEmail.setError("Invalid Email");
                    etEmail.requestFocus();
                    return;
                } else if(!MFunction.isValidPhone(etPhone.getText())){
                    etPhone.setError("Invalid Phone");
                    etPhone.requestFocus();
                    return;
                } else if(!TextUtils.isEmpty(etWeb.getText()) && !MFunction.isValidUrl(etWeb.getText())){
                    etWeb.setError("Invalid Url");
                    etWeb.requestFocus();
                    return;
                }

                restoName = etName.getText().toString();
                //categoryIDs = etRestoCat.getText().toString();
                email = etEmail.getText().toString();
                phone = etPhone.getText().toString();
                webaddress = etWeb.getText().toString();
                fbLink = etFBUrl.getText().toString();
                instaLink = etInstaUrl.getText().toString();
                averageCost = etAverageCost.getText().toString();
                //formattedAddress = tvChooseLocation.getText().toString();
                //latitude
                //longitude
                owner = Integer.toString(rgYesNo.getCheckedRadioButtonId());
                indoor = cbIndoor.isChecked()?"1":"0";
                outdoor = cbOutdoor.isChecked()?"1":"0";
                payment = Integer.toString(rgCardCash.getCheckedRadioButtonId());
                hearAboutUs = spinHearAboutUs.getSelectedItem().toString();
                description = etDescription.getText().toString();

                /*HashMap<String,String> amenMap = new HashMap<String,String>();
                amenMap.put("amen1",etAmen1.getText().toString());
                amenMap.put("amen2",etAmen2.getText().toString());
                amenMap.put("amen3",etAmen3.getText().toString());
                amenMap.put("amen4",etAmen4.getText().toString());
                String amenities = new Gson().toJson(amenMap);*/

                String amenArray[] = new String[5];
                amenArray[0]= etAmen1.getText().toString();
                amenArray[1]= etAmen2.getText().toString();
                amenArray[2]= etAmen3.getText().toString();
                amenArray[3]= etAmen4.getText().toString();
                amenArray[4]= etAmen5.getText().toString();
                String amenities = new Gson().toJson(amenArray);
                Log.i("amenities",amenities);

                /*HashMap<String,String> cuisineMap = new HashMap<String,String>();
                cuisineMap.put("cuisin1",etCuisin1.getText().toString());
                String restaurant_select_cuisine = new Gson().toJson(amenMap);*/
                String cuisineArray[] = new String[5];
                cuisineArray[0]= etCuisin1.getText().toString();
                cuisineArray[1]= etCuisin2.getText().toString();
                cuisineArray[2]= etCuisin3.getText().toString();
                cuisineArray[3]= etCuisin4.getText().toString();
                cuisineArray[4]= etCuisin5.getText().toString();
                String cuisins = new Gson().toJson(cuisineArray);

                /*HashMap<String,String> infoMap = new HashMap<String,String>();
                infoMap.put("info1",etInfo1.getText().toString());
                String restaurant_select_additional_info = new Gson().toJson(infoMap);*/

                String infoArray[] = new String[5];
                infoArray[0] = etInfo1.getText().toString();
                infoArray[1] = etInfo2.getText().toString();
                infoArray[2] = etInfo3.getText().toString();
                infoArray[3] = etInfo4.getText().toString();
                infoArray[4] = etInfo5.getText().toString();
                String additionalInfo = new Gson().toJson(infoArray);

                HashMap<String,String> tMap = new HashMap<String,String>();
                tMap.put("from_sun",restaurant_timing_from_sun);
                tMap.put("to_sun",restaurant_timing_to_sun);
                tMap.put("from_mon",restaurant_timing_from_mon);
                tMap.put("to_mon",restaurant_timing_to_mon);
                tMap.put("from_tue",restaurant_timing_from_tue);
                tMap.put("to_tue",restaurant_timing_to_tue);
                tMap.put("from_wed",restaurant_timing_from_wed);
                tMap.put("to_wed",restaurant_timing_to_wed);
                tMap.put("from_thu",restaurant_timing_from_thu);
                tMap.put("to_thu",restaurant_timing_to_thu);
                tMap.put("from_fri",restaurant_timing_from_fri);
                tMap.put("to_fri",restaurant_timing_to_fri);
                tMap.put("from_sat",restaurant_timing_from_sat);
                tMap.put("to_sat",restaurant_timing_to_sat);
                String timingInfo = new Gson().toJson(tMap);
                Log.i("timingInfo",timingInfo);


                /****************/
                String url = MConstant.API_END+"/addrestaurant";
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
                params.put("user_id", MFunction.getMyPrefVal("user_id",mContext));
                params.put("restaurant_name",restoName);
                params.put("category",categoryIDs);
                params.put("email",email);
                params.put("restaurant_phone",phone);
                params.put("restaurant_website",webaddress);
                params.put("restaurant_facebook",fbLink);
                params.put("restaurant_instagram",instaLink);
                params.put("restaurant_avgcost",averageCost);
                params.put("formatted_address",formattedAddress);//formatted address
                params.put("latitude",latitude);
                params.put("longitude",longitude);
                params.put("owner",owner);
                params.put("seating",indoor);
                params.put("payment","1");
                params.put("amenities",amenities);
                params.put("cuisins",cuisins);
                params.put("info",additionalInfo);
                params.put("timing_info",timingInfo);
                params.put("logo_image",restoLogo);
                params.put("banner_image",restoBanner);
                params.put("hear_about_us",hearAboutUs);
                params.put("description",description);
                pDialog.show();
                JSONObject objRegData = new JSONObject(params);
                Log.i("paramData",objRegData.toString());
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                pDialog.dismiss();
                                try {
                                    boolean status = response.getBoolean("status");
                                    if(status) {
                                        Toast.makeText(mContext, "Restaurant Created", Toast.LENGTH_LONG).show();
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pDialog.dismiss();
                                    finish();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pDialog.dismiss();
                                finish();
                                Log.i("Error",error.toString());
                            }
                        })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };
                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(jsObjRequest);
            }
        });

        tvChooseLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.requestFocusFromTouch();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(CreateRestoActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                Double lati = place.getLatLng().latitude;
                Double longi = place.getLatLng().longitude;
                latitude = String.valueOf(lati);
                longitude = String.valueOf(longi);
                String country = "";
                //String state = "";
                //String city = "";
                String featureName="";
                String locality = "";
                String subLocality = "";
                String subAdminArea = "";
                String postalCode = "";

                Geocoder geoCoder = new Geocoder(((Activity)this).getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder.getFromLocation(lati, longi, 1);
                    if(addresses.size() > 0) {
                        country = ((Address)addresses.get(0)).getCountryName();//country e.g Nepal
                        locality = ((Address)addresses.get(0)).getLocality();//city e.g Bharaul
                        subAdminArea = ((Address)addresses.get(0)).getSubAdminArea(); // State e.g Koshi
                        postalCode = ((Address)addresses.get(0)).getPostalCode();
                        String choosedAddress = subAdminArea+", "+locality+", "+country;
                        formattedAddress = choosedAddress.replace("null","");
                        tvChooseLocation.setText(choosedAddress.replace("null",""));
                    }
                } catch (IOException ex) {
                    //ex.printStackTrace();
                }
            }
        }
        if(requestCode == PICK_LOGO) {
            if(resultCode == RESULT_OK) {
                Uri uri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(),uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                restoLogo = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
                Log.i("uri",uri.toString());
                /*UCrop.of(uri, uri)
                        .withAspectRatio(16, 9)
                        .withMaxResultSize(100, 100)
                        .start(this);*/

                ImageView ivRestoLogo = (ImageView) findViewById(R.id.ivRestoLogo);
                ivRestoLogo.setImageURI(uri);
            }
        }

        if(requestCode == PICK_BANNER_IMAGE) {
            if(resultCode == RESULT_OK) {
                Uri uri2 = data.getData();
                Bitmap bitmap2 = null;
                try {
                    bitmap2 = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(),uri2);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                restoBanner = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
                ImageView ivRestoBanner = (ImageView) findViewById(R.id.ivRestoBanner);
                ivRestoBanner.setImageURI(uri2);
            }
        }
        /*if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            Log.i("resultUri",resultUri.toString());
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.i("cropError",cropError.toString());
        }*/
    }




    public void showCategoryDialog(View v) {

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        List<String> temp1=new ArrayList<String>();
        for(int i=0;i<catList.size();i++){
            JSONObject cat = catList.get(i);
            String name = null;
            try {
                name = cat.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            temp1.add(name);
        }

        final String[] category = new String[catList.size()];
        int index1 = 0;
        for (String object : temp1) {
            category[index1++] = object;
        }


        List<Boolean> temp2=new ArrayList<Boolean>();
        if(!TextUtils.isEmpty(categoryIDs)) {
            prevIDs = categoryIDs.split(",");
        }

        for(int i=0;i<catList.size();i++){
            boolean val= false;
            JSONObject cat = catList.get(i);
            String catID = null;
            try {
                catID = cat.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(prevIDs != null)
            for (String s: prevIDs) {
                if (s.equals(catID)){
                    val = true;
                }
            }
            temp2.add(val);
        }


        final boolean[] checkedFields = new boolean[temp2.size()];
        int index2 = 0;
        for (Boolean object : temp2) {
            checkedFields[index2++] = object;
        }

        builder.setMultiChoiceItems(category, checkedFields, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedFields[which]=isChecked;

            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Restaurant Category");

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                etRestoCat.setText(null);
                categoryIDs = "";
                List<String>  names= new ArrayList<String>();
                List<String>  ids= new ArrayList<String>();

                //String withoutLastComma = String.join(", ", paramList);
                for (int i = 0; i<checkedFields.length; i++){
                    boolean checked = checkedFields[i];
                    if (checked) {
                        JSONObject cat = catList.get(i);
                        String catName = null;
                        String catID = null;
                        try {
                            catName = cat.getString("name");
                            catID = cat.getString("id");
                            names.add(catName);
                            ids.add(catID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                etRestoCat.setText(TextUtils.join(",",names));
                categoryIDs = TextUtils.join(",",ids);
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onSelectedTime(int hr1, int min1, int hr2, int min2) {
        String time = getTimeWithM(hr1,min1)+"-"+getTimeWithM(hr2,min2);
        TextView chooseButton = (TextView) findViewById(clickedTimeChoose);
        Log.d("clickedTimeChoose",""+clickedTimeChoose);
        switch(clickedTimeChoose) {
            case R.id.tvSundayChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvSundayChoose);
                tvValue.setText(time);
                restaurant_timing_from_sun = getTimeWithM(hr1,min1);
                restaurant_timing_to_sun = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
            case R.id.tvMondayChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvMondayChoose);
                tvValue.setText(time);
                restaurant_timing_from_mon = getTimeWithM(hr1,min1);
                restaurant_timing_to_mon = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
            case R.id.tvTuesdayChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvTuesdayChoose);
                tvValue.setText(time);
                restaurant_timing_from_tue = getTimeWithM(hr1,min1);
                restaurant_timing_to_tue = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
            case R.id.tvWedChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvWedChoose);
                tvValue.setText(time);
                restaurant_timing_from_wed = getTimeWithM(hr1,min1);
                restaurant_timing_to_wed = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
            case R.id.tvThusChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvThusChoose);
                tvValue.setText(time);
                restaurant_timing_from_thu = getTimeWithM(hr1,min1);
                restaurant_timing_to_thu = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
            case R.id.tvFriChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvFriChoose);
                tvValue.setText(time);
                restaurant_timing_from_fri = getTimeWithM(hr1,min1);
                restaurant_timing_to_fri = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;

            }
            case R.id.tvSatChoose: {
                TextView tvValue = (TextView) findViewById(R.id.tvSatChoose);
                tvValue.setText(time);
                restaurant_timing_from_sat = getTimeWithM(hr1,min1);
                restaurant_timing_to_sat = getTimeWithM(hr2,min2);
               /* chooseButton.setText("Change Timing");*/
                break;
            }
        }
    }

    public void chooseTime(View view) {
        view.requestFocusFromTouch();
        initializeTimePicker();
        int viewID = view.getId();
        clickedTimeChoose = viewID;
    }



    public void btnAmenAction(View view) {
        view.requestFocusFromTouch();
        int viewID = view.getId();
        if(viewID == R.id.btnAddAmen1) {
            addAmenRow();
        } else {
            view.setVisibility(View.GONE);
            deleteAmenRow(viewID);
        }
    }
    public void btnCuisinAction(View view) {
        view.requestFocusFromTouch();
        int viewID = view.getId();
        if(viewID == R.id.btnAddCuisin1) {
            addCuisinRow();
        } else {
            view.setVisibility(View.GONE);
            deleteCuisinRow(viewID);
        }
    }
    public void btnInfoAction(View view) {
        view.requestFocusFromTouch();
        int viewID = view.getId();
        if(viewID == R.id.btnAddInfo1) {
            addInfoRow();
        } else {
            view.setVisibility(View.GONE);
            deleteInfoRow(viewID);
        }
    }



    private void addAmenRow(){
        if(btnAddAmen2.getVisibility() == View.GONE) {
            btnAddAmen2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gAmen2.setVisibility(View.VISIBLE);
        } else if(btnAddAmen3.getVisibility() == View.GONE) {
            btnAddAmen3.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gAmen3.setVisibility(View.VISIBLE);
        } else if(btnAddAmen4.getVisibility() == View.GONE) {
            btnAddAmen4.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gAmen4.setVisibility(View.VISIBLE);
        } else if(btnAddAmen5.getVisibility() == View.GONE){
            btnAddAmen5.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gAmen5.setVisibility(View.VISIBLE);
        }
    }
    private void addCuisinRow(){
        if(btnAddCuisin2.getVisibility() == View.GONE) {
            btnAddCuisin2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gCuisin2.setVisibility(View.VISIBLE);
        } else if(btnAddCuisin3.getVisibility() == View.GONE) {
            btnAddCuisin3.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gCuisin3.setVisibility(View.VISIBLE);
        } else if(btnAddCuisin4.getVisibility() == View.GONE) {
            btnAddCuisin4.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gCuisin4.setVisibility(View.VISIBLE);
        } else if(btnAddCuisin5.getVisibility() == View.GONE){
            btnAddCuisin5.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gCuisin5.setVisibility(View.VISIBLE);
        }
    }
    private void addInfoRow(){
        if(btnAddInfo2.getVisibility() == View.GONE) {
            btnAddInfo2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gInfo2.setVisibility(View.VISIBLE);
        } else if(btnAddInfo3.getVisibility() == View.GONE) {
            btnAddInfo3.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gInfo3.setVisibility(View.VISIBLE);
        } else if(btnAddInfo4.getVisibility() == View.GONE) {
            btnAddInfo4.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gInfo4.setVisibility(View.VISIBLE);
        } else if(btnAddInfo5.getVisibility() == View.GONE){
            btnAddInfo5.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gInfo5.setVisibility(View.VISIBLE);
        }
    }




    private void deleteAmenRow(int viewID){
        switch(viewID) {
            case R.id.btnAddAmen2:{
                etAmen2.setText(null);
                gAmen2.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddAmen3:{
                etAmen3.setText(null);
                gAmen3.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddAmen4:{
                etAmen4.setText(null);
                gAmen4.setVisibility(View.GONE);
                break;
            }

            case R.id.btnAddAmen5:{
                etAmen5.setText(null);
                gAmen5.setVisibility(View.GONE);
                break;
            }
        }
    }
    private void deleteCuisinRow(int viewID){
        switch(viewID) {
            case R.id.btnAddCuisin2:{
                etCuisin2.setText(null);
                gCuisin2.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddCuisin3:{
                etCuisin3.setText(null);
                gCuisin3.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddCuisin4:{
                etCuisin4.setText(null);
                gCuisin4.setVisibility(View.GONE);
                break;
            }

            case R.id.btnAddCuisin5:{
                etCuisin5.setText(null);
                gCuisin5.setVisibility(View.GONE);
                break;
            }
        }
    }
    private void deleteInfoRow(int viewID){
        switch(viewID) {
            case R.id.btnAddInfo2:{
                etInfo2.setText(null);
                gInfo2.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddInfo3:{
                etInfo3.setText(null);
                gInfo3.setVisibility(View.GONE);
                break;
            }
            case R.id.btnAddInfo4:{
                etInfo4.setText(null);
                gInfo4.setVisibility(View.GONE);
                break;
            }

            case R.id.btnAddInfo5:{
                etInfo5.setText(null);
                gInfo5.setVisibility(View.GONE);
                break;
            }
        }
    }



    public void chooseImage(View view) {
        int viewID = view.getId();
        switch(viewID) {
            case R.id.btnAddBlogImage:{
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_LOGO);
            }
            case R.id.btnAddRestobanner:{
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_BANNER_IMAGE);
            }
        }
    }

    private void initializeTimePicker() {

        RangeTimePickerDialog dialog = new RangeTimePickerDialog();
        dialog.newInstance();
        dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
        dialog.setIs24HourView(false); // Indicates if the format should be 24 hours
        dialog.setColorBackgroundHeader(R.color.colorPrimary); // Set Color of Background header dialog
        dialog.setColorTextButton(R.color.colorPrimaryDark); // Set Text color of button
        FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, "");



    }


    private void loadSpinnerData() {
        String[] optionArray = {"Facebook","Instagram","Google Ads","Restaurant","Friends"};
        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                CreateRestoActivity.this, android.R.layout.simple_spinner_dropdown_item, optionArray);

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinHearAboutUs.setAdapter(spinnerArrayAdapter);
    }


    private String getTimeWithM(int hr, int min){
        String AM_PM = " AM";
        String PREF_MIN ="";
        if (hr >= 12) {
            AM_PM = " PM";
            if (hr >=13 && hr < 24) {
                hr -= 12;
            }
            else {
                hr = 12;
            }
        } else if (hr == 0) {
            hr = 12;
        }

        if (min < 10) {
            PREF_MIN = "0";
        }
        return hr+":"+PREF_MIN+min+AM_PM;
    }

    private void removeImage(View view) {
    }

    public void getAllRestoCat() {
        String url = MConstant.API_END+"/getAllRestoCat";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objParamData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objParamData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray catArray = response.getJSONArray("data");
                            ActiveAndroid.beginTransaction();
                            for(int i=0;i<catArray.length();i++){
                                JSONObject obj = catArray.getJSONObject(i);
                                catList.add(obj);
                                /*RestoCat restoCat = RestoCat.getItem(obj.getString("id"));
                                if(restoCat == null) { restoCat = new RestoCat();}
                                restoCat.cat_id = obj.getString("id");
                                restoCat.name = obj.getString("name");
                                restoCat.summary = obj.optString ("summary");
                                restoCat.status = obj.getString("status");
                                restoCat.created_at = obj.getString("created_at");
                                restoCat.save();*/
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}