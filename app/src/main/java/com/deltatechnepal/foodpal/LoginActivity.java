package com.deltatechnepal.foodpal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.deltatechnepal.utility.SecurePreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    final Context mContext = this;
    ProgressDialog mProd;
    ImageView ivLogo;
    EditText etPassword,etEmail;
    CheckBox cbShowPassword;
    Button btnLogin,btnSkip,btnRegister;
    ImageButton btnFacebook,btnGoogle;
    LoginManager mFbLoginManager;
    public CallbackManager mCallbackManager;
    TextView tvForgotPass;

    private FirebaseAuth mAuth;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /* Generationg Hash For Facebook*/
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.deltatechnepal.foodpal",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Processing Login");
        //mProd.setCancelable(false);

        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnFacebook = (ImageButton) findViewById(R.id.btnFacebook);
        //btnGoogle = (ImageButton) findViewById(R.id.btnGoogle);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnSkip = (Button) findViewById(R.id.btnSkip);


        /*Setting Spannable Text For New To FoodPal?*/
       /* TextView tvNewToFoodpal = (TextView) findViewById(R.id.tvNewToFoodpal);
        String tempString = "New To FoodPal?";
        SpannableString tempSS = new SpannableString(tempString);

        int pIndex = tempString.indexOf("P");
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary));
        tempSS.setSpan(foregroundColorSpan,pIndex,pIndex+3,Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvNewToFoodpal.setText(tempSS);*/

        /*Animation for logo image*/
        /*Animation logoAnim = new AlphaAnimation(1, 0.5f);
        logoAnim.setDuration(500);
        logoAnim.setInterpolator(new LinearInterpolator());
        logoAnim.setRepeatCount(Animation.INFINITE);
        logoAnim.setRepeatMode(Animation.REVERSE);
        ivLogo.startAnimation(logoAnim);*/
        /*End Animation for logo image*/

        /********/
        // Facebook Login CallbackManager
        mFbLoginManager = com.facebook.login.LoginManager.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        mFbLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String token = accessToken.getToken();
                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                            sendRequestForFBLogin(user,token);
                    }
                });

                Bundle bundle=new Bundle();
                bundle.putString("fields", "id,first_name,last_name,link,gender,birthday,email,picture.width(150).height(150)");
                request.setParameters(bundle);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        });
        /*******/

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("103806924350-qu20e03sujg5qvl20pcbvakbmoi99duc.apps.googleusercontent.com") //found as Web Client ID https://console.firebase.google.com/project/foodpal-28709/authentication/providers
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        /*Event For Checkbox Click*/

        /*cbShowPassword = (CheckBox) findViewById(R.id.cbShowPassword);
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });*/

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFbLoginManager.logOut();
                mFbLoginManager.logInWithReadPermissions(LoginActivity.this,Arrays.asList("public_profile","user_friends","email","user_birthday"));
            }
        });



        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, RegisterActivity.class));
                finish();
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(mContext, MainActivity.class));
            }
        });

        tvForgotPass = (TextView) findViewById(R.id.tvForgotPass);


        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,ForgetPasswordActivity.class));
            }
        });


      /*  tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!MFunction.isInternetAvailable(getApplicationContext())){
                    Toast.makeText(mContext, "Please Try When Internet Is Available", Toast.LENGTH_SHORT).show();
                    return;
                }
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(mContext);
                View mView = layoutInflaterAndroid.inflate(R.layout.dialog_box_forgotpass, null);
                android.support.v7.app.AlertDialog.Builder otpDialogBuilder = new android.support.v7.app.AlertDialog.Builder(mContext);
                otpDialogBuilder.setView(mView);

                final EditText etEmail = (EditText) mView.findViewById(R.id.etEmail);
                otpDialogBuilder
                        .setCancelable(false)
                        .setTitle("FoodPal")
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {

                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });
                final android.support.v7.app.AlertDialog otpDialog = otpDialogBuilder.create();
                otpDialog.show();
                otpDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        Boolean wantToCloseDialog = false;
                        String mEmail = etEmail.getText().toString();
                        if(!MFunction.isValidEmail(mEmail)) {
                            etEmail.setError("Invalid Email");
                        } else {
                            //send authentication request to server
                            wantToCloseDialog = true;
                            startForgotPassword(mEmail);
                        }
                        if(wantToCloseDialog) {
                            otpDialog.dismiss();
                        }
                    }
                });
            }
        });*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if(account != null){
                    sendRequestForGoogleLogin(account);
                }
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        }

    }

    private void attemptLogin() {
        // Reset errors.
        etEmail.setError(null);
        etPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Email is required.");
            focusView = etEmail;
            cancel = true;
        } else if (!MFunction.isValidEmail(email)) {
            etEmail.setError("Email is not valid.");
            focusView = etEmail;
            cancel = true;
        }


        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            etPassword.setError("This field is required!");
            focusView = etPassword;
            cancel = true;
        } else if(!isPasswordValid(password)) {
            etPassword.setError("Password is invalid");
            focusView = etPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            if(!MFunction.isInternetAvailable(mContext)){
                Toast.makeText(mContext, "Please try when internet is available.", Toast.LENGTH_SHORT).show();
                return;
            }
            //send request for login
            sendRequestForLogin(email,password);
            mProd.show();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2 && password.length() <20;
    }

    private void sendRequestForLogin(String email, String password) {
        mProd.show();
        String url = MConstant.API_END+"/memberLogin";
        Map<String, String> params = new HashMap<String, String>();
        params.put("email",email);
        params.put("password",password);
        params.put("status","1");
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);

        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            mProd.dismiss();
                            if(status) {
                                JSONObject obj = response.getJSONObject("data");
                                Map<String,String> loginData = new HashMap<>();
                                loginData.put("is_login","true");
                                loginData.put("user_id",obj.getString("id"));
                                loginData.put("first_name",obj.getString("first_name"));
                                loginData.put("last_name",obj.getString("last_name"));
                                loginData.put("phone",obj.getString("phone"));
                                loginData.put("address",obj.getString("address"));
                                loginData.put("email",obj.getString("email"));
                                loginData.put("gender",obj.getString("gender"));
                                loginData.put("dob",obj.getString("dob"));
                                loginData.put("anniversary",obj.getString("anniversary"));
                                loginData.put("about",obj.getString("about"));
                                loginData.put("avatar",obj.getString("avatar"));
                                loginData.put("oauth_provider",obj.getString("oauth_provider"));
                                loginData.put("oauth_id",obj.getString("oauth_id"));
                                loginData.put("oauth_token",obj.getString("oauth_token"));
                                loginData.put("reviewed_rest",obj.getString("reviewed_rest"));
                                loginData.put("status",obj.getString("status"));
                                loginData.put("blogger",obj.getString("blogger"));
                                loginData.put("point",obj.getString("point"));
                                loginData.put("followers",response.getString("followers"));
                                loginData.put("rs",obj.getString("rs"));
                                String imagePath = MConstant.SERVER+"/uploads/avatars/members/100x100"+ obj.getString("avatar");
                                loginData.put("photoUrl",imagePath);
                                savePreferenceData(loginData);
                                Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
                                Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                                LoginActivity.this.startActivity(myIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Authentication Error!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error",error.toString());
                        Toast.makeText(mContext,"Network Error!", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void sendRequestForFBLogin(JSONObject user, String accessToken) {
        mProd.show();
        String url = MConstant.API_END+"/facebookLogin";
        String authID = null;
        String email = null;
        String firstName = null;
        String lastName = null;
        String imagePath = null;
        try {
            authID = user.getString("id");
            email = user.getString("email");
            firstName = user.getString("first_name");
            lastName = user.getString("last_name");
            JSONObject im = user.getJSONObject("picture").getJSONObject("data");
            imagePath = im.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putString("email",email);
        bundle.putString("first_name",firstName);
        bundle.putString("last_name",lastName);
        bundle.putString("imagePath",imagePath);
        final Bundle loginBundle = bundle;
        Map<String, String> params = new HashMap<String, String>();
        params.put("auth_id",authID);//authID
        params.put("email",email);
        params.put("first_name",firstName);
        params.put("last_name",lastName);
        params.put("token",accessToken);
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        Log.i("response",response.toString());
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            Map<String,String> loginData = new HashMap<>();
                            loginData.put("is_login","true");
                            loginData.put("user_id",data.get("id").getAsString());
                            loginData.put("first_name",data.get("first_name").getAsString());
                            loginData.put("last_name",data.get("last_name").getAsString());
                            loginData.put("phone",data.get("phone").getAsString());
                            loginData.put("address",data.get("address").getAsString());
                            loginData.put("email",data.get("email").getAsString());
                            loginData.put("about",data.get("about").getAsString());
                            loginData.put("avatar",data.get("avatar").getAsString());
                            loginData.put("reviewed_rest",data.get("reviewed_rest").getAsString());
                            loginData.put("status",data.get("status").getAsString());
                            loginData.put("blogger",data.get("blogger").toString());
                            loginData.put("point",data.get("point").getAsString());
                            loginData.put("followers",data.get("followerCount").getAsString());
                            loginData.put("photoUrl",loginBundle.getString("imagePath"));
                            savePreferenceData(loginData);
                            Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                            finish();
                            LoginActivity.this.startActivity(myIntent);
                            Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.e("MError",error.toString());
                        Toast.makeText(mContext,getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void sendRequestForGoogleLogin(GoogleSignInAccount account) {
        mProd.show();
        String url = MConstant.API_END+"/googleLogin";
        Map<String, String> params = new HashMap<String, String>();

        params.put("auth_id",account.getId());//oauth_id
        params.put("email",account.getEmail());
        params.put("first_name",account.getGivenName());
        params.put("last_name",account.getFamilyName());
        params.put("token",account.getIdToken());
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        final String imagePath = account.getPhotoUrl().toString();
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            Map<String,String> loginData = new HashMap<>();
                            loginData.put("is_login","true");
                            loginData.put("user_id",data.get("id").getAsString());
                            loginData.put("first_name",data.get("first_name").getAsString());
                            loginData.put("last_name",data.get("last_name").getAsString());
                            loginData.put("phone",data.get("phone").getAsString());
                            loginData.put("address",data.get("address").getAsString());
                            loginData.put("email",data.get("email").getAsString());
                            loginData.put("about",data.get("about").getAsString());
                            loginData.put("avatar",data.get("avatar").getAsString());
                            loginData.put("reviewed_rest",data.get("reviewed_rest").getAsString());
                            loginData.put("status",data.get("status").getAsString());
                            loginData.put("blogger",data.get("blogger").toString());
                            loginData.put("point",data.get("point").getAsString());
                            loginData.put("followers",data.get("followerCount").getAsString());
                            loginData.put("photoUrl",imagePath);
                            savePreferenceData(loginData);
                            Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
                            Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                            LoginActivity.this.startActivity(myIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.e("MError",error.toString());
                        Toast.makeText(mContext,getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void savePreferenceData(Map staffData){
        SecurePreferences preferences = new SecurePreferences(this, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        Iterator<Map.Entry<String, String>> it = staffData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            preferences.put(pair.getKey(),pair.getValue());
        }
    }
    public void startForgotPassword(String email){
        mProd.show();
        String url = MConstant.API_END+"/forgotPassword";
        Map<String, String> params = new HashMap<String, String>();
        params.put("email",email);
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                mProd.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error",error.toString());
                        Toast.makeText(mContext,getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);


    }
}