package com.deltatechnepal.foodpal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.deltatechnepal.adapter.GridviewAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.utility.MConstant;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CategoriesActivity extends AppCompatActivity {
    GridView androidGridView;

    String[] gridViewString ;
    int[] gridViewImageId = {
            R.mipmap.breakfast, R.mipmap.brunch, R.mipmap.cafe, R.mipmap.casual_dining, R.mipmap.clubbing, R.mipmap.delivery,
            R.mipmap.desserts,R.mipmap.dinner, R.mipmap.fine_dining, R.mipmap.liquor, R.mipmap.lunch, R.mipmap.quick_bites,
            R.mipmap.sweets, R.mipmap.takeaway,

    };
    JSONArray jsonArray;
    Context mContext;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        mContext=this;

        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView tvTitle =toolbar.findViewById(R.id.toolbar_title);
        tvTitle.setText("Categories");


        androidGridView=findViewById(R.id.grid_view_image_text);


        fetchCategories();
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {


                TextView cat= view.findViewById(R.id.android_gridview_text);
                for (int j = 0; j < jsonArray.length(); j++) {

                    try {
                        JSONObject category = jsonArray.getJSONObject(j);
                        int cat_id = category.getInt("id");
                        String cat_name=category.getString("name");
                        if(cat.getText().toString().matches(cat_name)){

                            Intent cIntent = new Intent(getApplicationContext(), MainActivity.class);
                            cIntent.putExtra("search_category", true);

                            cIntent.putExtra("category", String.valueOf(cat_id));

                            startActivity(cIntent);
                        }



                    }
                    catch (JSONException e){

                    }




                }





            }
        });


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_option, menu);
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_notifications: {

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }






    public void fetchCategories(){
        // Volley's json array request object

        String url = MConstant.API_END+"/getAllRestoCat";
        final StringRequest req = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // looping through json and adding to movies list

                        try {

                            JSONObject obj=new JSONObject(response);

                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            if(obj.getBoolean("status")) {
                                 jsonArray= obj.getJSONArray("data");
                                 gridViewString=new String[jsonArray.length()];
                                for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject category = jsonArray.getJSONObject(j);
                                        int cat_id = category.getInt("id");
                                        String cat_name=category.getString("name");
                                        gridViewString[j]=cat_name;




                                    }



                            }
                            else{


                            }


                            GridviewAdapter adapterViewAndroid = new GridviewAdapter(mContext,true, gridViewString, gridViewImageId);

                            androidGridView.setAdapter(adapterViewAndroid);
                            // updating offset value to highest value
                                       /* if (rank >= offSet)
                                            offSet = rank;*/

                        } catch (JSONException e) {

                        }




                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),error.getMessage() , Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };


        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }


}