package com.deltatechnepal.foodpal;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deltatechnepal.adapter.ProfileBookmarkAdapter;
import com.deltatechnepal.ormmodel.Resto;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.List;

public class ProfileBookmarkFragment extends Fragment {
    private JsonArray bookmarkData;
    private Context mContext;

    private OnFragmentInteractionListener mListener;

    private RecyclerView rvBookmark;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public List<Resto> mListResto;
    public FloatingActionButton btnAddBlog;

    public ProfileBookmarkFragment() {

    }
    public static ProfileBookmarkFragment newInstance(String param1, String param2) {
        ProfileBookmarkFragment fragment = new ProfileBookmarkFragment();
        Bundle args = new Bundle();
        args.putString("bookmarkData", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String tempData = getArguments().getString("bookmarkData");
            JsonParser jsonParser = new JsonParser();
            bookmarkData = (JsonArray) jsonParser.parse(tempData);
        }

        /*mListResto = Resto.getAllRestoByUserID(MFunction.getMyPrefVal("user_id",getActivity()));*/
        //mListResto = Resto.getAllRestoByUserID("1");
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_bookmarks, container, false);
        setRvBookmark(bookmarkData,view);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setRvBookmark(JsonArray tempArray, View view){
        rvBookmark = (RecyclerView) view.findViewById(R.id.rvBookmark);
        rvBookmark.setHasFixedSize(true);
        rvBookmark.setNestedScrollingEnabled(true);
        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        rvBookmark.setLayoutManager(mLayoutManager);

        ProfileBookmarkAdapter profileBookmarkAdapter = new ProfileBookmarkAdapter(mContext,tempArray);
        rvBookmark.swapAdapter(profileBookmarkAdapter,false);
    }
}
