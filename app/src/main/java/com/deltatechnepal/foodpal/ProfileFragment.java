package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.CommonPopularBlogAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.deltatechnepal.app.AppController.TAG;

public class ProfileFragment extends Fragment {
    private Context mContext;
    private JsonObject profileData;
    private JsonArray blogData,popularBlogs,recentBlogs;
    int PICK_PROFILE = 100;
    TextView tvChangeProfileImage;
    RadioGroup rgYesNo;
    RadioButton rbYes,rbNo;
    Button btnSave;
    String avatar,blogger;
    ProgressDialog mProd;
    ImageView ivProfile;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }



    public static ProfileFragment newInstance(String profileData, String blogData) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString("profileData", profileData);
        args.putString("blogData", blogData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        if (getArguments() != null) {
            String tempProfile = getArguments().getString("profileData");
            String tempBlogs = getArguments().getString("blogData");
            JsonParser jsonParser = new JsonParser();
            profileData = (JsonObject) jsonParser.parse(tempProfile);
            blogData = (JsonArray) jsonParser.parse(tempBlogs);

            //TODO: Need To Filter Data
            popularBlogs = blogData;
            recentBlogs = blogData;

            System.out.println("BLOGDATA "+blogData);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_PROFILE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            View view = getView();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            avatar = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            ivProfile = (ImageView) getActivity().findViewById(R.id.ivUserImage);
            ivProfile.setImageURI(uri);
            /*UCrop.of(uri,uri)
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(400,400 )
                    .start(getActivity());*/
        }

        /*if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            final Uri resultUri = UCrop.getOutput(data);
            ImageView ivProfile = (ImageView)findViewById(R.id.ivProfile);
            ivProfile.setImageURI(resultUri);
        } else if(resultCode == UCrop.RESULT_ERROR){
            Log.i(TAG, "onActivityResult:"+UCrop.getError(data).toString());
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_others, container, false);

        /*Setting About*/
        TextView tvAboutDesc = (TextView) view.findViewById(R.id.tvAboutDesc);
        tvAboutDesc.setText(profileData.get("about").getAsString());

        ImageButton ibFB = (ImageButton)view.findViewById(R.id.btnFacebook);
        ibFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tempUrl = profileData.get("facebookUrl").getAsString();
                if(TextUtils.isEmpty(tempUrl)) {
                    Toast.makeText(mContext, "Url Not Available", Toast.LENGTH_SHORT).show();
                } else{
                    Intent intent = new Intent(mContext,WebViewActivity.class);
                    intent.putExtra("url",tempUrl);
                    mContext.startActivity(intent);
                }
            }
        });

        ImageButton ibInsta = (ImageButton)view.findViewById(R.id.btnInsta);
        ibInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tempUrl = profileData.get("instagramUrl").getAsString();
                if(TextUtils.isEmpty(tempUrl)) {
                    Toast.makeText(mContext, "Url Not Available", Toast.LENGTH_SHORT).show();
                } else{
                    Intent intent = new Intent(mContext,WebViewActivity.class);
                    intent.putExtra("url",tempUrl);
                    mContext.startActivity(intent);
                }
            }
        });
        setBlogs(popularBlogs,view,R.id.rvPopularBlogs);
        setBlogs(recentBlogs,view,R.id.rvRecentBlogs);

      /*  tvChangeProfileImage = (TextView) view.findViewById(R.id.tvChangeProfileImage);
        rgYesNo = (RadioGroup) view.findViewById(R.id.rgYesNo);
        rbYes = (RadioButton) view.findViewById(R.id.rbYes);
        rbNo = (RadioButton) view.findViewById(R.id.rbNo);

        mProd = new ProgressDialog(getActivity());
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Changing Profile");

        if(TextUtils.equals(MFunction.getMyPrefVal("blogger",getActivity()),"1")) {
            rbYes.setChecked(true);
        } else {
            rbNo.setChecked(true);
        }

        rgYesNo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

            }
        });

        btnSave = (Button)view.findViewById(R.id.btnSave);

        tvChangeProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, PICK_PROFILE);
                } catch (Exception e){
                    Log.i("Exception",e.toString());

                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!MFunction.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), "No Internet", Toast.LENGTH_SHORT).show();
                    return;
                }
                int selectedID = rgYesNo.getCheckedRadioButtonId();
                if(selectedID == R.id.rbYes) {
                    blogger = "1";
                }else {
                    blogger = "0";
                }
                sendRequestForProfileChange(avatar,blogger);
            }
        });*/
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setBlogs(JsonArray tempArray, View view,int id){
        RecyclerView rvBlogs = (RecyclerView) view.findViewById(id);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvBlogs.setHasFixedSize(true);
        rvBlogs.setNestedScrollingEnabled(true);
        rvBlogs.setLayoutManager(popularLM);
        CommonPopularBlogAdapter commonPopularBlogAdapter = new CommonPopularBlogAdapter(mContext,tempArray);
        rvBlogs.swapAdapter(commonPopularBlogAdapter,false);
    }

    public void sendRequestForProfileChange(String avatar,String isBlogger){
        mProd.show();
        final String fAvatar = avatar;
        final String fBlogger = isBlogger;
        String url = MConstant.API_END+"/changeProfile";
        Map<String, String> params = new HashMap<String, String>();
        params.put("avatar",fAvatar);
        params.put("isBlogger",fBlogger);
        params.put("user_id",MFunction.getMyPrefVal("user_id",getActivity()));
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                Map<String,String> loginData = new HashMap<>();
                                loginData.put("avatar",fAvatar);
                                loginData.put("blogger",fBlogger);
                                MFunction.savePreferenceData(loginData, getActivity());
                                mProd.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
