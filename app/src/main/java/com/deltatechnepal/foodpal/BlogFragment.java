package com.deltatechnepal.foodpal;

import android.animation.Animator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aromajoin.actionsheet.ActionSheet;
import com.aromajoin.actionsheet.OnActionListener;
import com.deltatechnepal.adapter.BlogMainAdapter;
import com.deltatechnepal.adapter.CommonPopularBlogAdapter;
import com.deltatechnepal.adapter.CommonPopularBlogSeemoreAdapter;
import com.deltatechnepal.adapter.NearbyBlogAdapter;
import com.deltatechnepal.adapter.ProfileBlogAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.transitionseverywhere.TransitionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlogFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private Context mContext;
    private RecyclerView rvBlog;
    private RecyclerView.Adapter blogMainAdapter;
    public List<Blog> blogList = new ArrayList<Blog>();
     public List<Blog> searchblogList = new ArrayList<Blog>();
    android.widget.EditText searchText;
     RecyclerView matchingBlogs;
      LinearLayout defaultHolder,innerHolder;
      Button blogButton;
      TextView searchres, tvpopularblogseemore, tvrecentblogseemore;
      ImageButton filter,search;
    ViewGroup transitionsContainer;
    ImageButton gototop;
    Group mygroup;
    RecyclerView rvmydisplayview;
    ScrollView scrollView;

    JsonArray popBlogs,recentBlogs;


    public BlogFragment() {
    }

    public static BlogFragment newInstance(String param1, String param2) {
        BlogFragment fragment = new BlogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mContext = getActivity();
        ((MainActivity) getActivity()).setActionBarTitle("Search Blogs");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog, container, false);


        filter=view.findViewById(R.id.search_filter);
        filter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showFilterDialog(v);

            }
        });


        search=view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // do something
             searchTask();

            }
        });
        scrollView = view.findViewById(R.id.fcontainer);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        MainActivity mainActivity =(MainActivity) getActivity();
        gototop = mainActivity.findViewById(R.id.gotoTop);
        gototop.setVisibility(View.GONE);



        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0,0);
            }
        });
      /*  tvpopularblogseemore = view.findViewById(R.id.tvPopularBlogsseemore);
        tvrecentblogseemore = view.findViewById(R.id.tvRecentBlogsseemore);
        mygroup = view.findViewById(R.id.mygroup);
        rvmydisplayview = view.findViewById(R.id.mydisplayview);*/


       /* tvrecentblogseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mygroup.setVisibility(View.GONE);
                rvmydisplayview.setVisibility(View.VISIBLE);


                LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL, false);
                rvmydisplayview.setHasFixedSize(true);
                rvmydisplayview.setNestedScrollingEnabled(true);
                rvmydisplayview.setLayoutManager(popularLM);
                CommonPopularBlogSeemoreAdapter commonPopularBlogAdapter = new CommonPopularBlogSeemoreAdapter(mContext,recentBlogs);
                rvmydisplayview.swapAdapter(commonPopularBlogAdapter,false);


            }
        });*/

     /*   tvpopularblogseemore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mygroup.setVisibility(View.GONE);
                rvmydisplayview.setVisibility(View.VISIBLE);


                LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL, false);
                rvmydisplayview.setHasFixedSize(true);
                rvmydisplayview.setNestedScrollingEnabled(true);
                rvmydisplayview.setLayoutManager(popularLM);
                CommonPopularBlogSeemoreAdapter commonPopularBlogAdapter = new CommonPopularBlogSeemoreAdapter(mContext,popBlogs);
                rvmydisplayview.swapAdapter(commonPopularBlogAdapter,false);


            }
        });*/

        blogButton=view.findViewById(R.id.blog_button);
        blogButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // do something

                String buttonText = blogButton.getText().toString();
                if(buttonText.matches("Write a Blog")){
                    String isLogin = MFunction.getMyPrefVal("is_login",mContext);
                    if(TextUtils.equals(isLogin,"true")){
                        startActivity(new Intent(mContext,CreateBlogActivity.class));
                    } else{
                        startActivity(new Intent(mContext,LoginActivity.class));
                    }
                }
                else{
                    blogButton.setText("Write a Blog");
                    searchText.setText(null);
//                    TransitionManager.beginDelayedTransition(transitionsContainer);
                    matchingBlogs.setVisibility(View.GONE);
                    searchres.setVisibility(View.GONE);
                    innerHolder.setVisibility(View.VISIBLE);
                }
            }
        });

        searchres=view.findViewById(R.id.tvSearchResults);
//
//        defaultHolder=view.findViewById(R.id.default_holder);
       innerHolder=view.findViewById(R.id.inner_holder);
//        transitionsContainer = (ViewGroup) view.findViewById(R.id.default_holder);
        matchingBlogs = view.findViewById(R.id.rvBlog);

        searchText=view.findViewById(R.id.etSearchText);
        searchText.setOnEditorActionListener(new android.widget.EditText.OnEditorActionListener() {
             @Override
             public boolean onEditorAction(android.widget.TextView v, int actionId, android.view.KeyEvent event) {
                 if (actionId == android.view.inputmethod.EditorInfo.IME_ACTION_DONE) {

                     searchTask();
                     return true;
                 }
                 return false;
             }
        });

        /*Fetching&Setting popular/recent blogs*/
        fetchData(view);
        return view;
    }

    private void showFilterDialog(View view) {

        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());



        // String array for alert dialog multi choice items
        String[] filters = new String[]{
                "Popular",
                "Recent",
                "Location",
                "Author",
                "Liked"
        };

        // Boolean array for initial selected items
        final boolean[] checkedFields = new boolean[]{
                false, // Red
                true, // Green
                false, // Blue
                true, // Purple
                false // Olive

        };

        // Convert the color array to list
        final List<String> filterList = Arrays.asList(filters);
        builder.setMultiChoiceItems(filters, checkedFields, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedFields[which] = isChecked;

                // Get the current focused item
                String currentItem = filterList.get(which);

                // Notify the current action
                Toast.makeText(getActivity(),
                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Filter by :");

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                for (int i = 0; i<checkedFields.length; i++){
                    boolean checked = checkedFields[i];

                }
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    public void searchTask(){
        searchres.setText("Search Results for : "+searchText.getText().toString());
        blogButton.setText("Back");
        //TransitionManager.beginDelayedTransition(transitionsContainer);
        innerHolder.setVisibility(View.GONE);
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

        //TransitionManager.beginDelayedTransition(transitionsContainer);
        searchres.setVisibility(View.VISIBLE);
        matchingBlogs.setVisibility(View.VISIBLE);
        /*searchblogList = Blog.getMatchingBlog(searchText.getText().toString());
        matchingBlogs.setHasFixedSize(true);
        matchingBlogs.setNestedScrollingEnabled(true);
        matchingBlogs.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        blogMainAdapter = new BlogMainAdapter(mContext,searchblogList);
        matchingBlogs.setAdapter(blogMainAdapter);*/
        fetchAndSetSearchedData();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setPopularBlogs(JsonArray tempArray, View view){
        RecyclerView  rvPopularBlogs = (RecyclerView) view.findViewById(R.id.rvPopularBlogs);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvPopularBlogs.setHasFixedSize(true);
        rvPopularBlogs.setNestedScrollingEnabled(true);
        rvPopularBlogs.setLayoutManager(popularLM);
        CommonPopularBlogAdapter commonPopularBlogAdapter = new CommonPopularBlogAdapter(mContext,tempArray);
        rvPopularBlogs.swapAdapter(commonPopularBlogAdapter,false);
    }
    private void setRecentBlogs(JsonArray tempArray, View view){
        RecyclerView  rvRecentBlogs = (RecyclerView) view.findViewById(R.id.rvRecentBlogs);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rvRecentBlogs.setHasFixedSize(true);
        rvRecentBlogs.setNestedScrollingEnabled(true);
        rvRecentBlogs.setLayoutManager(popularLM);
        CommonPopularBlogAdapter commonPopularBlogAdapter = new CommonPopularBlogAdapter(mContext,tempArray);
        rvRecentBlogs.swapAdapter(commonPopularBlogAdapter,false);
    }

    private void setSearchData(JsonArray tempArray){
        matchingBlogs.setHasFixedSize(true);
        matchingBlogs.setNestedScrollingEnabled(true);
        matchingBlogs.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        blogMainAdapter = new BlogMainAdapter(mContext,tempArray);
        matchingBlogs.setAdapter(blogMainAdapter);
    }

    private void fetchAndSetSearchedData() {
        ((MainActivity) getActivity()).openDialog();
        String url = MConstant.API_END+"/searchBlog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("search_key",searchText.getText().toString());
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ((MainActivity) getActivity()).closeDialog();
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonArray tempArray = (JsonArray) jsonParser.parse(response.getString("data"));
                            setSearchData(tempArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((MainActivity) getActivity()).closeDialog();
                        Log.e("VolleyError",error.toString());
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void fetchData(View view) {
        ((MainActivity) getActivity()).openDialog();
        final View  mView = view;
        String url = MConstant.API_END+"/getBlogPageData";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MainActivity activity =(MainActivity) getActivity();
                        if(activity!=null) activity.closeDialog();
                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            popBlogs = data.getAsJsonArray("popular_blogs");
                             recentBlogs = data.getAsJsonArray("recent_blogs");
                            setPopularBlogs(popBlogs,mView);
                            setRecentBlogs(recentBlogs,mView);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainActivity activity =(MainActivity) getActivity();
                        if(activity!=null) activity.closeDialog();
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
