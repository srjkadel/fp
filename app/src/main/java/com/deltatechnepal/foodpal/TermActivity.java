package com.deltatechnepal.foodpal;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Policy;
import com.deltatechnepal.ormmodel.Term;
import com.deltatechnepal.utility.MConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TermActivity extends AppCompatActivity {
    private TextView tvContent;
    private String content;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        content = Term.getContent();
        if(TextUtils.isEmpty(content)) {
            fetchContent();
        } else{
            WebView webView = (WebView) findViewById(R.id.webView);
            webView.loadData(content,"text/html","utf-8");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_page_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh: {
                fetchContent();
                Toast.makeText(mContext, "Refreshed", Toast.LENGTH_SHORT).show();
                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void fetchContent(){
        String url = MConstant.API_END+"/getTermContent";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject paramData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, paramData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = response.getJSONObject("data");
                            String content = obj.getString("content");
                            Term term = Term.getTerm();
                            if(term == null) { term = new Term();}
                            term.term_id = "1";
                            term.content = content;
                            term.save();
                            WebView webView = (WebView) findViewById(R.id.webView);
                            webView.loadData(content,"text/html","utf-8");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        WebView webView = (WebView) findViewById(R.id.webView);
                        webView.loadData(content,"text/html","utf-8");
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
