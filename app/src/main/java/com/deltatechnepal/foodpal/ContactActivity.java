package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContactActivity extends AppCompatActivity {
    private TextView etName,etEmail,etPhone,etMessage;
    private Spinner spSubject;
    private Button btnSubmit;
    private Context mContext;
    private String name,email,phone,subject,message;
    private ProgressDialog mProd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Processing..");

        etName = (TextView) findViewById(R.id.etName);
        etEmail = (TextView) findViewById(R.id.etEmail);
        etPhone = (TextView) findViewById(R.id.etPhone);
        etMessage = (TextView) findViewById(R.id.etMessage);
        spSubject = (Spinner) findViewById(R.id.spSubject);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.message_subject_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSubject.setAdapter(adapter);

        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //catID = catList.get(position).cat_id;
                Log.i("Position",""+position);
                Log.i("id",""+id);
                subject = parent.getItemAtPosition(position).toString();
                Log.i("Subject",subject);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etName.getText().toString();
                email = etEmail.getText().toString();
                phone = etPhone.getText().toString();
                message = etMessage.getText().toString();
                if(TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(message) || TextUtils.isEmpty(subject)) {
                    Toast.makeText(mContext, "Please Fill All Required Fields", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!MFunction.isValidEmail(email)){
                    etEmail.setError("Invalid Email");
                    etEmail.requestFocus();
                    return;
                }

                if(!MFunction.isValidPhone(phone)){
                    etPhone.setError("Invalid Phone");
                    etPhone.requestFocus();
                    return;
                }
                if(!MFunction.isInternetAvailable(mContext)) {
                    Snackbar.make(view,"Internet Is Not Available", Snackbar.LENGTH_LONG).show();
                    return;
                }
                sendRequestForContact(name,email,phone,subject,message);
            }
        });
    }

    private void sendRequestForContact(String name,String email, String phone,String subject, String message) {
        mProd.show();
        String url = MConstant.API_END+"/contact";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("name",name);
        params.put("email",email);
        params.put("phone",phone);
        params.put("subject",subject);
        params.put("message",message);
        JSONObject paramData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, paramData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String msg = response.getString("message");
                            mProd.dismiss();
                            if(status) {
                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error",error.toString());
                        Toast.makeText(mContext,"Network Error!", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

}


