package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.model.MySingleton;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.deltatechnepal.utility.SecurePreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    final Context mContext = this;
    ProgressDialog mProd;
    ImageView ivLogo;
    TextView tvTermsAndCondition;
    EditText etFirstName, etLastName, etEmail, etPhone, etPassword, etConfirmPassword;
    CheckBox cbTermsAndCondition;
    Button btnSkip, btnRegister;
    ImageButton btnFacebook, btnback;
    LoginManager mFbLoginManager;
    private FirebaseAuth mAuth;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    public CallbackManager mCallbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private boolean checked = false;
    private JsonObjectRequest jsonObjectRequest;
    private JSONObject joData = new JSONObject();
    private RequestQueue rQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        changeStatusBarColor();// making status bar transparent



        cbTermsAndCondition = findViewById(R.id.cbAgreeTermAndCondition);

        /*Transparent Action Bar*/
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().hide();







        /*Setting Title of Activity*/
        this.setTitle("");

        /* Generationg Hash For Facebook*/
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.deltatechnepal.foodpal",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Processing Login");
        //mProd.setCancelable(false);

        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);

        btnRegister = (Button) findViewById(R.id.btnLogin);
        btnFacebook = (ImageButton) findViewById(R.id.btnFacebook);
      //  btnGoogle = (ImageButton) findViewById(R.id.btnGoogle);
        btnSkip = (Button) findViewById(R.id.btnSkip);
        tvTermsAndCondition = (TextView) findViewById(R.id.tvTermsAndCondition);

        btnback = findViewById(R.id.ibback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*Animation for logo image*/
       /* Animation logoAnim = new AlphaAnimation(1, 0.5f);
        logoAnim.setDuration(500);
        logoAnim.setInterpolator(new LinearInterpolator());
        logoAnim.setRepeatCount(Animation.INFINITE);
        logoAnim.setRepeatMode(Animation.REVERSE);
        ivLogo.startAnimation(logoAnim);*/
        /*End Animation for logo image*/

        /********/
        // Facebook Login CallbackManager
        mFbLoginManager = com.facebook.login.LoginManager.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        mFbLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String token = accessToken.getToken();
                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                        sendRequestForFBLogin(user, token);
                    }
                });

                Bundle bundle = new Bundle();
                bundle.putString("fields", "id,first_name,last_name,link,gender,birthday,email,picture.width(150).height(150)");
                request.setParameters(bundle);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        });
        /*******/

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("103806924350-qu20e03sujg5qvl20pcbvakbmoi99duc.apps.googleusercontent.com") //found as Web Client ID https://console.firebase.google.com/project/foodpal-28709/authentication/providers
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFbLoginManager.logOut();
                mFbLoginManager.logInWithReadPermissions(RegisterActivity.this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
            }
        });



        tvTermsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mContext,WebViewActivity.class);
                intent.putExtra("title","Terms And Condition");
                intent.putExtra("url",MConstant.SERVER+"/term-conditions");
                finish();
                startActivity(intent);*/
                finish();
                startActivity(new Intent(mContext, TermActivity.class));
            }
        });

        /*btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(getBaseContext(), MainActivity.class));
            }
        });*/

        cbTermsAndCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checked = true;
            }
        });
    }
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_register_option, menu);

        // changing color of back menu
        Drawable drawable = menu.findItem(R.id.action_back).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this, R.color.color2));
        menu.findItem(R.id.action_back).setIcon(drawable);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_back: {
                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
                return true;
            }

            default: {
                finish();
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);


        Bundle bundle = data.getExtras();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d(TAG, String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName()));
            }
        }



            System.out.println("GMAIL SIGN IN 0");
            if (requestCode == RC_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    if (account != null) {
                        sendRequestForGoogleLogin(account);
                    }
                } catch (ApiException e) {
                    Log.w(TAG, "Google sign in failed", e);
                }
            }


    }

/*    public void checkClick(View view) {
        checked = ((CheckBox) view).isChecked();
        if (checked) {
            MFunction.hideKeyboard(this);
        }
    }*/

    private void attemptRegister() {
        // Reset errors.
        etFirstName.setError(null);
        etLastName.setError(null);
        etEmail.setError(null);
        etPhone.setError(null);
        etPassword.setError(null);
        etConfirmPassword.setError(null);

        // Store values at the time of the register attempt.
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setError("First Name is required.");
            focusView = etFirstName;
            cancel = true;
        } else if (TextUtils.isEmpty(lastName)) {
            etLastName.setError("Last Name is required.");
            focusView = etLastName;
            cancel = true;
        } else if (TextUtils.isEmpty(email)) {
            etEmail.setError("Email is required.");
            focusView = etEmail;
            cancel = true;
        } else if (!MFunction.isValidEmail(email)) {
            etEmail.setError("Email is not valid.");
            focusView = etEmail;
            cancel = true;
        } else if (!checked) {
            Toast.makeText(mContext, "Terms and condition must be checked", Toast.LENGTH_SHORT).show();
            cancel = true;
        } else if (!cancel) {
            String url = MConstant.API_END + "/memberRegister";
            try {
                joData.put("first_name", firstName);
                joData.put("last_name", lastName);
                joData.put("email", email);
                joData.put("phone", phone);
                joData.put("password", password);
                joData.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password)) {
                etPassword.setError("This field is required!");
                focusView = etPassword;
                cancel = true;
            } else if (!isPasswordValid(password)) {
                etPassword.setError("Password is invalid");
                focusView = etPassword;
                cancel = true;
            }

            if (!TextUtils.equals(password, confirmPassword)) {
                etConfirmPassword.setError("Did not match with password");
                focusView = etConfirmPassword;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();

            } else {
                if (!MFunction.isInternetAvailable(mContext)) {
                    Toast.makeText(mContext, "Please try when internet is available.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    VolleyStandard(url);
                    mProd.show();
                }
                //send request for login
                //  sendRequestForRegister(firstName, lastName, email, phone, password);

            }


        }

    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2 && password.length() < 20;
    }

    private void sendRequestForRegister(String firstName, String lastName, String email, String phone, String password) {
        mProd.show();
        String url = MConstant.API_END + "/memberRegister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("email", email);
        params.put("phone", phone);
        params.put("password", password);
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("SIGNUP RESPONSE: " + response);

                            boolean status = response.getBoolean("status");
                            mProd.dismiss();
                            if (status) {
                                JSONObject obj = response.getJSONObject("data");
                                Map<String, String> loginData = new HashMap<>();
                                loginData.put("is_login", "true");
                                loginData.put("user_id", obj.getString("id"));
                                loginData.put("first_name", obj.getString("first_name"));
                                loginData.put("last_name", obj.getString("last_name"));
                                loginData.put("phone", obj.getString("phone"));
                                //loginData.put("address",obj.getString("address"));
                                loginData.put("email", obj.getString("email"));
                                //loginData.put("gender",obj.getString("gender"));
                                //loginData.put("dob",obj.getString("dob"));
                                //loginData.put("anniversary",obj.getString("anniversary"));
                                //loginData.put("about",obj.getString("about"));
                                loginData.put("avatar", obj.getString("avatar"));
                                //loginData.put("oauth_provider",obj.getString("oauth_provider"));
                                //loginData.put("oauth_id",obj.getString("oauth_id"));
                                //loginData.put("oauth_token",obj.getString("oauth_token"));
                                //loginData.put("reviewed_rest",obj.getString("reviewed_rest"));
                                loginData.put("status", obj.getString("status"));
                                //loginData.put("blogger",obj.getString("blogger"));
                                //loginData.put("point",obj.getString("point"));
                                //loginData.put("followers",response.getString("followers"));
                                //loginData.put("rs",obj.getString("rs"));
                                loginData.put("created_at", obj.getString("created_at"));
                                savePreferenceData(loginData);
                                Toast.makeText(mContext, "Register Success!", Toast.LENGTH_SHORT).show();
                                Intent myIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                RegisterActivity.this.startActivity(myIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error", error.toString());
                        Toast.makeText(mContext, "Network Error!", Toast.LENGTH_SHORT).show();
                        System.out.println("SIGNUP ERROR " + error.getMessage());

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void sendRequestForFBLogin(JSONObject user, String accessToken) {
        mProd.show();
        String url = MConstant.API_END + "/facebookLogin";
        String authID = null;
        String email = null;
        String firstName = null;
        String lastName = null;
        String imagePath = null;
        try {
            authID = user.getString("id");
            email = user.getString("email");
            firstName = user.getString("first_name");
            lastName = user.getString("last_name");
            JSONObject im = user.getJSONObject("picture").getJSONObject("data");
            imagePath = im.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        bundle.putString("first_name", firstName);
        bundle.putString("last_name", lastName);
        bundle.putString("imagePath", imagePath);
        final Bundle loginBundle = bundle;
        Map<String, String> params = new HashMap<String, String>();
        params.put("auth_id", authID);//authID
        params.put("email", email);
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("token", accessToken);
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        Log.i("response", response.toString());
                        try {
                            if (!response.getBoolean("status")) return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            Map<String, String> loginData = new HashMap<>();
                            loginData.put("is_login", "true");
                            loginData.put("user_id", data.get("id").getAsString());
                            loginData.put("first_name", data.get("first_name").getAsString());
                            loginData.put("last_name", data.get("last_name").getAsString());
                            loginData.put("phone", data.get("phone").getAsString());
                            loginData.put("address", data.get("address").getAsString());
                            loginData.put("email", data.get("email").getAsString());
                            loginData.put("about", data.get("about").getAsString());
                            loginData.put("avatar", data.get("avatar").getAsString());
                            loginData.put("reviewed_rest", data.get("reviewed_rest").getAsString());
                            loginData.put("status", data.get("status").getAsString());
                            loginData.put("blogger", data.get("blogger").toString());
                            loginData.put("point", data.get("point").getAsString());
                            loginData.put("followers", data.get("followerCount").getAsString());
                            loginData.put("photoUrl", loginBundle.getString("imagePath"));
                            savePreferenceData(loginData);
                            Intent myIntent = new Intent(RegisterActivity.this, MainActivity.class);
                            finish();
                            RegisterActivity.this.startActivity(myIntent);
                            Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.e("MError", error.toString());
                        Toast.makeText(mContext, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void sendRequestForGoogleLogin(GoogleSignInAccount account) {
        mProd.show();
        String url = MConstant.API_END + "/googleLogin";
        Map<String, String> params = new HashMap<String, String>();

        params.put("auth_id", account.getId());//oauth_id
        params.put("email", account.getEmail());
        params.put("first_name", account.getGivenName());
        params.put("last_name", account.getFamilyName());
        params.put("token", account.getIdToken());
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        final String imagePath = account.getPhotoUrl().toString();
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        try {
                            System.out.println("GMAIL SIGNUP RESPONSE " + response);
                            if (!response.getBoolean("status")) return;
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = (JsonObject) jsonParser.parse(response.getString("data"));
                            Map<String, String> loginData = new HashMap<>();
                            loginData.put("is_login", "true");
                            loginData.put("user_id", data.get("id").getAsString());
                            loginData.put("first_name", data.get("first_name").getAsString());
                            loginData.put("last_name", data.get("last_name").getAsString());
                            loginData.put("phone", data.get("phone").getAsString());
                            loginData.put("address", data.get("address").getAsString());
                            loginData.put("email", data.get("email").getAsString());
                            loginData.put("about", data.get("about").getAsString());
                            loginData.put("avatar", data.get("avatar").getAsString());
                            loginData.put("reviewed_rest", data.get("reviewed_rest").getAsString());
                            loginData.put("status", data.get("status").getAsString());
                            loginData.put("blogger", data.get("blogger").toString());
                            loginData.put("point", data.get("point").getAsString());
                            loginData.put("followers", data.get("followerCount").getAsString());
                            loginData.put("photoUrl", imagePath);
                            savePreferenceData(loginData);
                            Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
                            Intent myIntent = new Intent(RegisterActivity.this, MainActivity.class);
                            RegisterActivity.this.startActivity(myIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.e("MError", error.toString());
                        Toast.makeText(mContext, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void savePreferenceData(Map tempData) {
        SecurePreferences preferences = new SecurePreferences(this, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        Iterator<Map.Entry<String, String>> it = tempData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            preferences.put(pair.getKey(), pair.getValue());
        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void VolleyStandard(String url) {

        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, joData, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    //TODO
                    /*{"status":true,"message":"Successfully registered.",
                    "data":{"first_name":"Pramod","last_name":"Karn","blogger":0,"email":"pramod.deltatech@gmail.com","password":"25d55ad283aa400af464c76d713c07ad","status":1,
                    "created_at":"2019-06-28 12:38:37",
                    "avatar":"default.png","phone":"9802040402","id":64}}*/
                    System.out.println("DATA IS 0 " + response);
                    mProd.dismiss();
                    if (response.getBoolean("status")) {

                        System.out.println("DATA IS 0 1 " + response);

                        Toast.makeText(mContext, response.getString("message"), Toast.LENGTH_SHORT).show();

                        JSONObject obj = response.getJSONObject("data");

                        Map<String, String> loginData = new HashMap<>();
                        loginData.put("is_login", "true");
                        loginData.put("user_id", obj.getString("id"));
                        loginData.put("first_name", obj.getString("first_name"));
                        loginData.put("last_name", obj.getString("last_name"));
                        loginData.put("phone", obj.getString("phone"));
                        loginData.put("email", obj.getString("email"));

                        String imagePath = MConstant.SERVER + "/uploads/avatars/members/100x100" + obj.getString("avatar");
                        loginData.put("photoUrl", imagePath);

                        savePreferenceData(loginData);


                        PassUserInfoToMainActivity(response.toString());


                    } else {
                        //email aready registered
                        Toast.makeText(mContext, "Email already registered.", Toast.LENGTH_SHORT).show();

                        etEmail.setError("Email already registered.");
                        etEmail.setSelection(0, etEmail.getText().toString().length());

                    }


                } catch (Exception e) {
                    e.printStackTrace();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProd.dismiss();
            }
        }) {
            /**
             * Returns a list of extra HTTP headers to go along with this request. Can throw {@link
             * AuthFailureError} as authentication may be required to provide these values.
             *
             * @throws AuthFailureError In the event of auth failure
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                mProd.dismiss();

                return params;

            }
        };
        jsonObjectRequest.setTag("");

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user_info","user information");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
    }

    private void PassUserInfoToMainActivity(String responseJSON) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user_info", responseJSON);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        finish();
        return super.onContextItemSelected(item);
    }





}