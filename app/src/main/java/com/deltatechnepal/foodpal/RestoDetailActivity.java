package com.deltatechnepal.foodpal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.RestoInfoAdapter;
import com.deltatechnepal.adapter.RestoPhotoGalleryAdapter;
import com.deltatechnepal.adapter.RestoReviewAdapter;
import com.deltatechnepal.adapter.RestoTimingAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.circularindicator.CircularIndicator;
import com.deltatechnepal.model.RestoTimingModel;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.example.easywaylocation.EasyWayLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.blogc.android.views.ExpandableTextView;

public class RestoDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int RESTRO_REVIEW_CODE = 4004;
    private Context mContext;
    private String restoID, tvResTodayTimingVal, tvOpenText;
    private JsonObject resto, averageRating;
    private ImageView ivBannerImage;
    private RecyclerView rvTiming;
    private TextView tvName, tvCuisine, tvCategory, tvDistance, tvSeeAll, tvRating, tvRecommendation, tvDescription, tvAverageCost,
            tvResTodayTiming, tvAverageCostVal, tvNumbers, tvWebVal;
    private ExpandableTextView tvDescriptionContent;
    private ImageButton ibFB, ibInsta;
    private Double distance;
    private com.deltatechnepal.circularindicator.CircularIndicator rbResto;
    private String uniqueID;
    private int totalRecommendation;
    ImageButton gototop,share,bookmark;
    ScrollView scrollView;
    Button overview, menu, gallery, reviews;
    RecyclerView rvRestoGallery;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        mContext = this;

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
        }
        setContentView(R.layout.activity_resto_detail);
        JsonParser jsonParser = new JsonParser();
        resto = (JsonObject) jsonParser.parse(getIntent().getStringExtra("resto"));
        restoID = resto.get("id").getAsString();
        uniqueID = MFunction.getMyPrefVal("uniqueID", mContext);
        if (resto.get("total_recommendation") != null)
            totalRecommendation = resto.get("total_recommendation").getAsInt();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        // making notification bar transparent
        changeStatusBarColor();
        /*Transparent Action Bar*/
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getSupportActionBar().setElevation(0);

        /*Setting Title of Activity*/
        this.setTitle("");
        /*initializing ImageView*/
        ivBannerImage = (ImageView) findViewById(R.id.ivBannerImage);

        /*initializing TextView*/
        tvName = (TextView) findViewById(R.id.tvName);
        tvCuisine = (TextView) findViewById(R.id.tvCuisineIcon);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvDistance = (TextView) findViewById(R.id.tvDistance);
        //tvRating = (TextView) findViewById(R.id.tvRating);
        tvRecommendation = (TextView) findViewById(R.id.tvRecommendation);
        tvDescriptionContent = (ExpandableTextView) findViewById(R.id.tvDescriptionContent);
        tvAverageCostVal = (TextView) findViewById(R.id.tvAverageCostVal);
        tvNumbers = (TextView) findViewById(R.id.tvNumbers);
        tvWebVal = (TextView) findViewById(R.id.tvWebVal);
        tvResTodayTiming = (TextView) findViewById(R.id.tvResTodayTiming);
        scrollView = (ScrollView) findViewById(R.id.svRestoDetail);
        gototop = (ImageButton) findViewById(R.id.gotoTop);

        overview = (Button) findViewById(R.id.btnOverview);
        gallery = (Button) findViewById(R.id.btnGallery);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        gototop.setVisibility(View.GONE);





        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0,0);
            }
        });



        share = findViewById(R.id.ibActionShare);
        bookmark = findViewById(R.id.action_bookmark);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,MConstant.SERVER+"/restaurant-details/"+resto.get("slug").getAsString());
                startActivity(Intent.createChooser(sharingIntent, "Share"));

            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(resto.get("id").getAsString(),"2",mContext);

            }
        });

        overview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0,tvDescriptionContent.getTop());

                    }
                });
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0,rvRestoGallery.getTop());
                    }
                });
            }
        });


        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0,0);
            }
        });

        /*Initializing ImageButton*/
        ibFB = (ImageButton) findViewById(R.id.ibFB);
        ibInsta = (ImageButton) findViewById(R.id.ibInsta);


        try {
            averageRating = resto.getAsJsonObject("average_rating");
        } catch (Exception e) {
            averageRating = new JsonObject();

        }
        JsonArray reviewArray = (JsonArray) jsonParser.parse(resto.get("reviews").getAsString());
        JsonArray infoArray = (JsonArray) jsonParser.parse(resto.get("additional_info").getAsString());
        setReviews(reviewArray);


        List<JsonObject> mustTryList = new ArrayList<JsonObject>();
        List<JsonObject> menuList = new ArrayList<JsonObject>();
        List<JsonObject> highLightList = new ArrayList<JsonObject>();
        List<JsonObject> galList = new ArrayList<JsonObject>();
        JsonArray gallery = (JsonArray) jsonParser.parse(resto.get("photogallery").getAsString());
        for (int i = 0; i < gallery.size(); i++) {
            JsonElement jsonElement = gallery.get(i);
            JsonObject jsonObj = jsonElement.getAsJsonObject();
            String isHighlight = jsonObj.get("isHighlight").toString();
            String isMustTry = jsonObj.get("isMustTry").toString();
            String isMenu = jsonObj.get("isMenu").toString();
            if (TextUtils.equals(isHighlight, "1")) {
                highLightList.add(jsonObj);
            } else if (TextUtils.equals(isMustTry, "1")) {
                mustTryList.add(jsonObj);
            } else if (TextUtils.equals(isMenu, "1")) {
                menuList.add(jsonObj);
            } else {
                galList.add(jsonObj);
            }
        }

        rvTiming = (RecyclerView) findViewById(R.id.rvRestoTiming);
        rvTiming.setVisibility(View.GONE);

        tvSeeAll = (TextView) findViewById(R.id.tvSeeAll);
        tvSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rvTiming.getVisibility() == View.VISIBLE) {
                    rvTiming.setVisibility(View.GONE);
                    tvSeeAll.setText("See All");
                } else {
                    rvTiming.setVisibility(View.VISIBLE);
                    tvSeeAll.setText("Hide All");
                }
            }
        });

        RestoTimingAdapter restoTimingAdapter = null;
        try {
            restoTimingAdapter = new RestoTimingAdapter(prepareRestoTimingData());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            setUI();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTiming.setLayoutManager(mLayoutManager);
        rvTiming.setItemAnimator(new DefaultItemAnimator());
        rvTiming.setAdapter(restoTimingAdapter);


        TextView tvAdditionalInfo = findViewById(R.id.tvAdditionalInfo);
        GridView gvAdditionalInfo = findViewById(R.id.gvAdditionalInfo);
        RestoInfoAdapter restoInfoAdapter = new RestoInfoAdapter(mContext, infoArray);
        gvAdditionalInfo.setAdapter(restoInfoAdapter);
        if (infoArray.size() < 1) {
            tvAdditionalInfo.setVisibility(View.GONE);
            gvAdditionalInfo.setVisibility(View.GONE);
        }

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragmentResto);
        mapFragment.getMapAsync(this);

        /*Disableing Parent Scrolling when map is scrolled using two fingers*/
        final ScrollView svRestoDetail = (ScrollView) findViewById(R.id.svRestoDetail);
        ImageView ivMapTransparent = (ImageView) findViewById(R.id.ivMapTransparent);
        ivMapTransparent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        svRestoDetail.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        svRestoDetail.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        svRestoDetail.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        /*Click Events*/
        tvWebVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("url", resto.get("website").getAsString());
                mContext.startActivity(intent);
            }
        });

        tvNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + resto.get("phone").getAsString())));
            }
        });

        ibFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("url", resto.get("facebookUrl").getAsString());
                mContext.startActivity(intent);
            }
        });

        ibInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("url", resto.get("instagramUrl").getAsString());
                mContext.startActivity(intent);
            }
        });

        /* Setting Must Try Section*/
        TextView tvRestoMustTry = findViewById(R.id.tvRestoMustTry);
        RecyclerView rvRestoMustTry = (RecyclerView) findViewById(R.id.rvRestoMustTry);
        if (mustTryList.size() < 1) {
            tvRestoMustTry.setVisibility(View.GONE);
            rvRestoMustTry.setVisibility(View.GONE);
        }
        rvRestoMustTry.setHasFixedSize(true);
        RestoPhotoGalleryAdapter restoDetailMustTryAdapter = new RestoPhotoGalleryAdapter(mContext, mustTryList);
        rvRestoMustTry.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvRestoMustTry.setAdapter(restoDetailMustTryAdapter);


        /* Setting Highlight Section*/
        TextView tvRestoHightlight = findViewById(R.id.tvRestoHightlight);
        RecyclerView rvRestoHighlight = (RecyclerView) findViewById(R.id.rvRestoHighlight);
        if (highLightList.size() < 1) {
            tvRestoHightlight.setVisibility(View.GONE);
            rvRestoHighlight.setVisibility(View.GONE);
        }
        rvRestoHighlight.setHasFixedSize(true);
        RestoPhotoGalleryAdapter restoDetailHighlightAdapter = new RestoPhotoGalleryAdapter(mContext, highLightList);
        rvRestoHighlight.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvRestoHighlight.setAdapter(restoDetailHighlightAdapter);


        /* Setting Menu Section*/
        TextView tvRestoMenu = findViewById(R.id.tvRestoMenu);
        RecyclerView rvRestoMenu = (RecyclerView) findViewById(R.id.rvRestoMenu);
        if (menuList.size() < 1) {
            tvRestoMenu.setVisibility(View.GONE);
            rvRestoMenu.setVisibility(View.GONE);
        }
        rvRestoMenu.setHasFixedSize(true);
        RestoPhotoGalleryAdapter restoDetailMenuAdapter = new RestoPhotoGalleryAdapter(mContext, menuList);
        rvRestoMenu.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvRestoMenu.setAdapter(restoDetailMenuAdapter);

        /* Setting Gallery Section*/
        TextView tvRestoGallery = findViewById(R.id.tvRestoGallery);
         rvRestoGallery = (RecyclerView) findViewById(R.id.rvRestoGallery);
        if (galList.size() < 1) {
            tvRestoGallery.setVisibility(View.GONE);
            rvRestoGallery.setVisibility(View.GONE);
        }
        rvRestoGallery.setHasFixedSize(true);
        RestoPhotoGalleryAdapter restoDetailGallerydapter = new RestoPhotoGalleryAdapter(mContext, galList);
        rvRestoGallery.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvRestoGallery.setAdapter(restoDetailGallerydapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_restodetail_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_share: {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, MConstant.SERVER + "/restaurant-details/" + resto.get("slug").getAsString());
                startActivity(Intent.createChooser(sharingIntent, "Share"));
                return true;
            }
            case R.id.action_bookmark: {
                if (!TextUtils.equals(MFunction.getMyPrefVal("is_login", mContext), "true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(restoID, "2", mContext);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLang = new LatLng(Double.parseDouble(resto.get("lat").getAsString()), Double.parseDouble(resto.get("log").getAsString()));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

            }
        });
        googleMap.setMyLocationEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLang, 13));
        googleMap.addMarker(new MarkerOptions()
                .title(resto.get("name").getAsString())
                .snippet(resto.get("formatted_address").getAsString())
                .position(latLang));
    }


    public void postReviewClick(View view) {
        if (!TextUtils.equals(MFunction.getMyPrefVal("is_login", mContext), "true")) {
            Toast.makeText(mContext, "Please Login First", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent restroIntent = new Intent(this, PostReviewActivity.class);
        restroIntent.putExtra("resto_id", restoID);

        startActivityForResult(restroIntent, RESTRO_REVIEW_CODE);


    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setUI() throws JSONException {
        String imageUrl = MConstant.SERVER + "/uploads/restaurantbanner/1350x500" + resto.get("banner_image").getAsString();
        Picasso.with(mContext)
                .load(imageUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.logo))
                .error(mContext.getResources().getDrawable(R.drawable.logo))
                .into(ivBannerImage);
        tvName.setText(resto.get("name").getAsString());
        Typeface cabin = Typeface.createFromAsset(getAssets(), "fonts/cabin_regular.ttf");
        tvCuisine.setText(resto.get("cuisines").getAsString());
        tvCuisine.setTypeface(cabin);
        tvCuisine.setSelected(true);
        tvCategory.setText(resto.get("cat_name").getAsString());
        tvRecommendation.setText("" + totalRecommendation);
        distance = getDistance();
        tvDistance.setText(distance.toString() + " KM");
        rbResto = (CircularIndicator) findViewById(R.id.rbResto);
        //rbResto.setShouldUseDelimiter(true);
        rbResto.setProgressTextDelimiter(",");
        float mFloat = 0;
        int mInt = 0;
        try {
            mFloat = Float.parseFloat(resto.get("r_rating").getAsString());
            mInt = (int) Math.round(mFloat);
        } catch (NumberFormatException nfe) {
            Log.e("error", nfe.toString());
        }
        rbResto.setProgress(avarageRatingPercentage(resto.get("r_rating").getAsString()), 100);
        rbResto.setProgressStrokeWidthDp(7);
        rbResto.setTextSizeSp(2);
        rbResto.setProgressText(resto.get("r_rating").getAsString());

        if (!averageRating.entrySet().isEmpty()) {
            CircularIndicator rbFood = (CircularIndicator) findViewById(R.id.rbFood);
            rbFood.setProgress(getCurrentProgress(averageRating.get("avg_food").getAsString()), 5);
            rbFood.setProgressStrokeWidthDp(6);
            rbFood.setProgressText(getFormattedProgressText(getCurrentProgress(averageRating.get("avg_food").getAsString())));

            CircularIndicator rbService = (CircularIndicator) findViewById(R.id.rbService);
            rbService.setProgress(getCurrentProgress(averageRating.get("avg_service").getAsString()), 5);
            rbService.setProgressStrokeWidthDp(6);
            rbService.setProgressText(getFormattedProgressText(getCurrentProgress(averageRating.get("avg_service").getAsString())));

            CircularIndicator rbAmbience = (CircularIndicator) findViewById(R.id.rbAmbience);
            rbAmbience.setProgress(getCurrentProgress(averageRating.get("avg_ambience").getAsString()), 5);
            rbAmbience.setProgressStrokeWidthDp(6);
            rbAmbience.setProgressText(getFormattedProgressText(getCurrentProgress(averageRating.get("avg_ambience").getAsString())));

            CircularIndicator rbCleanliness = (CircularIndicator) findViewById(R.id.rbCleanliness);
            rbCleanliness.setProgress(getCurrentProgress(averageRating.get("avg_cleanliness").getAsString()), 5);
            rbCleanliness.setProgressStrokeWidthDp(6);
            rbCleanliness.setProgressText(getFormattedProgressText(getCurrentProgress(averageRating.get("avg_cleanliness").getAsString())));
        }


        //tvRating.setText(resto.r_rating);
        //tvRecommendation.setText(resto.get("recomended").getAsString());

        Button btnRecommend = (Button) findViewById(R.id.btnRecommend);
        btnRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new android.support.v7.app.AlertDialog.Builder(mContext)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Are you sure to Recommend?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                recommend();
                            }
                        })
                        .setNegativeButton("No", null).show();
            }
        });

        tvDescriptionContent.setText(resto.get("about").getAsString());
        final Button buttonToggle = (Button) this.findViewById(R.id.btnToggleDescriptionContent);
        // set interpolators for both expanding and collapsing animations
        tvDescriptionContent.setInterpolator(new OvershootInterpolator());
        // or set them separately
        tvDescriptionContent.setExpandInterpolator(new OvershootInterpolator());
        tvDescriptionContent.setCollapseInterpolator(new OvershootInterpolator());
        // toggle the ExpandableTextView
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                buttonToggle.setText(tvDescriptionContent.isExpanded() ? "Read More" : "Show Less");
                tvDescriptionContent.toggle();
            }
        });

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(getResources().getColor(R.color.suga));
        //gd.setStroke(2, Color.GREEN); // border width and color
        gd.setCornerRadius(16f); // border corner radius
        TextView tvOpen = (TextView) findViewById(R.id.tvOpen);
        tvOpen.setBackground(gd);

        TextView tvNoise = (TextView) findViewById(R.id.tvNoiseLowHigh);
        GradientDrawable gdNoise = new GradientDrawable();
        gdNoise.setShape(GradientDrawable.RECTANGLE);
        gdNoise.setColor(getResources().getColor(R.color.suga));
        gdNoise.setCornerRadius(16f);
        tvNoise.setBackground(gdNoise);

        tvResTodayTiming.setText(tvResTodayTimingVal);
        tvAverageCostVal.setText("Rs :" + resto.get("average_cost").getAsString());
        tvNumbers.setText(resto.get("phone").getAsString());
        tvWebVal.setText((TextUtils.isEmpty(resto.get("website").getAsString())) ? "N/A" : resto.get("website").getAsString());
    }

    private List<RestoTimingModel> prepareRestoTimingData() throws JSONException {
        List<RestoTimingModel> restoTimingList = new ArrayList<RestoTimingModel>();
        JSONArray restoTimingArray = new JSONArray(resto.get("timing").getAsString());
        if (restoTimingArray.length() > 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            Date date = new Date();
            String today = sdf.format(date).toLowerCase();
            for (int i = 0; i < restoTimingArray.length(); i++) {
                JSONObject obj = restoTimingArray.getJSONObject(i);
                RestoTimingModel rtm = new RestoTimingModel();
                if (TextUtils.equals(today, obj.getString("day"))) {
                    tvResTodayTimingVal = "Today: " + obj.getString("from") + "-" + obj.getString("to");

                    tvOpenText = "";
                }
                rtm.setDay(obj.getString("day"));
                rtm.setFrom(obj.getString("from"));
                rtm.setTo(obj.getString("to"));
                restoTimingList.add(rtm);
            }
        }
        return restoTimingList;
    }

    private void setReviews(JsonArray reviewArray) {
        RecyclerView rvRestoReview = (RecyclerView) findViewById(R.id.rvRestoReview);
        LinearLayoutManager reviewLM = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRestoReview.setHasFixedSize(true);
        rvRestoReview.setNestedScrollingEnabled(true);
        rvRestoReview.setLayoutManager(reviewLM);
        RestoReviewAdapter restoReviewAdapter = new RestoReviewAdapter(mContext, reviewArray);
        rvRestoReview.setAdapter(restoReviewAdapter);
    }

    private double getDistance() {
        String lati1 = resto.get("lat").getAsString();
        String longi1 = resto.get("log").getAsString();

        String lati2 = MFunction.getMyPrefVal("lati", mContext);
        String longi2 = MFunction.getMyPrefVal("longi", mContext);
        Double distance = EasyWayLocation.calculateDistance(Double.parseDouble(lati1), Double.parseDouble(longi1), Double.parseDouble(lati2), Double.parseDouble(longi2));
        Double inKM = distance / 1000;
        return Math.floor(inKM * 100) / 100;
    }

    private String getFormattedProgressText(int progress) {
        String progressText = "";
        switch (progress) {
            case 1: {
                progressText = "Below Average";
                break;
            }
            case 2: {
                progressText = "Average";
                break;
            }
            case 3: {
                progressText = "Good";
                break;
            }
            case 4: {
                progressText = "Very Good";
                break;
            }
            case 5: {
                progressText = "Excellent";
                break;
            }
        }
        return progressText;
    }

    private void recommend() {
        String url = MConstant.API_END + "/recommendRestaurant";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("restaurant_id", restoID);
        params.put("unique_id", uniqueID);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            int rCount = response.getInt("data");
                            if (status) {
                                String msg = response.getString("message");
                                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                                if (rCount == 1) {
                                    tvRecommendation.setText("" + totalRecommendation + 1);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private int getCurrentProgress(String rating) {
        int returnValue = 0;
        float mFloat = 0;
        try {
            mFloat = Float.parseFloat(resto.get("r_rating").getAsString());
        } catch (NumberFormatException nfe) {
            Log.e("error", nfe.toString());
        }

        returnValue = (int) Math.round(mFloat);
        return returnValue;
    }

    private int avarageRatingPercentage(String averageRating) {
        float rating = Float.parseFloat(averageRating);
        float percentage = (rating / 5) * 100;
        return Math.round(percentage);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESTRO_REVIEW_CODE && resultCode == Activity.RESULT_OK) {
            //data.getStringExtra(BLOG_COMMENT)
        }
    }
}