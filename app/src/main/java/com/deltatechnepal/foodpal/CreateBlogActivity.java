package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.nashapp.androidsummernote.Summernote;

public class CreateBlogActivity extends AppCompatActivity{
    @BindView(R.id.etBlogTitle) EditText etBlogTitle;
    @BindView(R.id.etBlogSubtitle) EditText etBlogSubtitle;
    @BindView(R.id.etBlogContent) Summernote etBlogContent;
    @BindView(R.id.etTag) EditText etTag;
    @BindView(R.id.etTimeToRead) EditText etTimeToRead;

    @BindView(R.id.ivHeaderImage) ImageView ivHeaderImage;
    @BindView(R.id.fabRemoveHeaderImage) FloatingActionButton fabRemoveHeaderImage;
    @BindView(R.id.gHeaderImageHolder) Group gHeaderImageHolder;
    @BindView(R.id.btnAddHeaderImage) Button btnAddHeaderImage;

    @BindView(R.id.ivBlogImage) ImageView ivBlogImage;
    @BindView(R.id.fabRemoveBlogImage) FloatingActionButton fabRemoveBlogImage;
    @BindView(R.id.gBlogImageHolder) Group gBlogImageHolder;
    @BindView(R.id.btnAddBlogImage) Button btnAddBlogImage;

    @BindView(R.id.btnPublishBlog) Button btnPublishBlog;


    int PICK_BLOG_IMAGE = 1;
    int PICK_HEADER_IMAGE = 2;
    ProgressDialog mProd;
    Context mContext;
    String ivHeaderImageVal,ivBlogImageVal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_blog);
        ButterKnife.bind(this);
        this.setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*Initializing Progress Dialog*/
        mProd = new ProgressDialog(mContext);
        mProd.setTitle(getString(R.string.app_name));
        mProd.setMessage("Saving");

        etBlogContent.setRequestCodeforFilepicker(5);

        /*Click Events*/

        btnAddHeaderImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_HEADER_IMAGE);
            }
        });

        btnAddBlogImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_BLOG_IMAGE);
            }
        });

        btnPublishBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!MFunction.isInternetAvailable(mContext)) {
                    Toast.makeText(mContext, "No Internet", Toast.LENGTH_SHORT).show();
                    return;
                }

                Bundle formData = new Bundle();
                formData.putString("title",etBlogTitle.getText().toString());
                formData.putString("summary",etBlogSubtitle.getText().toString());
                formData.putString("details",etBlogContent.getText());
                formData.putString("tags",etTag.getText().toString());
                formData.putString("readingtime",etTimeToRead.getText().toString());
                formData.putString("blog_image",ivBlogImageVal);
                formData.putString("header_image",ivHeaderImageVal);
                if(formValidation(formData)) {
                    createBlog(formData);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        etBlogContent.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_BLOG_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(),uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            ivBlogImageVal = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            ivBlogImage.setImageURI(uri);
            fabRemoveBlogImage.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gBlogImageHolder.setVisibility(View.VISIBLE);
        }

        if(requestCode == PICK_HEADER_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(),uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            ivHeaderImageVal = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            ivHeaderImage.setImageURI(uri);
            fabRemoveHeaderImage.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            gHeaderImageHolder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void createBlog(Bundle formData) {
        mProd.show();
        final Bundle bundle = formData;
        String url = MConstant.API_END+"/createblog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("author", MFunction.getMyPrefVal("user_id",mContext));
        params.put("title",bundle.getString("title"));
        params.put("summary",bundle.getString("summary"));
        params.put("details",bundle.getString("details"));
        params.put("tags",bundle.getString("tags"));
        params.put("readingtime",bundle.getString("readingtime"));
        params.put("header_image",bundle.getString("header_image"));
        params.put("blog_image",bundle.getString("blog_image"));
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProd.dismiss();
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                /*JSONObject obj = response.getJSONObject("data");
                                Blog blog = new Blog();
                                blog.blog_id = obj.getString("id");
                                blog.title = obj.getString("title");
                                blog.slug = obj.getString("slug");
                                blog.summary = obj.getString("summary");
                                blog.details = obj.getString("details");
                                blog.author = obj.getString("author");
                                blog.publish_date = obj.getString("publish_date");
                                blog.image = obj.getString("image");
                                blog.status = obj.getString("status");
                                blog.readingtime = obj.getString("readingtime");
                                blog.tags = obj.getString("tags");
                                blog.created_at = obj.getString("created_at");
                                blog.viewed = "0";
                                blog.creator_role = obj.getString("creator_role");
                                blog.save();*/
                                Toast.makeText(mContext, "Blog Created", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mProd.dismiss();
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProd.dismiss();
                        Log.i("Error",error.toString());
                        finish();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    private boolean formValidation(Bundle bundle){
        boolean result = true;
        boolean cancel = false;
        View focusView = null;
        /*Unset Errors*/
        etBlogTitle.setError(null);
        etBlogSubtitle.setError(null);

        if(TextUtils.isEmpty(bundle.getString("title"))){
            etBlogTitle.setError("This is required!");
            result = false;
            etBlogTitle.requestFocus();
        } else if(TextUtils.isEmpty(bundle.getString("summary"))) {
            etBlogSubtitle.setError("This is required!");
            result = false;
            etBlogSubtitle.requestFocus();
        } else if(TextUtils.isEmpty(bundle.getString("details"))){
            result = false;
            etBlogContent.requestFocus();
        } else if(TextUtils.isEmpty(bundle.getString("tags"))) {
            etTag.setError(getString(R.string.error_required_field));
            result = false;
            etTag.requestFocus();
        } else if(TextUtils.isEmpty(bundle.getString("readingtime"))){
            etTimeToRead.setError(getString(R.string.error_required_field));
            result = false;
            etTimeToRead.requestFocus();
        }
        return result;
    }

    public void removeImage(View view) {
        int tempID = view.getId();
        switch(tempID){
            case R.id.fabRemoveHeaderImage:{
                fabRemoveHeaderImage.setRippleColor(getResources().getColor(R.color.colorPrimaryDark));
                ivHeaderImageVal= null;
                ivHeaderImage.setImageDrawable(null);
                gHeaderImageHolder.setVisibility(View.GONE);
                btnAddHeaderImage.requestFocus();
                break;
            }
            case R.id.fabRemoveBlogImage:{
                fabRemoveBlogImage.setRippleColor(getResources().getColor(R.color.colorPrimaryDark));
                ivBlogImageVal= null;
                ivBlogImage.setImageDrawable(null);
                gBlogImageHolder.setVisibility(View.GONE);
                btnAddHeaderImage.requestFocus();
                break;
            }

        }
    }
}
