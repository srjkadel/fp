package com.deltatechnepal.foodpal;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.deltatechnepal.adapter.NotificationAdapter;
import com.deltatechnepal.ormmodel.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationList extends AppCompatActivity {
    private List<Notification> notifisList = new ArrayList<>();
    private  RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        mContext = this;
       // Toolbar toolbar =(Toolbar) findViewById(R.id.mytoolbar);
//        setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notification");

        preparenotificationdata();


        recyclerView =(RecyclerView) findViewById(R.id.notificationlist);
        notificationAdapter = new NotificationAdapter(mContext, notifisList);
        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationAdapter);


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void preparenotificationdata(){

        notifisList = Notification.getAllNotification(100);
        //notificationAdapter.notifyDataSetChanged();

    }





}
