package com.deltatechnepal.foodpal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.service.GeofenceTransitionsIntentService;
import com.deltatechnepal.utility.BottomNavigationViewHelper;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import android.Manifest;
import android.widget.VideoView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;

import static com.deltatechnepal.utility.MFunction.getUID;
import static com.example.easywaylocation.EasyWayLocation.LOCATION_SETTING_REQUEST_CODE;

public class MainActivity extends AppCompatActivity implements
        HomeFragment.OnFragmentInteractionListener,
        RestaurantFragment.OnFragmentInteractionListener,
        EventFragment.OnFragmentInteractionListener,
        BlogFragment.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener,
        Listener {

    public static String TAG = "MainActivity";

    Dialog dialog;
    private TextView mTextMessage;
    Toolbar toolbar;
    DrawerLayout drawer;
    EasyWayLocation easyWayLocation;
    private Double lati, longi;
    Context mContext;
    private GeofencingClient mGeofencingClient;
    List<Geofence> mGeofenceList;
    PendingIntent mGeofencePendingIntent;
    BottomNavigationView bottomNavigationView;
    private ImageView ivUser;
    private TextView tvUsername, tvOr;
    NavigationView navigationView;
    Menu gMenu;
    ActionBarDrawerToggle toggle;
    SearchView searchView;
    LinearLayout llSignUpToast;
    Button btnSignUp;
    ImageButton cafe, dinner, nightlife, delivery;
    ImageButton ibpic;
    List<Resto> allresto;
    List<Event> allevent;
    ImageButton gototop;
    ImageButton mymenu;


    boolean isCuisinesSearch = false, isCategorySearch = false, isApplyFilter = false;
    HomeFragment homeFragment;
    String[] cuisines = {
            "asian", "bakery", "burmese", "chinese", "continental", "european",
            "indian", "italian", "japanese", "korean", "mediterranean",
            "middle_eastern", "nepali", "newari", "oriental", "sea_food", "south_indian",
            "thai", "vietnamese"};

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_nearby:
                    selectedFragment = NearbyFragment.newInstance("", "");
                    break;
                case R.id.navigation_restaurant:
                    selectedFragment = RestaurantFragment.newInstance("", "");
                    break;
                case R.id.navigation_event:
                    selectedFragment = EventFragment.newInstance("", "");
                    break;
                case R.id.navigation_blog:
                    selectedFragment = BlogFragment.newInstance("", "");
                    break;
                default:
                    selectedFragment = HomeFragment.newInstance("", "");
                    unsetActionBarTitle();
                    break;
            }
            item.setChecked(true);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_main);
        mContext = this;

        // For showing sig up toast
        llSignUpToast = findViewById(R.id.llSignUpToast);
        btnSignUp = findViewById(R.id.btnSignUp);
        mymenu = findViewById(R.drawable.ic_menu);
        ibpic = findViewById(R.id.ibpic);


        String isLogin = MFunction.getMyPrefVal("is_login", mContext);
        boolean isLogined = TextUtils.equals(isLogin, "true") ? true : false;
        if (isLogined) {
            btnSignUp.setVisibility(View.GONE);
        }

        if (!isLogined) {
            llSignUpToast.setVisibility(View.VISIBLE);
        }


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, RegisterActivity.class);
                mContext.startActivity(i);
            }
        });


        Intent intent = getIntent();
        //DataProvider.getAllRestoCat(mContext);
        Bundle notification = getIntent().getBundleExtra("notification");
        if (notification != null) {
            // Start activity according to notification data
            startTargetActivity(notification);
        }
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setHomeAsUpIndicator(R.drawable.ic_menu);


        drawer.addDrawerListener(toggle);


        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toggle.syncState();



        navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);// where user avatar, email, name is seen
        ivUser = navHeader.findViewById(R.id.ivUser);
        tvUsername = navHeader.findViewById(R.id.tvUsername);
        tvOr = navHeader.findViewById(R.id.tvOr);
        Menu menuNav = navigationView.getMenu();
        Button navLogout = navHeader.findViewById(R.id.nav_logout);
        //Button navAuth = (Button)findViewById(R.id.nav_auth);
        Button navAuth = navHeader.findViewById(R.id.nav_auth);
        Button signup = navHeader.findViewById(R.id.nav_signup);

        cafe = navHeader.findViewById(R.id.iv_cafe);
        dinner = navHeader.findViewById(R.id.iv_dinner);
        nightlife = navHeader.findViewById(R.id.iv_night_life);
        delivery = navHeader.findViewById(R.id.iv_delivery);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RegisterActivity.class));

            }
        });

        /*dinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager =getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container,new RestaurantFragment()).commit();
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        nightlife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager =getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container,new RestaurantFragment()).commit();
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager =getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container,new RestaurantFragment()).commit();
                drawer.closeDrawer(GravityCompat.START);
            }
        });
*/


        if (TextUtils.equals(MFunction.getMyPrefVal("is_login", mContext), "true")) {
            navAuth.setVisibility(View.GONE);
            navLogout.setVisibility(View.VISIBLE);
            signup.setVisibility(View.GONE);
            Picasso.with(mContext)
                    .load(MFunction.getMyPrefVal("photoUrl", mContext))
                    .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                    .error(mContext.getResources().getDrawable(R.drawable.default_image))
                    .fit()
                    .into(ivUser);
            tvUsername.setText(MFunction.getMyPrefVal("first_name", mContext) + " " + MFunction.getMyPrefVal("last_name", mContext));
            tvOr.setText(MFunction.getMyPrefVal("email", mContext));

            System.out.println("PROFILE: user_id " + (MFunction.getMyPrefVal("user_id", mContext)));


        } else {
            hideItem();
            navAuth.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            navLogout.setVisibility(View.GONE);
        }

        /*Click event for Login Logout Button*/
        navAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, LoginActivity.class));
            }
        });
        navLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setItemHorizontalTranslationEnabled(false);
        //bottomNavigationView.getMenu().getItem(0).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FirebaseMessaging.getInstance().subscribeToTopic("All");
        mGeofenceList = new ArrayList<Geofence>();

        //initializing EasyWayLocation
        easyWayLocation = new EasyWayLocation(this, true, true, 1000);
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        if (!TextUtils.isEmpty(lati.toString())) {
            MFunction.putMyPrefVal("lati", lati.toString(), this);
        }
        if (!TextUtils.isEmpty(longi.toString())) {
            MFunction.putMyPrefVal("longi", longi.toString(), this);
        }
        easyWayLocation.setListener(this);

        /*initializing GeofencingClient and adding geofencing*/
        mGeofencingClient = LocationServices.getGeofencingClient(this);

        getAndSendCurrentTimeStamp();
        if (MFunction.getFirstTimeUse(mContext)) {
            MFunction.setFirstTimeUse(mContext);
            myfirstIntro();
        }

    }

    private void myfirstIntro() {

        /*new MaterialIntroView.Builder(this)
                .setInfoText("Click here for Menu")
                .enableDotAnimation(true)

                .setTarget(ibpic)

                .setFocusGravity(FocusGravity.LEFT)

                .enableFadeAnimation(true)
                .setFocusType(Focus.MINIMUM)
                .setShape(ShapeType.CIRCLE)
                .setDelayMillis(2000)
                .dismissOnTouch(true)
                .show();*/

        new GuideView.Builder(mContext)
                .setTitle("Click here for menu")
                .setContentText("Click here")
                .setTargetView(ibpic)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(GuideView.DismissType.targetView) //optional - default dismissible by TargetView
                .build()
                .show();
    }

    private void hideItem() {
        navigationView.getMenu().setGroupVisible(R.id.group1, false);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, query, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_option, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);

        ImageView searchIcon = searchView.findViewById(searchIconId);
        final PorterDuffColorFilter colorFilter
                = new PorterDuffColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);

        searchIcon.setColorFilter(colorFilter);

        menu.findItem(R.id.action_rate).setVisible(false);
        menu.findItem(R.id.action_search).setIcon(R.drawable.ic_search_red);
        menu.findItem(R.id.action_notifications).setIcon(R.drawable.ic_notifications_red);
        gMenu = menu;
        unsetActionBarTitle();
        openRequiredFragment();


        return true;
    }


    public void openRequiredFragment() {

        Intent mIntent = getIntent();
        isCuisinesSearch = mIntent.getBooleanExtra("search_cuisines", false);
        isCategorySearch = mIntent.getBooleanExtra("search_category", false);
        isApplyFilter = mIntent.getBooleanExtra("apply_filter", false);
        if (isCuisinesSearch) {
            String cuisine = mIntent.getStringExtra("cuisine");
            Bundle bundle = new Bundle();
            bundle.putBoolean("cuisines_search", true);
            bundle.putString("cuisine", cuisine);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            RestaurantFragment restoFrag = new RestaurantFragment();
            restoFrag.setArguments(bundle);
            transaction.replace(R.id.fragment_container, restoFrag);
            transaction.commit();
            handleIntent(getIntent());
            bottomNavigationView.getMenu().findItem(R.id.navigation_restaurant).setChecked(true);
        } else if (isCategorySearch) {
            String category = mIntent.getStringExtra("category");
            Bundle bundle = new Bundle();
            bundle.putBoolean("category_search", true);
            bundle.putString("category", category);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            RestaurantFragment restoFrag = new RestaurantFragment();
            restoFrag.setArguments(bundle);
            transaction.replace(R.id.fragment_container, restoFrag);
            transaction.commit();
            handleIntent(getIntent());
            bottomNavigationView.getMenu().findItem(R.id.navigation_restaurant).setChecked(true);
        } else if (isApplyFilter) {

            Bundle bundle = new Bundle();
            bundle.putBoolean("apply_filter", true);
            bundle.putString("categories", mIntent.getStringExtra("categories"));
            bundle.putString("cuisines", mIntent.getStringExtra("cuisines"));
            bundle.putString("amenities", mIntent.getStringExtra("amenities"));
            bundle.putString("rupees", String.valueOf(mIntent.getIntExtra("rupees", 0)));
            bundle.putString("rating", String.valueOf(mIntent.getIntExtra("rating", 0)));
            bundle.putString("order", String.valueOf(mIntent.getIntExtra("order", 0)));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            RestaurantFragment restoFrag = new RestaurantFragment();
            restoFrag.setArguments(bundle);
            transaction.replace(R.id.fragment_container, restoFrag);
            transaction.commit();
            handleIntent(getIntent());
            bottomNavigationView.getMenu().findItem(R.id.navigation_restaurant).setChecked(true);


        } else {
            //Manually displaying the first fragment at first

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, HomeFragment.newInstance("", ""));
            transaction.commit();
            handleIntent(getIntent());
            bottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home: {
                drawer.openDrawer(GravityCompat.START);
                return true;
            }

            case R.id.action_notifications: {
                Intent intent = new Intent(this, NotificationList.class);
                startActivity(intent);

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        /**************************/
        switch (id) {
            case R.id.nav_auth: {
                startActivity(new Intent(this, LoginActivity.class));
                break;
            }
            case R.id.nav_signup: {
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            }
            case R.id.nav_profile: {
                if (TextUtils.equals(MFunction.getMyPrefVal("is_login", mContext), "true")) {
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra("from_nav", "true");
                    intent.putExtra("user_id", MFunction.getMyPrefVal("user_id", mContext));
                    startActivity(intent);
                } else {
                    Toast.makeText(mContext, "Please Login First!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, LoginActivity.class));
                }
                break;
            }
            case R.id.nav_nearby: {
                Fragment selectedFragment = NearbyFragment.newInstance("", "");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                bottomNavigationView.getMenu().findItem(R.id.navigation_nearby).setChecked(true);
                item.setChecked(true);
                break;
            }
            case R.id.nav_restaurant: {
                Fragment selectedFragment = RestaurantFragment.newInstance("", "");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                bottomNavigationView.getMenu().findItem(R.id.navigation_restaurant).setChecked(true);
                break;
            }
            case R.id.nav_event: {
                Fragment selectedFragment = EventFragment.newInstance("", "");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                bottomNavigationView.getMenu().findItem(R.id.navigation_event).setChecked(true);
                break;
            }
            case R.id.nav_blog: {
                Fragment selectedFragment = BlogFragment.newInstance("", "");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                bottomNavigationView.getMenu().findItem(R.id.navigation_blog).setChecked(true);
                break;
            }
            case R.id.nav_aboutus: {
                startActivity(new Intent(this, AboutusActivity.class));
                break;
            }

            case R.id.nav_terms_condition: {
                startActivity(new Intent(this, TermActivity.class));
                break;
            }
            case R.id.nav_faq: {
                startActivity(new Intent(this, FaqActivity.class));
                break;
            }
            case R.id.nav_contactus: {
                startActivity(new Intent(this, ContactActivity.class));
                break;
            }
            case R.id.nav_logout: {
                logOut();
                break;
            }
            case R.id.nav_add_resto: {
                //if(TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")){

              /*  String isLogin = MFunction.getMyPrefVal("is_login", mContext);
                if (TextUtils.equals(isLogin, "true")) {

                    startActivity(new Intent(this, CreateRestoActivity.class));
                    break;
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                    break;
                }*/
               /* } else {
                    //startActivity(new Intent(this,CreateRestoActivity.class));
                    startActivity(new Intent(this,LoginActivity.class));
                    Toast.makeText(mContext, "Please Login First", Toast.LENGTH_SHORT).show();
                }*/
                startActivity(new Intent(this, CreateRestoActivity.class));
                break;



            }
            case R.id.nav_privacy: {
                startActivity(new Intent(getApplicationContext(), PolicyActivity.class));
                break;
            }
            case R.id.nav_home: {
                Fragment selectedFragment = HomeFragment.newInstance("", "");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                bottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);


                break;
            }


        }
        /**************************/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isCuisinesSearch || isCategorySearch || isApplyFilter) {
            finish();
        } else {


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Want To Exit?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", null).show();
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void locationOn() {
        // make the device update its location
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12153);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            easyWayLocation.beginUpdates();
            // Permission has already been granted
        }

    }

    @Override
    public void onPositionChanged() {

        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        List<Resto> top5NearbyResto = Resto.getNearbyResto(lati,longi,Long.valueOf(1000000000));

        if(!top5NearbyResto.isEmpty()){

            for (Resto resto: top5NearbyResto
            ) {
                mGeofenceList.add(new Geofence.Builder()
                        // Set the request ID of the geofence. This is a string to identify this
                        // geofence.
                        .setRequestId("resto"+resto.resto_id)

                        .setCircularRegion(
                                Double.valueOf(resto.lati),
                                Double.valueOf(resto.longi),
                                MConstant.GEOFENCE_RADIUS
                        )
                        .setExpirationDuration(Geofence.GEOFENCE_TRANSITION_ENTER)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build());
            }

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                          //  Toast.makeText(mContext, "Geofence Added", Toast.LENGTH_SHORT).show();
                            Log.i("mInfo", "Geofence Added");
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("Geofence message...-> ",e.toString());
                           // Toast.makeText(mContext, "Failed to add Geofence", Toast.LENGTH_SHORT).show();
                            Log.i("mInfo", "Failed to add Geofence");
                        }
                    });
        }

        String country = EasyWayLocation.getAddress(this, lati, longi, true, false);
        String locality = EasyWayLocation.getAddress(this, lati, longi, false, false);//city
        String fullAddress = EasyWayLocation.getAddress(this, lati, longi, false, true);
        if (!TextUtils.isEmpty(lati.toString()))
            MFunction.putMyPrefVal("lati", lati.toString(), this);
        if (!TextUtils.isEmpty(longi.toString()))
            MFunction.putMyPrefVal("longi", longi.toString(), this);
        if (!TextUtils.isEmpty(country)) MFunction.putMyPrefVal("country", country, this);
        if (!TextUtils.isEmpty(locality)) MFunction.putMyPrefVal("locality", locality, this);
        if (!TextUtils.isEmpty(fullAddress))
            MFunction.putMyPrefVal("fullAddress", fullAddress, this);
        //Toast.makeText(this, "("+lati.toString()+","+longi.toString()+")"+fullAddress, Toast.LENGTH_LONG).show();
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.app_name), "Please Turn On Location for good result.", null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SETTING_REQUEST_CODE:
                easyWayLocation.onActivityResult(resultCode);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // make the device update its location
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12153);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            easyWayLocation.beginUpdates();
            // Permission has already been granted
        }

    }

    @Override
    protected void onPause() {
        // stop location updates (saves battery)
        easyWayLocation.endUpdates();
        super.onPause();
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_EXIT);
        builder.addGeofences(mGeofenceList);
        Log.i("Geofence","Checking Geofence");
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private void logOut() {
        //TODO manage proper way
        String[] tempArray = {
                "is_login",
                "user_id",
                "first_name",
                "last_name",
                "phone",
                "address",
                "email",
                "about",
                "avatar",
                "reviewed_rest",
                "status",
                "blogger",
                "point",
                "followers",
                "photoUrl"
        };
        MFunction.clearMyPrefVal(getApplicationContext(), tempArray);
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 12153: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    easyWayLocation.beginUpdates();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    public void unsetActionBarTitle() {
        toolbar.setBackgroundColor(getResources().getColor(R.color.White));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);

        searchView.findViewById(searchIconId);
        searchView.setMaxWidth(280);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                return false;

            }

        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String myquery = String.valueOf(searchView.getQuery());
                if (myquery.length() != 0) {
                    // Todo some function here i need to call

                    Log.i("test", "test");

                    //search(myquery);
                    gotoHomeFragment(myquery);

                }

                return false;
            }
        });


//       searchView.setOnClickListener((new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mContext," I am clicked",Toast.LENGTH_SHORT).show();
//
//            }
//        }));
        ImageView searchIcon = searchView.findViewById(searchIconId);
        searchIcon.setImageResource(R.drawable.ic_search_red);



        /*SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)my_search_view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(#FFFFFF);
        searchAutoComplete.setTextColor(#FFFFFF);*/


        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_menu);
        gMenu.findItem(R.id.action_search).setVisible(true).setIcon(R.drawable.ic_search_red);
        gMenu.findItem(R.id.action_notifications).setIcon(R.drawable.ic_notifications_red);


        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        ImageView ivLogo = toolbar.findViewById(R.id.toolbar_logo);
        ivLogo.setVisibility(View.VISIBLE);
        mTitle.setVisibility(View.GONE);

        Window window = getWindow();
        View view = window.getDecorView();
        setLightStatusBar(view, this);

    }

    /*private void search(String myquery) {

        List<Resto> myresto = new ArrayList<>();
        List<Event> myevent = new ArrayList<>();
        String query = myquery.toLowerCase();
        if(true){
        for (Resto item :allresto){
            String restoname = item.name.toLowerCase();

            if(restoname.contains(query)){
                myresto.add(item);
            }

            for(Event eventitem :allevent){
                String eventanme = eventitem.name.toLowerCase();
                if (eventanme.contains(query))
                {
                    myevent.add(eventitem);
                }
            }

        }

        }

    }*/

    public void setActionBarTitle(String title) {

        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);
        searchView.findViewById(searchIconId);
        searchView.setOnClickListener((new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, " I am clicked", Toast.LENGTH_SHORT).show();

            }
        }));
//        EditText editText = (EditText) searchView.findViewById(searchIconId);
//        String check = String.valueOf(editText.getText());
//        Toast.makeText(mContext,check,Toast.LENGTH_SHORT).show();
        ImageView searchIcon = searchView.findViewById(searchIconId);
        searchIcon.setImageResource(R.drawable.ic_search);

        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_menu_white);
        gMenu.findItem(R.id.action_search).setVisible(false);
        gMenu.findItem(R.id.action_notifications).setIcon(R.drawable.ic_notifications);

        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        ImageView ivLogo = toolbar.findViewById(R.id.toolbar_logo);
        ivLogo.setVisibility(View.GONE);
        mTitle.setVisibility(View.VISIBLE);
        mTitle.setText(title);

        Window window = getWindow();
        View view = window.getDecorView();
        clearLightStatusBar(view, this);


    }

    public static void setLightStatusBar(View view, Activity activity) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            activity.getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    public static void clearLightStatusBar(View view, Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Window window = activity.getWindow();
            window.setStatusBarColor(ContextCompat
                    .getColor(activity, R.color.colorPrimaryDark));

        }
    }


    public void gotoRestaurantFragment() {
        Fragment selectedFragment = RestaurantFragment.newInstance("", "");
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
        bottomNavigationView.getMenu().findItem(R.id.navigation_restaurant).setChecked(true);
    }

    public void gotoEventFragment() {
        Fragment selectedFragment = EventFragment.newInstance("", "");
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
        bottomNavigationView.getMenu().findItem(R.id.navigation_event).setChecked(true);
    }

    public void gotoBlogFragment() {
        Fragment selectedFragment = BlogFragment.newInstance("", "");
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
        bottomNavigationView.getMenu().findItem(R.id.navigation_blog).setChecked(true);
    }

    public void gotoHomeFragment(String tempString) {

        Bundle tempBundle = new Bundle();
        tempBundle.putString("main_search_string", tempString);
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(tempBundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
    }


    public void openCategoriesActivity(View v) {


        Intent intent = new Intent(mContext, CategoriesActivity.class);
        startActivity(intent);


    }

    public void openCuisinesActivity(View v) {


        Intent intent = new Intent(mContext, CuisinesActivity.class);
        startActivity(intent);


    }


    private void startTargetActivity(Bundle bundle) {
        String activity = bundle.getString("activity");
        Intent intent = new Intent();
        switch (activity) {
            case "EventDetailActivity": {
                intent = new Intent(mContext, EventDetailActivity.class);
                break;
            }

            case "RestoDetailActivity": {
                intent = new Intent(mContext, RestoDetailActivity.class);
                break;
            }

            case "BlogDetailActivity": {
                intent = new Intent(mContext, BlogDetailActivity.class);
                break;
            }

        }
        finish();
        startActivity(intent, bundle);
    }


    public void openDialog() {
        dialog = new Dialog(mContext); // Context, this, etc.
        dialog.setContentView(R.layout.loading_dialog);
        dialog.setTitle(null);
        final FrameLayout frameLayout = dialog.findViewById(R.id.placeholder);
        final VideoView videoView = dialog.findViewById(R.id.loading_animation);
        final ConstraintLayout constraintLayout = dialog.findViewById(R.id.dialogBackground);
        videoView.setVideoPath("android.resource://" + mContext.getPackageName() + "/" + R.raw.foodpal);
        videoView.setZOrderOnTop(true);

        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(final MediaPlayer mp, int what, int extra) {

                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video started; hide the placeholder.
                            mp.setLooping(true);
                            frameLayout.setVisibility(View.GONE);


                            return true;
                        }
                        return false;
                    }
                });
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setFormat(PixelFormat.TRANSLUCENT);
        dialog.show();
    }

    public void closeDialog() {
        if (dialog != null)
            dialog.dismiss();
    }

    public void openCategoryPage(View view) {

//        FragmentManager fragmentManager =getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.fragment_container,new RestaurantFragment()).commit();
//        drawer.closeDrawer(GravityCompat.START);


        Intent cIntent = new Intent(mContext, MainActivity.class);
        cIntent.putExtra("search_category", true);

        //cIntent.putExtra("category", tvCategory.getText().toString());
        cIntent.putExtra("category", "Cafe");

        mContext.startActivity(cIntent);


    }

    public void DinnerCatagory(View view) {
        Intent cIntent = new Intent(mContext, MainActivity.class);
        cIntent.putExtra("search_category", true);

        //cIntent.putExtra("category", tvCategory.getText().toString());
        cIntent.putExtra("category", "Cafe");

        mContext.startActivity(cIntent);


    }

    public void nightpartyCatagory(View view) {
        Intent cIntent = new Intent(mContext, MainActivity.class);
        cIntent.putExtra("search_category", true);

        //cIntent.putExtra("category", tvCategory.getText().toString());
        cIntent.putExtra("category", "Cafe");

        mContext.startActivity(cIntent);


    }

    public void DeliveryCatagory(View view) {
        Intent cIntent = new Intent(mContext, MainActivity.class);
        cIntent.putExtra("search_category", true);

        //cIntent.putExtra("category", tvCategory.getText().toString());
        cIntent.putExtra("category", "Cafe");

        mContext.startActivity(cIntent);

    }

    public void getAndSendCurrentTimeStamp() {

        String prefTimestamp = MFunction.getMyPrefVal("pref_timestamp", mContext, "1");

        Long longPrefTimestamp = Long.valueOf(prefTimestamp);

        Long longCurrentTimestamp = System.currentTimeMillis();
        String currentTimestamp = String.valueOf(longCurrentTimestamp);
        Long difference = longCurrentTimestamp - longPrefTimestamp;

        boolean okToFire = difference >= TimeUnit.MINUTES.toMillis(5);
        if (okToFire) {

            MFunction.putMyPrefVal("pref_timestamp", currentTimestamp, mContext);
            sendLatestTimestamp(longCurrentTimestamp);
        }
    }

    private void sendLatestTimestamp(Long temp) {

        String url = MConstant.API_END + "/saveTimestamp";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("member_id",MFunction.getMyPrefVal("user_id",mContext,""));
        params.put("timestamp", String.valueOf(temp));
        params.put("fb_token", FirebaseInstanceId.getInstance().getToken());
        params.put("device", MFunction.getDeviceInfo());
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // Todo  add some operation here for result of notification of long unactive
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyError", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
