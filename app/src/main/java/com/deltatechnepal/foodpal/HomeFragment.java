package com.deltatechnepal.foodpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.CommonPopularBlogAdapter;
import com.deltatechnepal.adapter.NearbyEventAdapter;
import com.deltatechnepal.adapter.NearbyMostlovedAdapter;
import com.deltatechnepal.adapter.HomeGridAdapter;
import com.deltatechnepal.adapter.NearbyRestoAdapter;
import com.deltatechnepal.adapter.SearchEventAdapter;
import com.deltatechnepal.adapter.SearchEventHomeFragmentAdapter;
import com.deltatechnepal.adapter.SearchRestoAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.imagevideoslider.library.Animations.DescriptionAnimation;
import com.deltatechnepal.imagevideoslider.library.SliderLayout;
import com.deltatechnepal.imagevideoslider.library.SliderTypes.BaseSliderView;
import com.deltatechnepal.imagevideoslider.library.SliderTypes.TextSliderView;
import com.deltatechnepal.ormmodel.Banner;
import com.deltatechnepal.ormmodel.Blog;
import com.deltatechnepal.ormmodel.Event;
import com.deltatechnepal.ormmodel.Foodfact;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.androidannotations.annotations.App;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import smartdevelop.ir.eram.showcaseviewlib.GuideView;

import static com.deltatechnepal.foodpal.MainActivity.TAG;

public class HomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private SliderLayout mSponRestoSlider;
    private TextView tvTitle, tvSubtitle, tvMostLoved, tvquicksearch, tvseemorecategories, tvcuisines, tvseemorecuisines;
    private TextView tvrecommendationsection, tvrecommendation, tvrestomore, tvupcomingevents, tvEventMore, tvpopularblogs, tvMoreBlogs;
    private String lati, longi, country, city;
    private AdView adView1, adView2;
    List<Resto> restoList;
    List<Event> eventList;
    List<Blog> blogList;
    Double lat, lon;
    private Context mContext;
    ProgressDialog mProd;
    boolean firstLaunch = false;
    String allData, receivequery;
    View v;
    private Bundle savedState = null;
    RecyclerView rvFeaturedResto;
    GridView gvCuisines, gvQuickSearch;
    RecyclerView rvSearchResultforEvent, rvSearchResultforRestro, rvMostLoved, rvPopularBlogs, rvUpcomingEvents;
    SearchRestoAdapter searchRestoAdapter;
    ImageButton gototop;
    ScrollView scrollView;


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        searchRestoAdapter = new SearchRestoAdapter();

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            receivequery = getArguments().getString("main_search_string");
        }

        /*initializing variables*/
        restoList = new ArrayList<Resto>();
        lati = MFunction.getMyPrefVal("lati", getActivity());
        lat = (lati != null) ? Double.parseDouble(lati) : null;
        longi = MFunction.getMyPrefVal("longi", getActivity());
        lon = (longi != null) ? Double.parseDouble(longi) : null;
        country = MFunction.getMyPrefVal("country", getActivity());
        //Double distanceFromMe = EasyWayLocation.calculateDistance(26.4711892,87.2810679,26.4674076,87.2838407);
        city = MFunction.getMyPrefVal("locality", getActivity());
        restoList = Resto.getAllResto(500);
        eventList = Event.getAllEvent(500);
        Log.i(TAG, "onCreate: ");




    }

    private void searchviewsearch(String receivequery) {

        List<Resto> myresto = new ArrayList<>();
        List<Event> myevent = new ArrayList<>();


        if (!TextUtils.isEmpty(receivequery)) {

            String query = receivequery.toLowerCase();
            Log.i("test", "test");
            restoList = Resto.getAllResto(500);
            eventList = Event.getAllEvent(500);

            for (Resto item : restoList) {
                String restoname = item.name.toLowerCase();


                if (restoname.contains(query)) {
                    myresto.add(item);
                }
            }
            searchRestoAdapter = new SearchRestoAdapter(mContext, myresto);
            rvSearchResultforRestro.setAdapter(searchRestoAdapter);


            for (Event eventitem : eventList) {
                String eventanme = eventitem.name.toLowerCase();
                if (eventanme.contains(query)) {
                    myevent.add(eventitem);
                }
            }
            SearchEventHomeFragmentAdapter searchEventAdapter = new SearchEventHomeFragmentAdapter( mContext, myevent, true);
            rvSearchResultforEvent.setAdapter(searchEventAdapter);
        } else {
            Toast.makeText(mContext, "I am empty again", Toast.LENGTH_SHORT).show();

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list_row_resto_opening for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        v = view;
        rvSearchResultforEvent = (RecyclerView) view.findViewById(R.id.rvSearchResultforEvent);
        LinearLayoutManager popularLM1 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvSearchResultforEvent.setHasFixedSize(true);
        rvSearchResultforEvent.setNestedScrollingEnabled(true);
        rvSearchResultforEvent.setLayoutManager(popularLM1);


        rvSearchResultforRestro = (RecyclerView) view.findViewById(R.id.rvSearchResultforRestro);
        LinearLayoutManager popularLM2 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvSearchResultforRestro.setHasFixedSize(true);
        rvSearchResultforRestro.setNestedScrollingEnabled(true);
        rvSearchResultforRestro.setLayoutManager(popularLM2);

        rvMostLoved = (RecyclerView) view.findViewById(R.id.rvMostLoved);
        rvPopularBlogs = (RecyclerView) view.findViewById(R.id.rvPopularBlogs);
        rvUpcomingEvents = (RecyclerView) view.findViewById(R.id.rvUpcomingEvents);
        rvFeaturedResto = (RecyclerView) view.findViewById(R.id.rvFeaturedResto);

        tvquicksearch = (TextView) view.findViewById(R.id.tvQuickSearch);
        tvseemorecategories = (TextView) view.findViewById(R.id.tvSeeMoreCategories);
        tvcuisines = (TextView) view.findViewById(R.id.tvCuisines);
        tvseemorecuisines = (TextView) view.findViewById(R.id.tvSeemoreCuisines);
        tvrecommendation = (TextView) view.findViewById(R.id.tvRecommendation);
        tvrecommendationsection = (TextView) view.findViewById(R.id.tvRecommendationSection);
        tvrestomore = (TextView) view.findViewById(R.id.tvRestoMore);
        tvupcomingevents = (TextView) view.findViewById(R.id.tvUpcomingEvents);
        tvEventMore = (TextView) view.findViewById(R.id.tvEventMore);
        tvpopularblogs = (TextView) view.findViewById(R.id.tvPopularBlogs);
        tvMoreBlogs = (TextView) view.findViewById(R.id.tvMoreBlogs);
        tvseemorecuisines = (TextView) view.findViewById(R.id.tvSeemoreCuisines);

        mSponRestoSlider = (SliderLayout) view.findViewById(R.id.sponRestoSlider);
        tvTitle = (TextView) view.findViewById(R.id.tvName);
        tvSubtitle = (TextView) view.findViewById(R.id.tvSubtitle);


        gvQuickSearch = (GridView) view.findViewById(R.id.gvCategories);
        gvCuisines = (GridView) view.findViewById(R.id.gvCuisines);

        scrollView = view.findViewById(R.id.fcontainer);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });
        MainActivity mainActivity =(MainActivity) getActivity();
        gototop = mainActivity.findViewById(R.id.gotoTop);
        gototop.setVisibility(View.GONE);



        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0,0);
            }
        });
       /* gototop = view.findViewById(R.id.gotoTop);
        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0,0);
            }
        });*/



        /*Click Event For See More*/

        TextView tvRestoMore = (TextView) view.findViewById(R.id.tvRestoMore);
        tvRestoMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).gotoRestaurantFragment();
            }
        });

        tvEventMore = (TextView) view.findViewById(R.id.tvEventMore);
        tvEventMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).gotoEventFragment();
            }
        });

        tvMoreBlogs = (TextView) view.findViewById(R.id.tvMoreBlogs);
        tvMoreBlogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).gotoBlogFragment();
            }
        });


        /*Setting Banner*/
        setmSponRestoSlider();

        /*Setting Most Loved Section*/
        String loveString = "Most @ Places Around Town";
        SpannableString tempSS = new SpannableString(loveString);

        int loveIndex = loveString.indexOf("@");
        ImageSpan imageSpan = new ImageSpan(mContext, R.drawable.heart);

        tempSS.setSpan(imageSpan, loveIndex, loveIndex + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvMostLoved = (TextView) view.findViewById(R.id.tvMostLoved);
        tvMostLoved.setText(tempSS);

        //setgvMostLoved(view);

        /*Setting Food Fact*/
        Foodfact ff = Foodfact.getFoodfact();
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        String ffTitle = (ff != null) ? ff.title : "";
        String ffSubtitle = (ff != null) ? ff.content : "";
        tvName.setText(ffTitle);
        tvSubtitle = (TextView) view.findViewById(R.id.tvSubtitle);
        tvSubtitle.setText(ffSubtitle);
        tvSubtitle.setSelected(true);

        /*Setting Quick Search*/
        setgvQS(view);
        setgvCuisines(view);

        /*Advertisement1*/
        adView1 = (AdView) view.findViewById(R.id.adView1);
        adView2 = (AdView) view.findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        /*AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("F44E190C41CA712B9300447CD4349BF1")
                .build();*/
        adView1.loadAd(adRequest);

        fetchData(view);

        /*Advertisement1*/
        adView1 = (AdView) view.findViewById(R.id.adView1);
        AdRequest adRequest2 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest2);

        if (MFunction.getFirstTimeUse(mContext)) {
            MFunction.setFirstTimeUse(mContext);
        /*    ShowIntro("categories", "Quickly find your food through the categories", gvQuickSearch, 1);*/
        }

        if (!TextUtils.isEmpty(receivequery)) {

            view.findViewById(R.id.groupSearchResult).setVisibility(View.VISIBLE);
            view.findViewById(R.id.groupSearchResult2).setVisibility(View.VISIBLE);
            searchviewsearch(receivequery);
            //tvMostLoved.setVisibility(View.GONE);
            //  rvMostLoved.setVisibility(View.GONE);
            mSponRestoSlider.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            tvSubtitle.setVisibility(View.GONE);
            tvquicksearch.setVisibility(View.GONE);
            tvseemorecategories.setVisibility(View.GONE);
            gvCuisines.setVisibility(View.GONE);
            tvcuisines.setVisibility(View.GONE);
            tvseemorecategories.setVisibility(View.GONE);
            gvCuisines.setVisibility(View.GONE);
            tvrecommendation.setVisibility(View.GONE);
            tvrecommendationsection.setVisibility(View.GONE);
            tvrestomore.setVisibility(View.GONE);
            rvFeaturedResto.setVisibility(View.GONE);
            tvupcomingevents.setVisibility(View.GONE);
            tvEventMore.setVisibility(View.GONE);
            rvUpcomingEvents.setVisibility(View.GONE);
            tvpopularblogs.setVisibility(View.GONE);
            tvMoreBlogs.setVisibility(View.GONE);
            rvPopularBlogs.setVisibility(View.GONE);
            adView1.setVisibility(View.GONE);
            adView2.setVisibility(View.GONE);

            gvQuickSearch.setVisibility(View.GONE);
            gvCuisines.setVisibility(View.GONE);
            MainActivity activity = (MainActivity) getActivity();
            activity.closeDialog();


        } else {

            view.findViewById(R.id.groupSearchResult).setVisibility(View.GONE);
            view.findViewById(R.id.groupSearchResult2).setVisibility(View.GONE);
            //  tvMostLoved.setVisibility(View.VISIBLE);
            // rvMostLoved.setVisibility(View.VISIBLE);
            mSponRestoSlider.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            tvSubtitle.setVisibility(View.VISIBLE);
            tvquicksearch.setVisibility(View.VISIBLE);
            tvseemorecategories.setVisibility(View.VISIBLE);
            gvCuisines.setVisibility(View.VISIBLE);
            tvcuisines.setVisibility(View.VISIBLE);
            tvseemorecategories.setVisibility(View.VISIBLE);
            gvCuisines.setVisibility(View.VISIBLE);
            tvrecommendation.setVisibility(View.VISIBLE);
            tvrecommendationsection.setVisibility(View.VISIBLE);
            tvrestomore.setVisibility(View.VISIBLE);
            rvFeaturedResto.setVisibility(View.VISIBLE);
            tvupcomingevents.setVisibility(View.VISIBLE);
            tvEventMore.setVisibility(View.VISIBLE);
            rvUpcomingEvents.setVisibility(View.VISIBLE);
            tvpopularblogs.setVisibility(View.VISIBLE);
            tvMoreBlogs.setVisibility(View.VISIBLE);
            rvPopularBlogs.setVisibility(View.VISIBLE);
            adView1.setVisibility(View.GONE);
            adView2.setVisibility(View.GONE);

            gvQuickSearch.setVisibility(View.VISIBLE);
            gvCuisines.setVisibility(View.VISIBLE);
            MainActivity activity = (MainActivity) getActivity();
            activity.closeDialog();

        }


        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void createDummyData() {
        restoList = Resto.getAllResto(20);
    }

    private void setmSponRestoSlider() {
        List<Banner> sponRestList = Banner.getActiveBanner(3);

        mSponRestoSlider.removeAllSliders();
        for (Banner item : sponRestList) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            String title = (item.title.length() > 0) ? item.title : "";
            String caption = (item.caption.length() > 0) ? "(" + item.caption + ")" : "";
            String description = title + caption;
            String imageUrl = MConstant.SERVER + "/uploads/banner/1350x670".concat(item.image);
            Bundle bundle = new Bundle();
            bundle.putString("externalLink", item.link);
            textSliderView
                    .description(description)
                    .image(imageUrl)
                    .bundle(bundle)
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    Bundle bundle = slider.getBundle();
                    if (TextUtils.isEmpty(bundle.getString("externalLink"))) return;
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("title", slider.getDescription());
                    intent.putExtra("url", bundle.getString("externalLink"));
                    startActivity(intent);
                }
            });
            mSponRestoSlider.addSlider(textSliderView);
        }
        mSponRestoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSponRestoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSponRestoSlider.setCustomAnimation(new DescriptionAnimation());
        mSponRestoSlider.setDuration(MConstant.BANNER_TRANSITION_DURATION);
    }

    private void setgvQS(View view) {
        gvQuickSearch = (GridView) view.findViewById(R.id.gvCategories);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String, String> map1 = new HashMap<>();
        map1.put("cat_id", "1");
        map1.put("image", "cafe");
        map1.put("cat_name", "Cafe");

        HashMap<String, String> map2 = new HashMap<>();
        map2.put("cat_id", "2");
        map2.put("image", "dinner");
        map2.put("cat_name", "Dinner");

        HashMap<String, String> map3 = new HashMap<>();
        map3.put("cat_id", "3");
        map3.put("image", "fine_dining");
        map3.put("cat_name", "Night Life");

        HashMap<String, String> map4 = new HashMap<>();
        map4.put("cat_id", "4");
        map4.put("image", "delivery");
        map4.put("cat_name", "Delivery");

        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        tempList.add(map4);

        HomeGridAdapter nearbyQuickSearch = new HomeGridAdapter(mContext, true, tempList);
        gvQuickSearch.setAdapter(nearbyQuickSearch);

    }


    private void setgvCuisines(View view) {
        gvCuisines = (GridView) view.findViewById(R.id.gvCuisines);
        List<HashMap> tempList = new ArrayList<HashMap>();

        HashMap<String, String> map1 = new HashMap<>();
        map1.put("cat_id", "1");
        map1.put("image", "chinese");
        map1.put("cat_name", "");

        HashMap<String, String> map2 = new HashMap<>();
        map2.put("cat_id", "2");
        map2.put("image", "indian");
        map2.put("cat_name", "");

        HashMap<String, String> map3 = new HashMap<>();
        map3.put("cat_id", "3");
        map3.put("image", "italian");
        map3.put("cat_name", "");

        tempList.add(map1);
        tempList.add(map2);
        tempList.add(map3);
        HomeGridAdapter nearbyQuickSearch = new HomeGridAdapter(mContext, false, tempList);
        gvCuisines.setAdapter(nearbyQuickSearch);


        gvCuisines.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {


                Intent cIntent = new Intent(mContext, MainActivity.class);
                cIntent.putExtra("search_cuisines", true);
                switch (i) {

                    case 0:
                        cIntent.putExtra("cuisine", "chinese");
                        break;
                    case 1:
                        cIntent.putExtra("cuisine", "indian");
                        break;
                    case 2:
                        cIntent.putExtra("cuisine", "italian");
                        break;

                }

                startActivity(cIntent);
            }
        });

    }

    private List<Resto> getNearbyResto() {
        return Resto.getNearbyResto(lat, lon);
    }

    private List<Event> getNearbyEvent() {
        return Event.getNearbyEvent(lat, lon);
    }


    private void fetchData(View view) {
        final View mView = view;
        ((MainActivity) getActivity()).openDialog();
        String url = MConstant.API_END + "/getHomePageData";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Tag", "onResponse:i need a response "+response);
                        MainActivity activity = (MainActivity) getActivity();
                        if (activity != null) activity.closeDialog();
                        try {
                            if (!response.getBoolean("status")) return;
                            firstLaunch = true;
                            allData = response.getString("data");
                            setUpHomeData(allData);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MainActivity activity = (MainActivity) getActivity();
                        if (activity != null) activity.closeDialog();
                        setUpHomeData("");
                        Log.e("VolleyError", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }


    public void setUpHomeData(String allData) {

        if(TextUtils.isEmpty(allData)){

            //setMostLoved(mostLoved, v);
            //setSponsoredResto(sponsoredResto, v);
            setUpcomingEvents(eventList, v);
            //setPopularBlogs(popBlogs, v);


        } else {

            JsonParser jsonParser = new JsonParser();
            JsonObject data = (JsonObject) jsonParser.parse(allData);
            JsonArray mostLoved = data.getAsJsonArray("most_loved_around");
            JsonArray sponsoredResto = data.getAsJsonArray("sponsored_restaurants");
            JsonArray upcomingEvents = data.getAsJsonArray("upcoming_events");
            JsonArray popBlogs = data.getAsJsonArray("popular_blogs");

            setMostLoved(mostLoved, v);
            setSponsoredResto(sponsoredResto, v);
            setUpcomingEvents(upcomingEvents, v);
            setPopularBlogs(popBlogs, v);

        }



    }


    private void setMostLoved(JsonArray tempArray, View view) {
        rvMostLoved = (RecyclerView) view.findViewById(R.id.rvMostLoved);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        rvMostLoved.setHasFixedSize(true);
        rvMostLoved.setNestedScrollingEnabled(true);
        rvMostLoved.setLayoutManager(popularLM);
        NearbyMostlovedAdapter nearbyMostlovedAdapter = new NearbyMostlovedAdapter(mContext, tempArray);
        rvMostLoved.swapAdapter(nearbyMostlovedAdapter, false);
    }

    private void setSponsoredResto(JsonArray tempArray, View view) {
        rvFeaturedResto = view.findViewById(R.id.rvFeaturedResto);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        rvFeaturedResto.setHasFixedSize(true);
        rvFeaturedResto.setNestedScrollingEnabled(true);
        rvFeaturedResto.setLayoutManager(popularLM);
        NearbyRestoAdapter nearbyRestoAdapter = new NearbyRestoAdapter(mContext, tempArray, false);
        rvFeaturedResto.swapAdapter(nearbyRestoAdapter, false);


    }

    private void setUpcomingEvents(JsonArray tempArray, View view) {

        rvUpcomingEvents = view.findViewById(R.id.rvUpcomingEvents);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvUpcomingEvents.setHasFixedSize(true);
        rvUpcomingEvents.setNestedScrollingEnabled(true);
        rvUpcomingEvents.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyEventAdapter = new NearbyEventAdapter(mContext, tempArray, false);
        rvUpcomingEvents.swapAdapter(nearbyEventAdapter, false);
    }

    private void setUpcomingEvents(List<Event> eventList, View view) {

        rvUpcomingEvents = view.findViewById(R.id.rvUpcomingEvents);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvUpcomingEvents.setHasFixedSize(true);
        rvUpcomingEvents.setNestedScrollingEnabled(true);
        rvUpcomingEvents.setLayoutManager(popularLM);
        NearbyEventAdapter nearbyEventAdapter = new NearbyEventAdapter(mContext, eventList, false);
        rvUpcomingEvents.swapAdapter(nearbyEventAdapter, false);
    }


    private void setPopularBlogs(JsonArray tempArray, View view) {
        rvPopularBlogs = view.findViewById(R.id.rvPopularBlogs);
        LinearLayoutManager popularLM = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvPopularBlogs.setHasFixedSize(true);
        rvPopularBlogs.setNestedScrollingEnabled(true);
        rvPopularBlogs.setLayoutManager(popularLM);
        CommonPopularBlogAdapter commonPopularBlogAdapter = new CommonPopularBlogAdapter(mContext, tempArray);
        rvPopularBlogs.swapAdapter(commonPopularBlogAdapter, false);
    }


   /* private void ShowIntro(String title, String text, View view, final int type) {

        new GuideView.Builder(mContext)
                .setTitle(title)
                .setContentText(text)
                .setTargetView(view)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(GuideView.DismissType.targetView) //optional - default dismissible by TargetView
                .setGuideListener(new GuideView.GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        if (type == 1) {
                            ShowIntro("Cuisines", "Browse through our wide number of quisines section to taste food from different countries", gvCuisines, 2);
                        } else if (type == 2)
                            ShowIntro("Recommended Restaurants", "Browse through our recommended restaurants section to taste food from different restaurants", rvFeaturedResto, 3);
                    }
                })
                .build()
                .show();
    }*/
}







