package com.deltatechnepal.foodpal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.deltatechnepal.utility.DataProvider;
import com.deltatechnepal.utility.MFunction;

public class SplashActivity extends AppCompatActivity {
    private ImageView ivLogo;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*Fetching Data From Server*/
        fetchAllData();

        /*Animation for logo image*/
        Animation logoAnim = new AlphaAnimation(1, 0.3f);
        logoAnim.setDuration(1000);
        logoAnim.setInterpolator(new LinearInterpolator());
        logoAnim.setRepeatCount(2);
        logoAnim.setRepeatMode(Animation.REVERSE);
        ivLogo= (ImageView) findViewById(R.id.ivLogo);
        ivLogo.startAnimation(logoAnim);
        logoAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                /*if(TextUtils.equals(MFunction.getMyPrefVal("is_login",getApplicationContext()),"true")) {
                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                } else {
                    if(isFirstInstall){
                        startActivity(new Intent(getBaseContext(), WelcomeActivity.class));
                        //startActivity(new Intent(getBaseContext(), LoginActivity.class));
                    }
                }
                finish();*/
                if(TextUtils.equals(MFunction.getMyPrefVal("introduced",mContext),"true")){
                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                    finish();

                } else{
                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                    finish();

                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void fetchAllData(){
        //DataProvider.getAllResto(mContext);
        DataProvider.getBannerAndFoodFact(mContext);
        //DataProvider.getAllEvent(mContext);
        //DataProvider.getAllRestoPhoto(mContext);
    }
}












