package com.deltatechnepal.foodpal;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deltatechnepal.adapter.BlogDetailPopularBlogAdapter;
import com.deltatechnepal.app.AppController;
import com.deltatechnepal.model.MySingleton;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.deltatechnepal.foodpal.BlogCreateCommentActivity.BLOG_COMMENT;

public class BlogDetailActivity extends AppCompatActivity {
    private Context mContext;
    private String blogID, authorID,userID, slug;
    private JsonObject blog;
    private ImageView ivBannerImage,ivBlogger;
    private TextView tvBlogger,tvLike,tvComment,tvFollowers;
    private ShareActionProvider mShareActionProvider;
    private Button btnFollow;
    private Boolean isFollowing = false;
    private static final int COMMENT_RESULT_CODE=4001;
    private static final int ADD_COMMENT =100;
    private static final int ADD_LIKE=101;

    Dialog dialog;

    private JsonObjectRequest jsonObjectRequest;
    private JSONObject joData = new JSONObject();

    Button writeBlog,popularBlog,recentBlog;
    ScrollView scrollView;
    ImageButton gototop,share,bookmark;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        mContext = this;



        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {

            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_blog_detail);

        // making notification bar transparent
        changeStatusBarColor();

        /*Transparent Action Bar*/
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getSupportActionBar().setElevation(0);

        /*Setting Title of Activity*/
        this.setTitle("");

        /*blogID = getIntent().getStringExtra("blog_id");
        slug = getIntent().getStringExtra("slug");*/
        JsonParser jsonParser = new JsonParser();
        blog = (JsonObject) jsonParser.parse(getIntent().getStringExtra("blog"));

        System.out.println(" COMPLETE BLOG DATA "+blog);
        blogID = blog.get("id").getAsString();
        authorID = blog.get("member_id").getAsString();
        userID = MFunction.getMyPrefVal("user_id",mContext);
        tvFollowers = (TextView)findViewById(R.id.tvFollowers);

        tvLike=findViewById(R.id.tvLike);
        tvComment=findViewById(R.id.tvComment);


        share = findViewById(R.id.btnShare);
        bookmark = findViewById(R.id.btnBookmark);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,MConstant.SERVER+"/restaurant-details/"+MFunction.getStringVal(blog,"slug"));
                startActivity(Intent.createChooser(sharingIntent, "Share"));

            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(MFunction.getStringVal(blog,"id"),"2",mContext);

            }
        });

//{"slug":"blog-title","title":"Blog Title","created_at":"2019-06-25 15:44:55","first_name":"Pramod","about":"This is me pramod from janakpur.","last_name":"Karn","avatar":"default.png","details":"P/%TE5XpkAijBc%LjA;_-pZcbiU25E6feX5y/n6qxCTmhprLrqC3H%^hU!%q2,k'm`SHheoW^'mQ~zW93,C?~GtYk!wi/&'3KxW8","publish_date":"2019-06-25 15:44:55","image":"3d06b09bbc9f55a76baa057928314054.png","headerimage":"1bb9a7522274df58a82485e89dce2798.png","readingtime":"0","tags":"contents","id":"16","member_id":"60","blogger_type":"1","tlikes":0,"tfollowers":0}

//{"slug":"pramod-co-blog-here-2","title":"Pramod co blog here","created_at":"2019-06-25 12:52:21","first_name":"Pramod","about":"This is me pramod from janakpur.","last_name":"Karn","avatar":"default.png","details":"P/%TE5XpkAijBc%LjA;_-pZcbiU25E6feX5y/n6qxCTmhprLrqC3H%^hU!%q2,k'm`SHheoW^'mQ~zW93,C?~GtYk!wi/&'3KxW8","publish_date":"2019-06-25 12:52:21","image":"default.jpg","headerimage":"default.jpg","readingtime":"0","tags":"pramod","id":"15","member_id":"60","blogger_type":"1","tlikes":0,"tfollowers":0}


        //String followerIDS = blog.get("followers_ids").getAsString();
        String followerIDS = blog.get("tfollowers").getAsString();
        String[] tempArray = followerIDS.split(",");
        btnFollow = (Button) findViewById(R.id.btnFollow);
        if(tempArray.length>0) {
            for (String id:tempArray
                 ) {
                if(TextUtils.equals(id,userID)){
                    btnFollow.setText("Following");
                    isFollowing = true;
                    break;
                }

            }
        }

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFollowing){
                    follow();
                } else {
                    Toast.makeText(mContext, "You are already Following.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        setUI(blog);
        fetchBlog();
        TextView tvBlogger = (TextView) findViewById(R.id.tvBlogger);
        tvBlogger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,ProfileActivity.class);
                intent.putExtra("user_id",authorID);
                startActivity(intent);

            }
        });


        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO add like

                String url = MConstant.API_END+"/likeblog";

                try {
                    joData.put("blog_id",blogID);
                    joData.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
                    joData.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                  VolleyStandard(url,ADD_LIKE);
            }
        });

        writeBlog = (Button) findViewById(R.id.btnWriteaBlog);
        popularBlog = (Button) findViewById(R.id.btnPopularBlog);
        recentBlog = (Button) findViewById(R.id.btnRecentBlogs);
        scrollView = (ScrollView) findViewById(R.id.fcontainer);

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                if(i1>100){
                    gototop.setVisibility(View.VISIBLE);
                }
                else
                {
                    gototop.setVisibility(View.GONE);
                }


            }
        });

        gototop = findViewById(R.id.ontop);
        gototop.setVisibility(View.GONE);
        gototop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0,0);
            }
        });

        writeBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String isLogin = MFunction.getMyPrefVal("is_login",mContext);
                if(TextUtils.equals(isLogin,"true")){
                    startActivity(new Intent(mContext,CreateBlogActivity.class));
                } else{
                    startActivity(new Intent(mContext,LoginActivity.class));
                }
            }
        });

    }

    public void openBloggerProfile(View v){
        Intent intent=new Intent(mContext,ProfileActivity.class);
        intent.putExtra("author_id",authorID);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                //startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
                return true;
            }

            case R.id.action_share: {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,MConstant.SERVER+"/blog-details/"+blog.get("slug").getAsString());
                startActivity(Intent.createChooser(sharingIntent, "Share"));
                return true;
            }
            case R.id.action_bookmark: {
                if(!TextUtils.equals(MFunction.getMyPrefVal("is_login",mContext),"true")) {
                    Toast.makeText(mContext, "You must Login First", Toast.LENGTH_SHORT).show();
                }
                MFunction.bookMark(blogID,"0",mContext);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setUI(JsonObject temp){
        /*Setting Primary Data*/
        String imageUrl = MConstant.SERVER+"/uploads/blogs/"+temp.get("image").getAsString();
        ImageView ivBannerImage = (ImageView)findViewById(R.id.ivBannerImage);
        Picasso.with(mContext)
                .load(imageUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.logo))
                .error(mContext.getResources().getDrawable(R.drawable.logo))
                .into(ivBannerImage);

        TextView tvTitle = findViewById(R.id.tvName);
        tvTitle.setText(temp.get("title").getAsString());

        TextView tvDate = findViewById(R.id.tvDate);
        tvDate.setText(getFormattedDate(temp.get("publish_date").getAsString()));

        TextView tvReading = findViewById(R.id.tvReading);
        tvReading.setText(temp.get("readingtime").getAsString()+" min read");


        CircleImageView ivUser = findViewById(R.id.ivUser);
        String avatarUrl = MConstant.SERVER+"/uploads/avatars/"+temp.get("avatar").getAsString();
        Picasso.with(mContext)
                .load(avatarUrl)
                .placeholder(mContext.getResources().getDrawable(R.drawable.logo))
                .error(mContext.getResources().getDrawable(R.drawable.logo))
                .into(ivUser);
        TextView tvBlogger = findViewById(R.id.tvBlogger);
        tvBlogger.setText(temp.get("first_name").getAsString()+" "+temp.get("last_name").getAsString());
        tvBlogger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,ProfileActivity.class));
            }
        });

        TextView tvLevel = (TextView)findViewById(R.id.tvLevel);
        tvLevel.setText("Level 1");

        tvFollowers.setText(temp.get("tfollowers").getAsString()+" Followers");

        boolean selfProfile = (TextUtils.equals(authorID,MFunction.getMyPrefVal("user_id",mContext)))?true:false;
        if(selfProfile) {
            btnFollow.setVisibility(View.GONE);
        } else {
            btnFollow.setVisibility(View.VISIBLE);
        }

        /*Setting Tag for Cuisines*/
        List<Chip> chipList = new ArrayList<>();
        String tags = temp.get("tags").getAsString();
        String[] splited = tags.split(",");
        if(splited.length >0) {
            for(int i=0; i<splited.length; i++) {
                if(i<2) {
                    chipList.add(new Tag(splited[i]));
                }
            }
        }
        ChipView chipTags =  findViewById(R.id.chipTags);

        if (!chipList.isEmpty()) {

            chipTags.setVisibility(View.VISIBLE);


            chipTags.setChipList(chipList);
        }

        TextView tvDetails =  findViewById(R.id.tvDetails);
        tvDetails.setText(Html.fromHtml(temp.get("details").getAsString()));

        TextView tvLike = findViewById(R.id.tvLike);
        tvLike.setText("Like ("+temp.get("tlikes").getAsString()+")");

        /*Setting Popular and Recent Blogs*/


    }

    private void setPopularBlogs(JsonArray popularBlogs){
    RecyclerView  rvPopularBlogs = findViewById(R.id.rvPopularBlogs);
    LinearLayoutManager popularLM = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL, false);

    rvPopularBlogs.setHasFixedSize(true);
    rvPopularBlogs.setNestedScrollingEnabled(true);
    rvPopularBlogs.setLayoutManager(popularLM);
    BlogDetailPopularBlogAdapter blogDetailPopularBlogAdapter = new BlogDetailPopularBlogAdapter(mContext,popularBlogs);
    rvPopularBlogs.swapAdapter(blogDetailPopularBlogAdapter,false);

}
    private void setRecentBlogs(JsonArray recentBlogs){
        RecyclerView  rvRecentPost = (RecyclerView) findViewById(R.id.rvRecentPost);
        LinearLayoutManager recentLM = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL, false);

        rvRecentPost.setHasFixedSize(true);
        rvRecentPost.setNestedScrollingEnabled(true);
        rvRecentPost.setLayoutManager(recentLM);
        BlogDetailPopularBlogAdapter blogDetailPopularBlogAdapter2 = new BlogDetailPopularBlogAdapter(mContext,recentBlogs);
        rvRecentPost.swapAdapter(blogDetailPopularBlogAdapter2,false);
    }

    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, MConstant.SERVER+"/blog-details/"+blog.get("slug").getAsString());
        Intent intent = Intent.createChooser(shareIntent,"Share");
        return shareIntent;
    }

    public void sendLike() {
        String url = MConstant.API_END+"/likeblog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("blog_id",blogID);
        params.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = response.getJSONObject("data");
                            String totalLikes = obj.getString("total_like");
                            tvLike.setText("Like ("+totalLikes+")");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, "Network Error", Toast.LENGTH_SHORT).show();
                        Log.e("VolleyError",error.toString());
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void sendComment() {
        /**api_key
         *blog_id
         *user_id
         *comment*/
        String url = MConstant.API_END+"/likeblog";
        Map<String, String> params = new HashMap<String, String>();
        params.put("blog_id",blogID);
        params.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = response.getJSONObject("data");
                            String totalLikes = obj.getString("total_like");
                            tvLike.setText("Like ("+totalLikes+")");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, "Network Error", Toast.LENGTH_SHORT).show();
                        Log.e("VolleyError",error.toString());
                        error.printStackTrace();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    /*private void bookmark(){
        String url = MConstant.API_END+"/addBookmark";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("id",blogID);
        params.put("user_id",MFunction.getMyPrefVal("user_id",mContext));
        params.put("type","1");
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                String msg = response.getString("message");
                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }*/

    private void follow(){
        String url = MConstant.API_END+"/follow";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("follow_to",authorID);
        params.put("follow_by",MFunction.getMyPrefVal("user_id",mContext));
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status) {
                                int followerCount = blog.get("tfollowers").getAsInt();

                                tvFollowers.setText(followerCount+1+" Followers");
                                btnFollow.setText("Following");
                                String msg = response.getString("message");

                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

/*    public void showCommentDialog(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogBlogCommentFragment frag = new DialogBlogCommentFragment();
        frag.show(ft, "txn_tag");
    }*/
    public void showNewCommentDialog(View view) {
        Intent nameIntent = new Intent(BlogDetailActivity.this, BlogCreateCommentActivity.class);
        startActivityForResult(nameIntent, COMMENT_RESULT_CODE);
    }

    static public class DialogBlogCommentFragment extends DialogFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
        }

        @Override
        public void onStart() {
            super.onStart();
            Dialog d = getDialog();
            if (d!=null){
                int width = ViewGroup.LayoutParams.MATCH_PARENT;
                int height = ViewGroup.LayoutParams.MATCH_PARENT;
                d.getWindow().setLayout(width, height);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.dialog_blog_comment, container, false);
            return root;
        }

    }

    public void fetchBlog() {
       openDialog();
        String url = MConstant.API_END+"/getBlogDetailData";
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
        params.put("blog_id",blogID);
        JSONObject objRegData = new JSONObject(params);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, objRegData, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        closeDialog();

                        try {
                            if(!response.getBoolean("status"))return;
                            JsonParser jsonParser = new JsonParser();
                            blog = (JsonObject) jsonParser.parse(response.getString("data"));
                            JsonArray arrayPopularBlogs = (JsonArray) jsonParser.parse(response.getString("popular_blogs"));
                            JsonArray arrayRecentBlogs = (JsonArray) jsonParser.parse(response.getString("popular_blogs"));
                            //setUI(blog);
                            setPopularBlogs(arrayPopularBlogs);
                            setRecentBlogs(arrayRecentBlogs);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                       closeDialog();
                        /*
                        blogList = Blog.getAllBlog(100);
                        blogMainAdapter = new BlogMainAdapter(mContext,blogList);
                        rvBlog.setAdapter(blogMainAdapter);*/
                        error.printStackTrace();
                        Log.i("volleyError",error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public String getFormattedDate(String tempDate){
        String mDate = tempDate;
        SimpleDateFormat fromDFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat toDFormat=new SimpleDateFormat("d MMM , h:mm a");
        String finalDate= null;
        int fromYear=0;
        int fromMonth=0;
        int fromDay=0;
        long dbSeconds =0;
        double showHours = 0;
        double showMinutes =0;
        double showSeconds =0;
        double mRemainder =0;
        try {
            Date date = fromDFormat.parse(mDate);
                /* for getting yyyy,mm,dd*/
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            fromYear = calendar.get(Calendar.YEAR);
            fromMonth = calendar.get(Calendar.MONTH) + 1;
            fromDay = calendar.get(Calendar.DAY_OF_MONTH);
            dbSeconds = date.getTime()/1000;
            finalDate=   toDFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        long currentSeconds = new Date().getTime()/1000;
        long diffSeconds = currentSeconds - dbSeconds;

        if (diffSeconds < (long)86400) {
            showHours = (double)diffSeconds/(double) 3600;
            int intHrPart = (int) showHours;
            showMinutes = (showHours - (double)intHrPart) * (double)60;
            int intMPart = (int) showMinutes;
            showSeconds = (showMinutes - (double) intMPart) * (double)60;
            int intSPart = (int) showSeconds;

            String hr   = (intHrPart != 0)?String.valueOf(intHrPart) + "hr ":"";
            String m    = (intMPart != 0)?String.valueOf(intMPart) + "m ":"";
            String s    = (intSPart != 0)?String.valueOf(intSPart) + "s ":"";
            finalDate   = hr + m + s + "ago";
        }
        return finalDate;
    }



    public void openDialog() {
        dialog = new Dialog(mContext); // Context, this, etc.
        dialog.setContentView(R.layout.loading_dialog);
        dialog.setTitle(null);
        /*final FrameLayout frameLayout=dialog.findViewById(R.id.placeholder);*/
        final VideoView videoView = dialog.findViewById(R.id.loading_animation);
        final ConstraintLayout constraintLayout=dialog.findViewById(R.id.dialogBackground);
        videoView.setVideoPath("android.resource://" + mContext.getPackageName() + "/" + R.raw.foodpal);
        videoView.setZOrderOnTop(true);



        videoView.setOnPreparedListener (new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
               /* frameLayout.setVisibility(View.GONE);*/
                mp.setLooping(true);
                videoView.start();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    public void closeDialog() {
        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == COMMENT_RESULT_CODE)
        if (resultCode == Activity.RESULT_OK) {

            try {
                joData.put("api_key",MConstant.API_ID_ACCESS_TOKEN_STRING);
                joData.put("blog_id",blogID);
                joData.put("user_id",MFunction.getMyPrefVal("user_id", mContext));
                joData.put("comment",data.getStringExtra(BLOG_COMMENT));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = MConstant.API_END + "/doblogcomment";

            VolleyStandard(url, ADD_COMMENT);

        }

    }


    private void VolleyStandard(String url,final int opt) {



        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, joData, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    //TODO ADD_COMMENT RESPONSE
                    if(opt== ADD_COMMENT){
                        if(response.getBoolean("status")==true){
                            Toast.makeText(mContext,"Commemnt added successfully",Toast.LENGTH_SHORT).show();
                        }
                    }else if(opt==ADD_LIKE){
                        JSONObject jData=response.getJSONObject("data");

                        int likeCount=jData.getInt("total_like");

                        if(response.getBoolean("status")==true) {
                            System.out.println("RESPONSE " + response);

                            tvLike.setText("Like ("+likeCount+")");
                            if(jData.getInt("Islike")>0){
                                System.out.println("LIKE added " + jData.getInt("total_like"));
                            }else{
                                System.out.println("LIKE reduced " + jData.getInt("total_like"));
                            }

                            //{"status":true,"message":"Success.","data":{"success":true,"type":"update","Islike":1,"total_like":4}}

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            /**
             * Returns a list of extra HTTP headers to go along with this request. Can throw {@link
             * AuthFailureError} as authentication may be required to provide these values.
             *
             * @throws AuthFailureError In the event of auth failure
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;

            }
        };
        jsonObjectRequest.setTag("OBTAINMETHOD");

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }



}










