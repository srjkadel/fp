package com.deltatechnepal.service;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.NotificationList;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.ormmodel.Notification;
import com.deltatechnepal.utility.MFunction;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

/**
 * Created by DeltaTech on 4/26/2018.
 */

public class GeofenceTransitionsIntentService extends IntentService {
    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            /*String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());*/
            Log.e("TAG", "errorMessage");
            Toast.makeText(this, "geofencingEventHasError", Toast.LENGTH_SHORT).show();
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(this, geofenceTransition, triggeringGeofences);
            //String geofenceTransitionDetails = "geofenceTransitionDetails";

            // Send notification and log the transition details.
            if(!TextUtils.isEmpty(geofenceTransitionDetails)){

                sendNotification(geofenceTransitionDetails);
                //TODO  save notification into database



                Notification tempNotification = new Notification();
                tempNotification.unique_id = MFunction.getUID();
                tempNotification.geofence_id = triggeringGeofences.get(0).getRequestId();
                tempNotification.geofence_transition_type = String.valueOf(geofenceTransition);
                tempNotification.title = "Geofence";
                tempNotification.description = geofenceTransitionDetails;
                tempNotification.unix_timestamp = String.valueOf(System.currentTimeMillis());
                tempNotification.save();
                Log.i("TAG", geofenceTransitionDetails);


            }

        } else {
            // Log the error.
            Log.e("TAG", "geofence_transition_invalid_type");
        }
    }

    private String getGeofenceTransitionDetails(Context context,int geofenceTransition, List<Geofence> triggeringGeofecnes){
        String returnString= "";
        Geofence triggeredG = triggeringGeofecnes.get(0);
        String tempID = triggeredG.getRequestId();



        if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            //  Geofence triggeredG = triggeringGeofecnes.get(0);

            Notification tempcheckexit = Notification.checkTransation(tempID,Geofence.GEOFENCE_TRANSITION_EXIT);
            Notification tempcheckenter = Notification.checkTransation(tempID,Geofence.GEOFENCE_TRANSITION_ENTER);

//            if(tempcheckenter ==null){
//                Toast.makeText(getApplicationContext(),"I am not available",Toast.LENGTH_LONG).show();
//            }


            if(tempcheckexit== null && tempcheckenter ==null ){

                returnString= "You have entered"+tempID;

            }

            else
            {
                returnString = "";
            }



        }

        if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {



      //      String i = String.valueOf(Notification.lastrow());
            Notification tempNot = Notification.checkTransation(tempID,Geofence.GEOFENCE_TRANSITION_ENTER);

            if(tempNot != null){
                returnString = "You have left "+ tempID;

            } else {

                returnString = "";
            }
        }
        return returnString;
    };

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "";
        String appName = this.getApplicationInfo().loadLabel(this.getPackageManager()).toString();

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(appName)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel  Title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        //For open activity
        Intent notifyintent =new Intent(this, NotificationList.class);
        notifyintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent notifypendingintent =PendingIntent.getActivity(this,0,notifyintent,PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(notifypendingintent);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
