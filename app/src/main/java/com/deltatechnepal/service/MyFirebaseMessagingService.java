package com.deltatechnepal.service;
import android.app.TaskStackBuilder;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.deltatechnepal.foodpal.MainActivity;
import com.deltatechnepal.foodpal.R;
import com.deltatechnepal.foodpal.RestoDetailActivity;
import com.deltatechnepal.ormmodel.Resto;
import com.deltatechnepal.ormmodel.Notification;
import com.deltatechnepal.utility.MConstant;
import com.deltatechnepal.utility.MFunction;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by DeltaTech on 3/21/2018.
 */



//package com.google.firebase.quickstart.fcm;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    Context mcontext;

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {

            JsonParser jsonParser = new JsonParser();
            String type = remoteMessage.getData().get("type"); //restaurant or event or blog or reminder
            String message = remoteMessage.getData().get("message"); //message body {"data_type":"restaurant","data":"","action_id":"100","title":"title1","description":"created_at"}
            String data = remoteMessage.getData().get("data"); // data payload {"data_type":"restaurant","restaurant":"data","action"=>"add");
            if(TextUtils.isEmpty(type)) return;

            switch(type){

                case MConstant.NOTIFICATION_TYPE_RESTAURANT:{ //Handle Restaurant Data
                    JsonElement dataElem = jsonParser.parse(data);
                    JsonObject dataObj = dataElem.getAsJsonObject();
                    String action = MFunction.getStringVal(dataObj,"action");

                    if(TextUtils.equals(action,"add")){
                        JsonObject obj = dataObj.getAsJsonObject("restaurant");
                        Resto tempResto = new Resto(obj);
                        long tempRestoSaved = tempResto.save();
                        sendNotification(message);
                    }
                    break;
                }

                case MConstant.NOTIFICATION_TYPE_EVENT:{

                    break;
                }

                case MConstant.NOTIFICATION_TYPE_BLOG:{

                    break;
                }

                case MConstant.NOTIFICATION_TYPE_REMINDER:{ // notify that use has not used app for long time
                    sendNotification(message);

                    break;
                }
                default:{
                    //do nothing
                    break;
                }
            }
        }
    }

    private void sendNotification(String message){

        //{"data_type":"restaurant","data":"","action_id":"100","title":"title1","description":"created_at"}

        JsonParser jsonParser = new JsonParser();
        JsonElement messageElem = jsonParser.parse(message);
        JsonObject noticeObj = messageElem.getAsJsonObject();
        String dataType = MFunction.getStringVal(noticeObj,"data_type");
        String actionID = MFunction.getStringVal(noticeObj,"action_id");

        Notification notification = new Notification();
        notification.notification_id    = MFunction.getStringVal(noticeObj,"notification_id");
        notification.title              = MFunction.getStringVal(noticeObj,"title");
        notification.description        = MFunction.getStringVal(noticeObj,"title");
        //notification.description      = MFunction.stripHtml(MFunction.getStringVal(noticeObj,"description")).toString();
        notification.unix_timestamp     = String.valueOf(System.currentTimeMillis());

        notification.save();

        Intent intent = new Intent();
        switch(dataType) {
            case MConstant.NOTIFICATION_TYPE_RESTAURANT: {

                intent = new Intent(this, RestoDetailActivity.class);
                Resto restaurant = Resto.getItem(actionID);
                String jsResto= new Gson().toJson(restaurant);
                Bundle restoBundle = new Bundle();
                restoBundle.putString("resto", jsResto);
                restoBundle.putBoolean("fromNotification", true);
                intent.putExtra("bundle", restoBundle);

                break;
            }

            default : {
                intent = new Intent(this, MainActivity.class);
                break;
            }
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        PendingIntent contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        //PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(TAG, notification.title, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(notification.description);
            getApplicationContext().getSystemService(NotificationManager.class).createNotificationChannel(channel);
        }

//        else if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP){
//            android.app.Notification notif = new NotificationCompat.Builder(getApplicationContext(), TAG)
//
//                    .setContentTitle(notification.title)
//                    .setContentText(notification.description)
//                    .setAutoCancel(true)
//                    .setChannelId(TAG)
//                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                    .setContentIntent(contentIntent)
//                    .setSmallIcon(R.drawable.logo)
//                    .setShowWhen(true)
//                    //.setColor(Color.WHITE)
//                    .setLocalOnly(true)
//                    .build();
//
//            NotificationManagerCompat.from(getApplicationContext()).notify(new Random().nextInt(), notif);
//
//        }

        android.app.Notification notif = new NotificationCompat.Builder(getApplicationContext(), TAG)
                .setContentTitle(notification.title)
                .setContentText(notification.description)
                .setAutoCancel(true)
                .setChannelId(TAG)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.foodpalnotiflogo)
                .setShowWhen(true)
               //.setColor(Color.WHITE)
                .setLocalOnly(true)
                .build();

        NotificationManagerCompat.from(getApplicationContext()).notify(new Random().nextInt(), notif);
    }


}
